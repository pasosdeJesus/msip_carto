Dummy::Application.config.relative_url_root = ENV.fetch(
  'RUTA_RELATIVA', '/carto'
)
Dummy::Application.config.assets.prefix = if ENV.fetch(
  'RUTA_RELATIVA', '/carto'
) == '/'
                                            '/assets'
                                          else
                                            (ENV.fetch('RUTA_RELATIVA', '/carto') + '/assets')
                                          end
