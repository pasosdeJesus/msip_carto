Msip.setup do |config|
  config.ruta_anexos = ENV.fetch(
    'MSIP_RUTA_ANEXOS', "#{Rails.root}/archivos/anexos/"
  )
  config.ruta_volcados = ENV.fetch(
    'MSIP_RUTA_VOLCADOS', "#{Rails.root}/archivos/bd/"
  )
  config.titulo = "msip_carto #{MsipCarto::VERSION}"
  config.descripcion = 'Aplicación de prueba del motor msip_carto que valida fronteras de Colombia en OpenStreetMap'
  config.codigofuente = 'https://gitlab.com/pasosdeJesus/msip_carto'
  config.urlcontribuyentes = 'https://gitlab.com/pasosdeJesus/msip_carto/-/graphs/main'
  config.urlcreditos = 'https://gitlab.com/pasosdeJesus/msip_carto/-/blob/main/CREDITOS.md'
  config.agradecimientoDios = "<p>
  Agradecemos a Dios por su palabra y por permitir este desarrollo, el cual
  le dedicamos.
  </p>
  <blockquote>
    <i>De Jehová es la tierra y su plenitud;
    El mundo, y los que en él habitan.</i>
    <br>
    Salmo 24:1
  <blockquote>".html_safe
end
