Rails.application.routes.draw do
  rutarel = ENV.fetch('RUTA_RELATIVA', 'msip_carto/')
  scope rutarel do
    devise_scope :usuario do
      get 'sign_out' => 'devise/sessions#destroy'
      get 'salir' => 'devise/sessions#destroy',
          as: :salir_sesion
      post 'usuarios/iniciar_sesion', to: 'devise/sessions#create'
      get 'usuarios/iniciar_sesion', to: 'devise/sessions#new',
                                     as: :nuevo_iniciar_sesion
      if Rails.configuration.relative_url_root != '/'
        ruta = File.join(Rails.configuration.relative_url_root,
                         'usuarios/sign_in')
        post ruta, to: 'devise/sessions#create'
        get  ruta, to: 'devise/sessions#new'
        ruta = File.join(Rails.configuration.relative_url_root,
                         'usuarios/iniciar_sesion')
        post ruta, to: 'devise/sessions#create'
        get  ruta, to: 'devise/sessions#new'
      end
    end
    devise_for :usuarios, skip: [:registrations], module: :devise
    as :usuario do
      get 'usuarios/edit' => 'devise/registrations#edit',
          :as => 'edit_registro_usuario'
      get 'usuarios/editar' => 'devise/registrations#edit',
          :as => 'editar_registro_usuario'

      put 'usuarios/:id' => 'devise/registrations#update',
          :as => 'registro_usuario'
    end
    resources :usuarios, path_names: { new: 'nuevo', edit: 'edita' }

    root 'msip/hogar#index'

    namespace :admin do
      ab = ::Ability.new
      ab.tablasbasicas.each do |t|
        next unless t[0] == ''

        c = t[1].pluralize
        resources c.to_sym,
                  path_names: { new: 'nueva', edit: 'edita' }
      end
    end
  end

  mount MsipCarto::Engine, at: rutarel, as: 'msip_carto'
  mount Msip::Engine, at: rutarel, as: 'msip'
end
