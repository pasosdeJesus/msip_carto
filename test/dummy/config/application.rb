require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
# require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
# require "action_mailbox/engine"
# require "action_text/engine"
require "action_view/railtie"
require "action_cable/engine"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
require 'msip_carto'

module Dummy
  class Application < Rails::Application
    config.load_defaults Rails::VERSION::STRING.to_f

    config.autoload_lib(ignore: %w(assets tasks))

    # For compatibility with applications that use this config
    config.action_controller.include_all_helpers = true

    # Configuration for the application, engines, and railties goes here.

    config.active_record.schema_format = :sql

    #config.railties_order = [:main_app, Msip::Engine, :all]

    config.i18n.default_locale = :es

    config.x.formato_fecha = ENV.fetch('MSIP_FORMATO_FECHA', 'dd/M/yyyy')

    config.hosts.concat(
      ENV.fetch('CONFIG_HOSTS', '127.0.0.1').downcase.split(',')
    )

    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    config.time_zone = 'America/Bogota'
    # config.eager_load_paths << Rails.root.join("extras")
   
    if config.respond_to?(:web_console)
      config.web_console.whitelisted_ips = ENV.fetch(
        'WEBCONSOLE_WHITELISTEDIPS', "127.0.0.1").split(',')
    end
  end
end
