require 'json'

a = File.open('ref/depmun.geojson')
g = JSON.load a
a.close

manual = 0
g['features'].select { |f| f['properties']['admin_level'].to_i == 6 }.each do |nivel6|
  nom = nivel6['properties']['name']
  warn "Analizando #{nom}"
  div = nivel6['properties']['all_tags'] ? nivel6['properties']['all_tags']['divipola'] : nil
  dep = nil
  mun = nil
  if div.to_i > 0
    cdep = div[0..1]
    cmun = div[2..4]
    if Msip::Departamento.where(pais_id: 170).where(deplocal_cod: cdep.to_i).count == 1
      dep = Msip::Departamento.where(pais_id: 170).where(deplocal_cod: cdep.to_i).take
      if Msip::Municipio.where(departamento_id: dep.id).where(munlocal_cod: cmun.to_i).count == 1
        mun = Msip::Municipio.where(departamento_id: dep.id).where(munlocal_cod: cmun.to_i).take
      end
      if I18n.transliterate(mun.nombre.downcase) ==
         I18n.transliterate(nom.downcase)
        warn "Identificado #{mun.nombre} / #{dep.nombre} con municipio #{mun.id} de msip"
        mun.osm_id = nivel6['properties']['osm_id'].to_i
        mun.save
        puts "UPDATE msip_municipio SET osm_id=#{mun.osm_id} WHERE id=#{mun.id}; #{mun.nombre} / #{mun.departamento.nombre}"
      else
        warn '* Tiene DIVIPOLA pero no coinciden nombres'
      end
    end
  else
    warn '* No tiene DIVIPOLA'
  end
  next unless mun.nil?

  pmun = Msip::Municipio.where('LOWER(nombre)=LOWER(?)', nom)
  if pmun.count == 1
    if nivel6['properties']['parents'].split(',')
                                      .map(&:to_i).include?(pmun[0].departamento.osm_id)
      mun = pmun[0]
      warn "Por nombre #{nom} se encontró #{mun.nombre} en el mismo departamento #{mun.departamento.nombre}"
      mun.osm_id = nivel6['properties']['osm_id'].to_i
      mun.save
      puts "UPDATE msip_municipio SET osm_id=#{mun.osm_id} \n"\
      "  WHERE id=#{mun.id}; #{mun.nombre} / #{mun.departamento.nombre}"
    else
      warn "* Aunque sólo un municipio tiene nombre #{pmun[0].nombre} no coincide departamento"
    end
  else
    warn "* Hay #{pmun.count} municipios con nombre #{nom} como en relación #{nivel6['properties']['osm_id'].to_i}. Debe identificarse manualmente."
    manual += 1
  end
end

warn "En total #{manual} relaciones deben identificarse manualmente"
