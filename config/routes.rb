MsipCarto::Engine.routes.draw do
  post "/validar_dep_osm" => "validar#validar_dep_osm",
       as: :post_validar_dep_osm
  get "/validar_dep_osm" => "validar#validar_dep_osm",
      as: :validar_dep_osm
  get(
    "/pais_actualizar_silueta/:id" => 
    "validar#pais_actualizar_silueta_de_osm",
    as: :pais_actualizar_silueta_de_osm
  )
  get(
    "/departamento_actualizar_silueta/:id" => 
    "validar#departamento_actualizar_silueta_de_osm",
    as: :departamento_actualizar_silueta_de_osm
  )
  get(
    "/municipio_actualizar_silueta/:id" => 
    "validar#municipio_actualizar_silueta_de_osm",
    as: :municipio_actualizar_silueta_de_osm
  )

  get(
    "/generar_pais_completo/:id" => 
    "validar#generar_pais_completo",
    as: :generar_pais_completo
  )




  namespace :admin do
    ab = ::Ability.new
    ab.tablasbasicas.each do |t|
      next unless t[0] == "MsipCarto"

      c = t[1].pluralize
      resources c.to_sym,
                path_names: { new: "nueva", edit: "edita" }
    end

    # get "/paises/:id/actualizar_silueta_de_osm" => "/msip/admin/paises#actualizar_silueta_de_osm",
    #  as: :pais_actualizar_silueta_de_osm
  end
end
