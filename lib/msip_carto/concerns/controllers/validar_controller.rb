# frozen_string_literal: true

module MsipCarto
  module Concerns
    module Controllers
      module ValidarController
        extend ActiveSupport::Concern

        included do
          include ActionView::Helpers::AssetUrlHelper

          def validar_dep_osm
            @prob = []
            @dep_nombre = ''
            if !params || !params[:validar] ||
               (divipoladep = params[:validar][:divipoladep]).nil?
            elsif (dep = Msip::Departamento.where(pais_id: 170)
                   .where(deplocal_cod: divipoladep).take).nil?
              @prob << 'Parámetro validar[divipoladep] tiene valor inválido ' +
                       divipoladep
            elsif dep.osm_frontera.nil?
              @prob << 'En base de datos falta frontera para el departamento '\
                "#{dep.nombre}"
            elsif dep.osm_id.nil?
              @prob << 'En base de datos falta id de relación en OSM para '\
                "#{dep.nombre}"
            else
              @dep_nombre = dep.nombre
              # mpais = ActiveRecord::Base.connection.execute(
              #  "SELECT st_AsGeoJSON(osm_frontera) "\
              #  "  FROM msip_pais WHERE id=170")
              # geop = mpais[0]["st_asgeojson"]
              geoj = <<~EOF
                {
                  "type": "FeatureCollection",
                  "features": [

              EOF
              sep = ''
              ldep = Msip::Departamento.habilitados
                                       .where(pais_id: 170).order(:id)
              ldep.each do |od|
                mdep = ActiveRecord::Base.connection.execute(
                  'SELECT st_AsGeoJSON(osm_frontera) '\
                  "  FROM msip_departamento WHERE id=#{od.id}"
                )
                geod = mdep[0]['st_asgeojson']
                next unless geod

                geoj += <<~EOF
                  #{sep}{
                    "type": "Feature",
                    "properties": {
                      "id": "mixtidep#{od.deplocal_cod}",
                      "stroke-linejoin": "round",
                      "stroke-dashoffset": "0.9",
                      "stroke": "#{od.id == dep.id ? '#ffffff' : '#000000'}",
                      "stroke-width": "0.75",
                      "fill": "#{od.id == dep.id ? '#000000' : '#ffffff'}"
                     },
                     "geometry": #{geod}
                  },
                  {"type":"Feature",
                    "geometry":{
                      "type":"Point",
                      "coordinates":[#{od.osm_rotulolon},#{od.osm_rotulolat}]
                    },
                    "properties":{
                      "label-text":"#{od.nombre}",
                      "id": "nombredep#{od.deplocal_cod}"
                    }
                  }
                EOF
                sep = ", "
              end
              geoj += <<~EOF
                  ]
                }
              EOF

              dsvg = MsipCarto::OsmHelper.convertir_geojson_a_svg(geoj)
              @dep_svg = dsvg.to_s.html_safe

              puts "# Validación de #{dep.nombre} "\
                "<https://www.openstreetmap.org/relation/#{dep.osm_id}>"
              #        r = ActiveRecord::Base.connection.execute (
              #          "SELECT ST_Equals(geom, d.osm_frontera::geometry) AS ig "\
              #          "  FROM osm_relacion AS o"\
              #          "  JOIN msip_departamento AS d ON d.osm_id=o.id WHERE o.id=#{dep.osm_id}")
              #        if !r[0]["ig"]
              #          STDERR.puts "Diferencia en silueta de OSM y de msip"
              # if descargar
              #  STDERR.puts "  UPDATE msip_departamento SET "\
              #    "    osm_frontera=ST_Multi('#{siluetas_deps[dep.osm_id]}'), "\
              #    "    osm_fecha='#{Date.today.to_s}' "\
              #    "    WHERE id=#{dep.id};"
              # end
              #        end
              r = ActiveRecord::Base.connection.execute(
                'SELECT ST_IsValid(d.osm_frontera::geometry) '\
                '  FROM msip_departamento AS d'\
                "  WHERE id=#{dep.id}"
              )
              unless r[0]['st_isvalid']
                @prob << 'La silueta no es válida (no es cerrada o '\
                  'se autointersecta)'
              end

              puts "# Descargando relación de OSM #{dep.osm_id} "
              url = 'https://www.openstreetmap.org/api/0.6/relation/'\
                "#{dep.osm_id.abs}.json"
              uri = URI.parse(url)
              h = uri.read
              rel = JSON.parse(h)
              elem = rel['elements'][0]
              tags = elem['tags']
              @prob << 'No tiene la etiqueta `admin_level` -> `4`' if tags['admin_level'].to_s != '4'
              if tags['boundary'].to_s != 'administrative'
                @prob << 'No tiene la etiqueta `boundary` -> `administrative`'
              end
              @prob << 'No tiene la etiqueta `type` -> `boundary`' if tags['type'].to_s != 'boundary'
              if tags['divipola'].to_s.to_i != dep.deplocal_cod
                @prob << 'No tiene la etiqueta `divipola` -> '\
                  "`#{dep.deplocal_cod}`"
              end
              if tags['ISO3166-2'].to_s != dep.codiso
                @prob << 'Tiene mal la etiqueta `ISO3166-2` que es '\
                  "`#{tags['ISO3166-2']}` pero que debería ser "\
                  "`#{dep.codiso}`"
              end
              if tags.keys.select { |l| l[0..4] == 'name:' }.count > 0 &&
                 !tags['name:es']
                @prob << 'Requiere etiqueta `name:es`'
              end
              @prob << 'No tiene la etiqueta `wikipedia`' unless tags['wikipedia']

              [
                'clc_file',
                'DANE:departamento',
                'mechanical',
                'ref',
                'state_code'
              ].each do |t|
                @prob << "Le sobra la etiqueta `#{t}`" if tags[t]
              end

              isin = tags.keys.select { |l| l[0..5] == 'is_in:' }
              @prob << "Le sobra(n) la(s) etiqueta(s) `#{isin.join(',')}`" if isin.count > 0

              if tags['name'].to_s != dep.nombre &&
                 tags['official_name'] != dep.nombre
                @prob << 'Tiene mal etiqueta `name` o bien etiqueta '\
                  '`official_name` pues la primera o en su defecto la segunda '\
                  "deben ser el nombre en la DIVIPOLA es decir `#{dep.nombre}`"
              end

              nca = elem['members'].select do |m|
                m['type'] == 'node' && m['role'] == 'admin_centre'
              end
              if nca.count != 1
                @prob << 'No tiene un miembro `admin_centre` '\
                  '(centro administrativo)'
              else
                MsipCarto::OsmHelper.validar_nodo_centro_administrativo(
                  nca[0]['ref'], @prob
                )
              end

              nr = elem['members'].select do |m|
                m['type'] == 'node' && m['role'] == 'label'
              end
              if nr.count != 1
                @prob << 'No tiene un miembro label (rótulo)'
              else
                nodo_id = nr[0]['ref']
                MsipCarto::OsmHelper.validar_nodo_rotulo(
                  nodo_id, dep.nombre, @prob
                )
              end

              elem['members'].select do |m|
                m['type'] == 'way'
              end.each do |m|
                MsipCarto::OsmHelper.validar_via(m['ref'], @prob)
              end
            end

            respond_to do |format|
              format.html do
                render 'validar_dep_osm', layout: 'application'
              end
              format.json { head :no_content }
              format.js   { head :no_content }
            end
          end  # validar_dep_osm

          def pais_actualizar_silueta_de_osm
            if !params || !params['id'] || Msip::Pais.where(
              id: params['id'].to_i
            ).count != 1
              redirect_to Rails.configuration.relative_url_root,
                alert: 'Falta id única de país como parámetro'
              return
            end
            pais = Msip::Pais.find(params['id'].to_i)
            authorize! :update, pais
            llave = ENV.fetch('LLAVEOSMBOUNDARIES')
            prob = ''.dup
            unless MsipCarto::OsmHelper.actualizar_silueta_pais_de_osm(
              pais, llave, false, prob
            )
              redirect_to msip.admin_pais_path(pais.id),
                alert: prob
              return
            end

            redirect_to msip.admin_pais_path(pais.id),
              notice: "Silueta y rotulo de país actualizados. #{prob}"
          end  # def


          def departamento_actualizar_silueta_de_osm
            if !params || !params['id'] || Msip::Departamento.where(
              id: params['id'].to_i
            ).count != 1
              redirect_to Rails.configuration.relative_url_root,
                          alert: 'Falta id única de departamento como parámetro'
              return
            end
            departamento = Msip::Departamento.find(params['id'].to_i)
            authorize! :update, departamento
            llave = ENV.fetch('LLAVEOSMBOUNDARIES')
            prob = ''.dup
            unless MsipCarto::OsmHelper.actualizar_silueta_departamento_de_osm(
              departamento, llave, true, nil, prob
            )

              redirect_to msip.admin_departamento_path(departamento.id),
                          alert: prob
              return
            end
            redirect_to msip.admin_departamento_path(departamento.id),
              notice: "Silueta y rotulo de departamento actualizados. #{prob}"
          end  # def

          def municipio_actualizar_silueta_de_osm
            if !params || !params['id'] || Msip::Municipio.where(
              id: params['id'].to_i
            ).count != 1
            redirect_to Rails.configuration.relative_url_root,
                          alert: 'Falta id única de municipio como parámetro'
              return
            end
            municipio = Msip::Municipio.find(params['id'].to_i)
            authorize! :update, municipio
            llave = ENV.fetch('LLAVEOSMBOUNDARIES')
            prob = ''.dup
            unless MsipCarto::OsmHelper.actualizar_silueta_municipio_de_osm(
              municipio, llave, nil, prob
            )
              redirect_to msip.admin_municipio_path(municipio.id),
                          alert: prob
              return
            end

            redirect_to msip.admin_municipio_path(municipio.id),
              notice: "Silueta y rotulo de municipio actualizados. #{prob}"
          end  # def

          def generar_pais_completo
            if !params || !params['id'] || Msip::Pais.where(
              id: params['id'].to_i
            ).count != 1
              redirect_to Rails.configuration.relative_url_root,
                alert: 'Falta id única de país como parámetro'
              return
            end
            pais = Msip::Pais.find(params['id'].to_i)
            authorize! :read, pais
            geoj = <<~EOF
              {
                "type": "FeatureCollection",
                "features": [
              EOF
            sep = ''
            ldep = Msip::Departamento.habilitados
              .where(pais_id: pais.id).order(:id)
            ldep.each do |od|
              mdep = ActiveRecord::Base.connection.execute(
                'SELECT st_AsGeoJSON(osm_frontera) '\
                "  FROM msip_departamento WHERE id=#{od.id}"
              )
              geod = mdep[0]['st_asgeojson']
              next unless geod

              geoj += <<~EOF
                  #{sep}{
                    "type": "Feature",
                    "properties": {
                      "id": "mixtidep#{od.deplocal_cod}",
                      "stroke-linejoin": "round",
                      "stroke-dashoffset": "0.9",
                      "stroke": "#000",
                      "stroke-width": "0.75",
                      "fill": "#fff"
                     },
                     "geometry": #{geod}
                  },
                  {"type":"Feature",
                    "geometry":{
                      "type":"Point",
                      "coordinates":[#{od.osm_rotulolon},#{od.osm_rotulolat}]
                    },
                    "properties":{
                      "label-text":"#{od.nombre}",
                      "id": "nombredep#{od.deplocal_cod}"
                    }
                  }
              EOF
              sep = ", "
            end

            lmuns = Msip::Municipio.habilitados.where(
              'departamento_id IN (select id FROM msip_departamento AS d
                 WHERE pais_id=170 AND d.fechadeshabilitacion IS NULL)')
            lmuns.each do |mun|
              mmun = ActiveRecord::Base.connection.execute(
                'SELECT st_AsGeoJSON(osm_frontera) '\
                "  FROM msip_municipio WHERE id=#{mun.id}"
              )
              geom = mmun[0]['st_asgeojson']
              next unless geom

              codmun = mun.munlocal_cod
              if pais.id == 170
                codmun = mun.departamento.deplocal_cod * 1000 + mun.munlocal_cod
              end
              geoj += <<~EOF
                  ,{
                    "type": "Feature",
                    "properties": {
                      "id": "mixtimun#{codmun}",
                      "stroke-linejoin": "round",
                      "stroke-dashoffset": "0.9",
                      "stroke": "#000",
                      "stroke-width": "0.75",
                      "fill": "#fff"
                     },
                     "geometry": #{geom}
                  },
                  {"type":"Feature",
                    "geometry":{
                      "type":"Point",
                      "coordinates":[#{mun.osm_rotulolon || 0},#{mun.osm_rotulolat || 0}]
                    },
                    "properties":{
                      "label-text":"#{mun.nombre}",
                      "id": "nombremun#{codmun}"
                    }
                  }
                EOF
            end


              geoj += <<~EOF
                  ]
                }
              EOF

              psvg = MsipCarto::OsmHelper.convertir_geojson_a_svg(geoj, 512)
              msvg = MsipCarto::OsmHelper.modificar_svg_y_actualizar_base(psvg)
              @svg = msvg.to_s.html_safe


              respond_to do |format|
                format.html do
                  render 'generar_pais_completo', layout: 'application'
                end
                format.json { head :no_content }
                format.js   { head :no_content }
              end


          end


        end # include
      end # module
    end # module
  end # module
end # module
