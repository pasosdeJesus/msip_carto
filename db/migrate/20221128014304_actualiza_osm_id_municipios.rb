class ActualizaOsmIdMunicipios < ActiveRecord::Migration[7.0]
  def up
    execute <<~SQL
      --- Determinados automaticamente con lo descargado de osm-boundaries ver directorio ref
      UPDATE msip_municipio SET osm_id=-1342330 WHERE id=175; -- Campo de La Cruz / Atlántico
      UPDATE msip_municipio SET osm_id=-11375157 WHERE id=1118; -- San Carlos de Guaroa / Meta
      UPDATE msip_municipio SET osm_id=-1309437 WHERE id=1088; -- San Agustín / Huila
      UPDATE msip_municipio SET osm_id=-11730767 WHERE id=1122; -- San José de Uré / Córdoba
      UPDATE msip_municipio SET osm_id=-1306572 WHERE id=1327; -- Togüí / Boyacá
      UPDATE msip_municipio SET osm_id=-1345093 WHERE id=485; -- Fosca / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1350556 WHERE id=597; -- Heliconia / Antioquia
      UPDATE msip_municipio SET osm_id=-1473617 WHERE id=1339; -- Totoró / Cauca
      UPDATE msip_municipio SET osm_id=-1354767 WHERE id=857; -- Venecia / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1421723 WHERE id=217; -- Cáqueza / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1350552 WHERE id=973; -- Armenia / Antioquia
      UPDATE msip_municipio SET osm_id=-11375017 WHERE id=57; -- Villavicencio / Meta
      UPDATE msip_municipio SET osm_id=-1346368 WHERE id=1354; -- Ubaque / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1309985 WHERE id=1145; -- San Pablo / Nariño
      UPDATE msip_municipio SET osm_id=-1422329 WHERE id=87; -- Buenavista / Boyacá
      UPDATE msip_municipio SET osm_id=-1343716 WHERE id=731; -- Macaravita / Santander
      UPDATE msip_municipio SET osm_id=-1315419 WHERE id=406; -- Chivor / Boyacá
      UPDATE msip_municipio SET osm_id=-11909566 WHERE id=508; -- Gama / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1309462 WHERE id=477; -- Flandes / Tolima
      UPDATE msip_municipio SET osm_id=-1344182 WHERE id=369; -- Covarachía / Boyacá
      UPDATE msip_municipio SET osm_id=-1308646 WHERE id=1455; -- Betéitiva / Boyacá
      UPDATE msip_municipio SET osm_id=-1346472 WHERE id=1254; -- Susa / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1343436 WHERE id=132; -- Cáceres / Antioquia
      UPDATE msip_municipio SET osm_id=-1315399 WHERE id=583; -- Guayatá / Boyacá
      UPDATE msip_municipio SET osm_id=-1461222 WHERE id=905; -- Argelia / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1423451 WHERE id=480; -- Floridablanca / Santander
      UPDATE msip_municipio SET osm_id=-1343023 WHERE id=568; -- Guática / Risaralda
      UPDATE msip_municipio SET osm_id=-1314633 WHERE id=1452; -- Dibulla / La Guajira
      UPDATE msip_municipio SET osm_id=-1306575 WHERE id=1316; -- Tinjacá / Boyacá
      UPDATE msip_municipio SET osm_id=-11909887 WHERE id=1206; -- Simijaca / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1314682 WHERE id=285; -- Ciénaga / Magdalena
      UPDATE msip_municipio SET osm_id=-1343025 WHERE id=362; -- Córdoba / Quindío
      UPDATE msip_municipio SET osm_id=-1350526 WHERE id=546; -- Giraldo / Antioquia
      UPDATE msip_municipio SET osm_id=-1448414 WHERE id=1229; -- Soplaviento / Bolívar
      UPDATE msip_municipio SET osm_id=-1423514 WHERE id=1382; -- Vetas / Santander
      UPDATE msip_municipio SET osm_id=-407424 WHERE id=762; -- Marquetalia / Caldas
      UPDATE msip_municipio SET osm_id=-1307270 WHERE id=1044; -- Sabaneta / Antioquia
      UPDATE msip_municipio SET osm_id=-1342086 WHERE id=888; -- Palermo / Huila
      UPDATE msip_municipio SET osm_id=-4061117 WHERE id=820; -- Nuquí / Chocó
      UPDATE msip_municipio SET osm_id=-1326239 WHERE id=787; -- Santa Cruz de Mompox / Bolívar
      UPDATE msip_municipio SET osm_id=-407423 WHERE id=1384; -- Victoria / Caldas
      UPDATE msip_municipio SET osm_id=-1309476 WHERE id=1123; -- Santa Rosa de Cabal / Risaralda
      UPDATE msip_municipio SET osm_id=-1324918 WHERE id=766; -- Apartadó / Antioquia
      UPDATE msip_municipio SET osm_id=-3947502 WHERE id=200; -- El Carmen de Viboral / Antioquia
      UPDATE msip_municipio SET osm_id=-1315450 WHERE id=1240; -- Suaita / Santander
      UPDATE msip_municipio SET osm_id=-1350661 WHERE id=237; -- Cereté / Córdoba
      UPDATE msip_municipio SET osm_id=-1354797 WHERE id=1428; -- Zetaquira / Boyacá
      UPDATE msip_municipio SET osm_id=-1315405 WHERE id=772; -- Miraflores / Boyacá
      UPDATE msip_municipio SET osm_id=-1343471 WHERE id=1069; -- San Juan de Urabá / Antioquia
      UPDATE msip_municipio SET osm_id=-1314683 WHERE id=1367; -- Urumita / La Guajira
      UPDATE msip_municipio SET osm_id=-1314630 WHERE id=110; -- El Molino / La Guajira
      UPDATE msip_municipio SET osm_id=-1356238 WHERE id=591; -- Guayabetal / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1308617 WHERE id=1366; -- Valle de San Juan / Tolima
      UPDATE msip_municipio SET osm_id=-1335176 WHERE id=541; -- Hatillo de Loba / Bolívar
      UPDATE msip_municipio SET osm_id=-1315455 WHERE id=357; -- Contratación / Santander
      UPDATE msip_municipio SET osm_id=-10688523 WHERE id=1185; -- Sasaima / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11897328 WHERE id=476; -- Firavitoba / Boyacá
      UPDATE msip_municipio SET osm_id=-1315457 WHERE id=426; -- El Guacamayo / Santander
      UPDATE msip_municipio SET osm_id=-1341561 WHERE id=502; -- Galapa / Atlántico
      UPDATE msip_municipio SET osm_id=-4079109 WHERE id=648; -- La Celia / Risaralda
      UPDATE msip_municipio SET osm_id=-1324937 WHERE id=379; -- Clemencia / Bolívar
      UPDATE msip_municipio SET osm_id=-1348574 WHERE id=194; -- Caramanta / Antioquia
      UPDATE msip_municipio SET osm_id=-1355985 WHERE id=1050; -- San Antonio del Tequendama / Cundinamarca
      UPDATE msip_municipio SET osm_id=-410732 WHERE id=654; -- La Merced / Caldas
      UPDATE msip_municipio SET osm_id=-1350647 WHERE id=852; -- Olaya / Antioquia
      UPDATE msip_municipio SET osm_id=-11890103 WHERE id=149; -- Agustín Codazzi / Cesar
      UPDATE msip_municipio SET osm_id=-1335177 WHERE id=989; -- Achí / Bolívar
      UPDATE msip_municipio SET osm_id=-1309443 WHERE id=453; -- Altamira / Huila
      UPDATE msip_municipio SET osm_id=-1342088 WHERE id=223; -- Aipe / Huila
      UPDATE msip_municipio SET osm_id=-1315464 WHERE id=324; -- Albania / Santander
      UPDATE msip_municipio SET osm_id=-1342087 WHERE id=326; -- Algeciras / Huila
      UPDATE msip_municipio SET osm_id=-1314685 WHERE id=534; -- Algarrobo / Magdalena
      UPDATE msip_municipio SET osm_id=-1353248 WHERE id=983; -- Apulo / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1308621 WHERE id=737; -- Anzoátegui / Tolima
      UPDATE msip_municipio SET osm_id=-4047308 WHERE id=1091; -- Ataco / Tolima
      UPDATE msip_municipio SET osm_id=-1448415 WHERE id=878; -- Arjona / Bolívar
      UPDATE msip_municipio SET osm_id=-1309446 WHERE id=146; -- Agrado / Huila
      UPDATE msip_municipio SET osm_id=-11893388 WHERE id=722; -- Lourdes / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1343439 WHERE id=1117; -- Ayapel / Córdoba
      UPDATE msip_municipio SET osm_id=-1311762 WHERE id=1270; -- Barbacoas / Nariño
      UPDATE msip_municipio SET osm_id=-11312106 WHERE id=1056; -- Arauquita / Arauca
      UPDATE msip_municipio SET osm_id=-1423484 WHERE id=571; -- Astrea / Cesar
      UPDATE msip_municipio SET osm_id=-1343465 WHERE id=1197; -- Barranco de Loba / Bolívar
      UPDATE msip_municipio SET osm_id=-1348334 WHERE id=1212; -- Balboa / Risaralda
      UPDATE msip_municipio SET osm_id=-1353371 WHERE id=1373; -- Beltrán / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1353027 WHERE id=6; -- Agua de Dios / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1315400 WHERE id=1109; -- San Miguel de Sema / Boyacá
      UPDATE msip_municipio SET osm_id=-1350599 WHERE id=576; -- Guatapé / Antioquia
      UPDATE msip_municipio SET osm_id=-1308648 WHERE id=1386; -- Belén / Boyacá
      UPDATE msip_municipio SET osm_id=-1343728 WHERE id=1454; -- Betulia / Santander
      UPDATE msip_municipio SET osm_id=-1335179 WHERE id=22; -- Barranquilla / Atlántico
      UPDATE msip_municipio SET osm_id=-1316185 WHERE id=83; -- Briceño / Antioquia
      UPDATE msip_municipio SET osm_id=-1461292 WHERE id=78; -- Bolívar / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1419932 WHERE id=1310; -- Tibacuy / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1423468 WHERE id=1011; -- Bosconia / Cesar
      UPDATE msip_municipio SET osm_id=-1342085 WHERE id=138; -- Cajamarca / Tolima
      UPDATE msip_municipio SET osm_id=-1422325 WHERE id=198; -- Caparrapí / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11378018 WHERE id=201; -- Calamar / Guaviare
      UPDATE msip_municipio SET osm_id=-1343458 WHERE id=1451; -- Canalete / Córdoba
      UPDATE msip_municipio SET osm_id=-1454259 WHERE id=109; -- Buesaco / Nariño
      UPDATE msip_municipio SET osm_id=-1353412 WHERE id=242; -- Chaguaní / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11889894 WHERE id=142; -- Cajicá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1350633 WHERE id=79; -- Ciudad Bolívar / Antioquia
      UPDATE msip_municipio SET osm_id=-1342127 WHERE id=1256; -- Baraya / Huila
      UPDATE msip_municipio SET osm_id=-1455822 WHERE id=415; -- Chachagüí / Nariño
      UPDATE msip_municipio SET osm_id=-1315422 WHERE id=241; -- Charalá / Santander
      UPDATE msip_municipio SET osm_id=-1344035 WHERE id=276; -- Chiscas / Boyacá
      UPDATE msip_municipio SET osm_id=-4047593 WHERE id=342; -- Colombia / Huila
      UPDATE msip_municipio SET osm_id=-1343438 WHERE id=244; -- Chimá / Córdoba
      UPDATE msip_municipio SET osm_id=-1419938 WHERE id=277; -- Choachí / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1307274 WHERE id=616; -- Angelópolis / Antioquia
      UPDATE msip_municipio SET osm_id=-1356077 WHERE id=1464; -- Bojacá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1307263 WHERE id=549; -- Girardota / Antioquia
      UPDATE msip_municipio SET osm_id=-1423474 WHERE id=261; -- Chimichagua / Cesar
      UPDATE msip_municipio SET osm_id=-1456122 WHERE id=370; -- Colón / Putumayo
      UPDATE msip_municipio SET osm_id=-1308619 WHERE id=243; -- Chaparral / Tolima
      UPDATE msip_municipio SET osm_id=-1344609 WHERE id=281; -- Chitaraque / Boyacá
      UPDATE msip_municipio SET osm_id=-1315414 WHERE id=1346; -- Turmequé / Boyacá
      UPDATE msip_municipio SET osm_id=-1315078 WHERE id=1369; -- Valle de San José / Santander
      UPDATE msip_municipio SET osm_id=-1346479 WHERE id=280; -- Chocontá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1353019 WHERE id=398; -- Chíquiza / Boyacá
      UPDATE msip_municipio SET osm_id=-1717031 WHERE id=108; -- Buenos Aires / Cauca
      UPDATE msip_municipio SET osm_id=-1324941 WHERE id=107; -- Buenavista / Sucre
      UPDATE msip_municipio SET osm_id=-11375373 WHERE id=380; -- Cubarral / Meta
      UPDATE msip_municipio SET osm_id=-1723758 WHERE id=360; -- Corinto / Cauca
      UPDATE msip_municipio SET osm_id=-11329283 WHERE id=609; -- Imués / Nariño
      UPDATE msip_municipio SET osm_id=-1462480 WHERE id=113; -- Bugalagrande / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1343457 WHERE id=31; -- Cartagena de Indias / Bolívar
      UPDATE msip_municipio SET osm_id=-1343711 WHERE id=195; -- Capitanejo / Santander
      UPDATE msip_municipio SET osm_id=-1345019 WHERE id=359; -- Coper / Boyacá
      UPDATE msip_municipio SET osm_id=-1438583 WHERE id=471; -- El Dorado / Meta
      UPDATE msip_municipio SET osm_id=-1448408 WHERE id=361; -- Córdoba / Bolívar
      UPDATE msip_municipio SET osm_id=-11375320 WHERE id=216; -- Castilla La Nueva / Meta
      UPDATE msip_municipio SET osm_id=-3947350 WHERE id=222; -- Caucasia / Antioquia
      UPDATE msip_municipio SET osm_id=-1423470 WHERE id=233; -- Cepitá / Santander
      UPDATE msip_municipio SET osm_id=-1346398 WHERE id=493; -- Fúquene / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1423513 WHERE id=421; -- El Banco / Magdalena
      UPDATE msip_municipio SET osm_id=-1438573 WHERE id=444; -- El Castillo / Meta
      UPDATE msip_municipio SET osm_id=-1343467 WHERE id=278; -- Chinú / Córdoba
      UPDATE msip_municipio SET osm_id=-11891460 WHERE id=266; -- Chiriguaná / Cesar
      UPDATE msip_municipio SET osm_id=-1473612 WHERE id=429; -- El Cerrito / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1319503 WHERE id=478; -- Florida / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1346517 WHERE id=334; -- Cogua / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1311764 WHERE id=390; -- Cumbal / Nariño
      UPDATE msip_municipio SET osm_id=-11891221 WHERE id=391; -- Curumaní / Cesar
      UPDATE msip_municipio SET osm_id=-1315463 WHERE id=572; -- Guadalupe / Santander
      UPDATE msip_municipio SET osm_id=-1456137 WHERE id=341; -- Curillo / Caquetá
      UPDATE msip_municipio SET osm_id=-1324953 WHERE id=463; -- Guaranda / Sucre
      UPDATE msip_municipio SET osm_id=-1307002 WHERE id=959; -- Pueblorrico / Antioquia
      UPDATE msip_municipio SET osm_id=-1343400 WHERE id=140; -- Cácota / Norte de Santander
      UPDATE msip_municipio SET osm_id=-11332010 WHERE id=366; -- Córdoba / Nariño
      UPDATE msip_municipio SET osm_id=-1343270 WHERE id=403; -- Dabeiba / Antioquia
      UPDATE msip_municipio SET osm_id=-1438571 WHERE id=1461; -- Distracción / La Guajira
      UPDATE msip_municipio SET osm_id=-11892258 WHERE id=424; -- El Carmen / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1423493 WHERE id=596; -- Hato / Santander
      UPDATE msip_municipio SET osm_id=-1335175 WHERE id=1129; -- Santo Tomás / Atlántico
      UPDATE msip_municipio SET osm_id=-11895071 WHERE id=1264; -- Tabio / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1343721 WHERE id=1131; -- San Miguel / Santander
      UPDATE msip_municipio SET osm_id=-1350612 WHERE id=555; -- Granada / Antioquia
      UPDATE msip_municipio SET osm_id=-1314973 WHERE id=1127; -- San José de Miranda / Santander
      UPDATE msip_municipio SET osm_id=-1306574 WHERE id=167; -- Caldas / Boyacá
      UPDATE msip_municipio SET osm_id=-1423452 WHERE id=564; -- Guamal / Magdalena
      UPDATE msip_municipio SET osm_id=-1314966 WHERE id=464; -- Enciso / Santander
      UPDATE msip_municipio SET osm_id=-1342921 WHERE id=407; -- Dolores / Tolima
      UPDATE msip_municipio SET osm_id=-1309463 WHERE id=606; -- Icononzo / Tolima
      UPDATE msip_municipio SET osm_id=-1306568 WHERE id=1080; -- San José de Pare / Boyacá
      UPDATE msip_municipio SET osm_id=-1423518 WHERE id=348; -- Confines / Santander
      UPDATE msip_municipio SET osm_id=-1342119 WHERE id=428; -- El Doncello / Caquetá
      UPDATE msip_municipio SET osm_id=-1448419 WHERE id=418; -- El Carmen de Bolívar / Bolívar
      UPDATE msip_municipio SET osm_id=-1306999 WHERE id=423; -- El Carmen de Atrato / Chocó
      UPDATE msip_municipio SET osm_id=-1341550 WHERE id=932; -- Polonuevo / Atlántico
      UPDATE msip_municipio SET osm_id=-1352536 WHERE id=1135; -- Santana / Boyacá
      UPDATE msip_municipio SET osm_id=-1344489 WHERE id=1321; -- Tipacoque / Boyacá
      UPDATE msip_municipio SET osm_id=-4046923 WHERE id=622; -- Jamundí / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1346474 WHERE id=1261; -- Sutatausa / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1344193 WHERE id=430; -- El Espino / Boyacá
      UPDATE msip_municipio SET osm_id=-1307276 WHERE id=358; -- Copacabana / Antioquia
      UPDATE msip_municipio SET osm_id=-4060664 WHERE id=410; -- El Copey / Cesar
      UPDATE msip_municipio SET osm_id=-1413037 WHERE id=489; -- Funza / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1324944 WHERE id=402; -- El Roble / Sucre
      UPDATE msip_municipio SET osm_id=-1352331 WHERE id=783; -- Monguí / Boyacá
      UPDATE msip_municipio SET osm_id=-1314679 WHERE id=481; -- Fonseca / La Guajira
      UPDATE msip_municipio SET osm_id=-1423495 WHERE id=133; -- Cabrera / Santander
      UPDATE msip_municipio SET osm_id=-13436740 WHERE id=1389; -- Villagómez / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11378335 WHERE id=433; -- El Retorno / Guaviare
      UPDATE msip_municipio SET osm_id=-11329267 WHERE id=856; -- Ospina / Nariño
      UPDATE msip_municipio SET osm_id=-1355727 WHERE id=588; -- Guayabal de Síquima / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11890330 WHERE id=467; -- El Retén / Magdalena
      UPDATE msip_municipio SET osm_id=-1352118 WHERE id=238; -- Cerinza / Boyacá
      UPDATE msip_municipio SET osm_id=-1356128 WHERE id=553; -- Granada / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1423501 WHERE id=503; -- Galán / Santander
      UPDATE msip_municipio SET osm_id=-1345213 WHERE id=1235; -- Soracá / Boyacá
      UPDATE msip_municipio SET osm_id=-1324919 WHERE id=405; -- Galeras / Sucre
      UPDATE msip_municipio SET osm_id=-1342091 WHERE id=472; -- Falan / Tolima
      UPDATE msip_municipio SET osm_id=-1353242 WHERE id=373; -- Almeida / Boyacá
      UPDATE msip_municipio SET osm_id=-1345219 WHERE id=1402; -- Viracachá / Boyacá
      UPDATE msip_municipio SET osm_id=-1315394 WHERE id=365; -- Corrales / Boyacá
      UPDATE msip_municipio SET osm_id=-1309984 WHERE id=1128; -- San Bernardo / Nariño
      UPDATE msip_municipio SET osm_id=-1344425 WHERE id=851; -- Oicatá / Boyacá
      UPDATE msip_municipio SET osm_id=-1309472 WHERE id=618; -- Istmina / Chocó
      UPDATE msip_municipio SET osm_id=-1453518 WHERE id=540; -- Guachené / Cauca
      UPDATE msip_municipio SET osm_id=-11375481 WHERE id=490; -- Fuente de Oro / Meta
      UPDATE msip_municipio SET osm_id=-1315454 WHERE id=147; -- Aguada / Santander
      UPDATE msip_municipio SET osm_id=-1423512 WHERE id=559; -- Guacamayas / Boyacá
      UPDATE msip_municipio SET osm_id=-1315458 WHERE id=890; -- Palmas del Socorro / Santander
      UPDATE msip_municipio SET osm_id=-4060699 WHERE id=551; -- González / Cesar
      UPDATE msip_municipio SET osm_id=-1353243 WHERE id=1231; -- Somondoco / Boyacá
      UPDATE msip_municipio SET osm_id=-1554698 WHERE id=496; -- Florencia / Cauca
      UPDATE msip_municipio SET osm_id=-1314970 WHERE id=744; -- Málaga / Santander
      UPDATE msip_municipio SET osm_id=-11895448 WHERE id=598; -- Herrán / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1307012 WHERE id=608; -- Hispania / Antioquia
      UPDATE msip_municipio SET osm_id=-1346376 WHERE id=1429; -- Zipacón / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1353177 WHERE id=803; -- Nariño / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11347687 WHERE id=814; -- Nobsa / Boyacá
      UPDATE msip_municipio SET osm_id=-11909599 WHERE id=586; -- Guatavita / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1309991 WHERE id=1148; -- San Pedro de Cartago / Nariño
      UPDATE msip_municipio SET osm_id=-1423508 WHERE id=919; -- Pinchote / Santander
      UPDATE msip_municipio SET osm_id=-1307022 WHERE id=655; -- La Pintada / Antioquia
      UPDATE msip_municipio SET osm_id=-1306571 WHERE id=82; -- Briceño / Boyacá
      UPDATE msip_municipio SET osm_id=-11374024 WHERE id=556; -- Granada / Meta
      UPDATE msip_municipio SET osm_id=-1345071 WHERE id=286; -- Ciénega / Boyacá
      UPDATE msip_municipio SET osm_id=-1315083 WHERE id=635; -- La Belleza / Santander
      UPDATE msip_municipio SET osm_id=-1412932 WHERE id=135; -- Cachipay / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1423471 WHERE id=577; -- Guapotá / Santander
      UPDATE msip_municipio SET osm_id=-1353191 WHERE id=581; -- Guataquí / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1315451 WHERE id=582; -- Guavatá / Santander
      UPDATE msip_municipio SET osm_id=-1355978 WHERE id=1280; -- Tena / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1356450 WHERE id=569; -- Guadalupe / Huila
      UPDATE msip_municipio SET osm_id=-1356410 WHERE id=570; -- Guamo / Tolima
      UPDATE msip_municipio SET osm_id=-1315416 WHERE id=817; -- Nuevo Colón / Boyacá
      UPDATE msip_municipio SET osm_id=-1438575 WHERE id=565; -- Guamal / Meta
      UPDATE msip_municipio SET osm_id=-11329285 WHERE id=574; -- Guaitarilla / Nariño
      UPDATE msip_municipio SET osm_id=-1454221 WHERE id=802; -- Nariño / Nariño
      UPDATE msip_municipio SET osm_id=-1315448 WHERE id=628; -- Jesús María / Santander
      UPDATE msip_municipio SET osm_id=-1315426 WHERE id=1238; -- Barbosa / Santander
      UPDATE msip_municipio SET osm_id=-1345186 WHERE id=1233; -- Sora / Boyacá
      UPDATE msip_municipio SET osm_id=-1345031 WHERE id=81; -- Boyacá / Boyacá
      UPDATE msip_municipio SET osm_id=-1316179 WHERE id=619; -- Ituango / Antioquia
      UPDATE msip_municipio SET osm_id=-1344585 WHERE id=1284; -- Tenza / Boyacá
      UPDATE msip_municipio SET osm_id=-1309445 WHERE id=599; -- Herveo / Tolima
      UPDATE msip_municipio SET osm_id=-1343387 WHERE id=168; -- California / Santander
      UPDATE msip_municipio SET osm_id=-1353254 WHERE id=627; -- Jerusalén / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1351880 WHERE id=389; -- Cuítiva / Boyacá
      UPDATE msip_municipio SET osm_id=-1342321 WHERE id=1241; -- Suan / Atlántico
      UPDATE msip_municipio SET osm_id=-1315082 WHERE id=652; -- Landázuri / Santander
      UPDATE msip_municipio SET osm_id=-1306576 WHERE id=1343; -- Tununguá / Boyacá
      UPDATE msip_municipio SET osm_id=-1473614 WHERE id=610; -- Inzá / Cauca
      UPDATE msip_municipio SET osm_id=-1343141 WHERE id=633; -- Juradó / Chocó
      UPDATE msip_municipio SET osm_id=-1309986 WHERE id=1341; -- Belén / Nariño
      UPDATE msip_municipio SET osm_id=-1346504 WHERE id=500; -- Gachancipá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1326254 WHERE id=1034; -- San Cristóbal / Bolívar
      UPDATE msip_municipio SET osm_id=-1341558 WHERE id=1046; -- Sabanagrande / Atlántico
      UPDATE msip_municipio SET osm_id=-1315080 WHERE id=629; -- Jordán / Santander
      UPDATE msip_municipio SET osm_id=-1423466 WHERE id=885; -- Panqueba / Boyacá
      UPDATE msip_municipio SET osm_id=-1315415 WHERE id=1253; -- Sutatenza / Boyacá
      UPDATE msip_municipio SET osm_id=-408356 WHERE id=758; -- Marmato / Caldas
      UPDATE msip_municipio SET osm_id=-1315410 WHERE id=579; -- Guateque / Boyacá
      UPDATE msip_municipio SET osm_id=-1717033 WHERE id=792; -- Morales / Cauca
      UPDATE msip_municipio SET osm_id=-1473589 WHERE id=694; -- La Unión / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1423483 WHERE id=800; -- Mutiscua / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1348326 WHERE id=695; -- La Virginia / Risaralda
      UPDATE msip_municipio SET osm_id=-1315386 WHERE id=620; -- Iza / Boyacá
      UPDATE msip_municipio SET osm_id=-1343739 WHERE id=706; -- Lebrija / Santander
      UPDATE msip_municipio SET osm_id=-1342111 WHERE id=712; -- La Montañita / Caquetá
      UPDATE msip_municipio SET osm_id=-1314684 WHERE id=728; -- La Jagua del Pilar / La Guajira
      UPDATE msip_municipio SET osm_id=-1352343 WHERE id=1333; -- Tópaga / Boyacá
      UPDATE msip_municipio SET osm_id=-11890104 WHERE id=1035; -- La Paz / Cesar
      UPDATE msip_municipio SET osm_id=-1354480 WHERE id=662; -- La Peña / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1324935 WHERE id=604; -- La Apartada / Córdoba
      UPDATE msip_municipio SET osm_id=-1350650 WHERE id=693; -- La Unión / Sucre
      UPDATE msip_municipio SET osm_id=-1354753 WHERE id=815; -- Nocaima / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11313213 WHERE id=386; -- Nunchía / Casanare
      UPDATE msip_municipio SET osm_id=-1448418 WHERE id=745; -- Mahates / Bolívar
      UPDATE msip_municipio SET osm_id=-1335174 WHERE id=36; -- Leticia / Amazonas
      UPDATE msip_municipio SET osm_id=-1351873 WHERE id=872; -- Pailitas / Cesar
      UPDATE msip_municipio SET osm_id=-1343470 WHERE id=724; -- Los Córdobas / Córdoba
      UPDATE msip_municipio SET osm_id=-1309451 WHERE id=716; -- Líbano / Tolima
      UPDATE msip_municipio SET osm_id=-7229628 WHERE id=589; -- Mesetas / Meta
      UPDATE msip_municipio SET osm_id=-11333474 WHERE id=580; -- Gualmatán / Nariño
      UPDATE msip_municipio SET osm_id=-11372130 WHERE id=584; -- Mapiripán / Meta
      UPDATE msip_municipio SET osm_id=-1326231 WHERE id=740; -- Magangué / Bolívar
      UPDATE msip_municipio SET osm_id=-1315388 WHERE id=782; -- Mongua / Boyacá
      UPDATE msip_municipio SET osm_id=-1343464 WHERE id=736; -- Majagual / Sucre
      UPDATE msip_municipio SET osm_id=-1307001 WHERE id=742; -- Medio Baudó / Chocó
      UPDATE msip_municipio SET osm_id=-1423463 WHERE id=908; -- Pedraza / Magdalena
      UPDATE msip_municipio SET osm_id=-1342922 WHERE id=805; -- Natagaima / Tolima
      UPDATE msip_municipio SET osm_id=-1343468 WHERE id=793; -- Morroa / Sucre
      UPDATE msip_municipio SET osm_id=-11317170 WHERE id=177; -- Maní / Casanare
      UPDATE msip_municipio SET osm_id=-1310023 WHERE id=900; -- Piamonte / Cauca
      UPDATE msip_municipio SET osm_id=-1343031 WHERE id=765; -- Melgar / Tolima
      UPDATE msip_municipio SET osm_id=-1343264 WHERE id=38; -- Medellín / Antioquia
      UPDATE msip_municipio SET osm_id=-11890015 WHERE id=760; -- Manaure Balcón del Cesar / Cesar
      UPDATE msip_municipio SET osm_id=-1343449 WHERE id=41; -- Montería / Córdoba
      UPDATE msip_municipio SET osm_id=-1723761 WHERE id=773; -- Miranda / Cauca
      UPDATE msip_municipio SET osm_id=-1307018 WHERE id=804; -- Nariño / Antioquia
      UPDATE msip_municipio SET osm_id=-1309481 WHERE id=790; -- Montenegro / Quindío
      UPDATE msip_municipio SET osm_id=-1343261 WHERE id=796; -- Murindó / Antioquia
      UPDATE msip_municipio SET osm_id=-1311752 WHERE id=575; -- Orito / Putumayo
      UPDATE msip_municipio SET osm_id=-1413051 WHERE id=794; -- Mosquera / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1309485 WHERE id=769; -- Medio San Juan / Chocó
      UPDATE msip_municipio SET osm_id=-1305052 WHERE id=875; -- Paime / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1343461 WHERE id=811; -- Necoclí / Antioquia
      UPDATE msip_municipio SET osm_id=-11323562 WHERE id=44; -- Pasto / Nariño
      UPDATE msip_municipio SET osm_id=-1423459 WHERE id=823; -- Ocaña / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1345185 WHERE id=797; -- Motavita / Boyacá
      UPDATE msip_municipio SET osm_id=-2182325 WHERE id=799; -- Mutatá / Antioquia
      UPDATE msip_municipio SET osm_id=-1309474 WHERE id=816; -- Nóvita / Chocó
      UPDATE msip_municipio SET osm_id=-1354768 WHERE id=891; -- Pandi / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1423469 WHERE id=883; -- Pamplonita / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1722356 WHERE id=192; -- Caloto / Cauca
      UPDATE msip_municipio SET osm_id=-1308644 WHERE id=871; -- Paipa / Boyacá
      UPDATE msip_municipio SET osm_id=-1343414 WHERE id=877; -- Pamplona / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1351879 WHERE id=911; -- Pesca / Boyacá
      UPDATE msip_municipio SET osm_id=-1309478 WHERE id=917; -- Pijao / Quindío
      UPDATE msip_municipio SET osm_id=-1423479 WHERE id=931; -- Plato / Magdalena
      UPDATE msip_municipio SET osm_id=-1324955 WHERE id=859; -- Ovejas / Sucre
      UPDATE msip_municipio SET osm_id=-1342118 WHERE id=940; -- Puerto Asís / Putumayo
      UPDATE msip_municipio SET osm_id=-4060568 WHERE id=946; -- Puerto Guzmán / Putumayo
      UPDATE msip_municipio SET osm_id=-11372668 WHERE id=974; -- Puerto Rico / Meta
      UPDATE msip_municipio SET osm_id=-1314689 WHERE id=945; -- Puebloviejo / Magdalena
      UPDATE msip_municipio SET osm_id=-11332179 WHERE id=951; -- Puerres / Nariño
      UPDATE msip_municipio SET osm_id=-1335183 WHERE id=952; -- Puerto Colombia / Atlántico
      UPDATE msip_municipio SET osm_id=-4047009 WHERE id=980; -- Quimbaya / Quindío
      UPDATE msip_municipio SET osm_id=-1343138 WHERE id=48; -- Quibdó / Chocó
      UPDATE msip_municipio SET osm_id=-1314974 WHERE id=901; -- Páramo / Santander
      UPDATE msip_municipio SET osm_id=-1326253 WHERE id=966; -- Regidor / Bolívar
      UPDATE msip_municipio SET osm_id=-11890151 WHERE id=927; -- Pivijay / Magdalena
      UPDATE msip_municipio SET osm_id=-11892166 WHERE id=344; -- Convención / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1343726 WHERE id=367; -- Coromoro / Santander
      UPDATE msip_municipio SET osm_id=-4046886 WHERE id=401; -- Dagua / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1306998 WHERE id=967; -- Río Iró / Chocó
      UPDATE msip_municipio SET osm_id=-1311754 WHERE id=1023; -- Ricaurte / Nariño
      UPDATE msip_municipio SET osm_id=-1309466 WHERE id=388; -- Cunday / Tolima
      UPDATE msip_municipio SET osm_id=-11892018 WHERE id=1094; -- San Calixto / Norte de Santander
      UPDATE msip_municipio SET osm_id=-408548 WHERE id=1082; -- San José / Caldas
      UPDATE msip_municipio SET osm_id=-1460028 WHERE id=1021; -- San José del Fragua / Caquetá
      UPDATE msip_municipio SET osm_id=-1324948 WHERE id=1176; -- San Juan de Betulia / Sucre
      UPDATE msip_municipio SET osm_id=-1344654 WHERE id=1045; -- Saboyá / Boyacá
      UPDATE msip_municipio SET osm_id=-1343463 WHERE id=1093; -- San Andrés de Sotavento / Córdoba
      UPDATE msip_municipio SET osm_id=-1421703 WHERE id=1054; -- San Bernardo / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1328503 WHERE id=1086; -- San Martín de Loba / Bolívar
      UPDATE msip_municipio SET osm_id=-1324924 WHERE id=1112; -- San Benito Abad / Sucre
      UPDATE msip_municipio SET osm_id=-1324954 WHERE id=1180; -- San Marcos / Sucre
      UPDATE msip_municipio SET osm_id=-1350644 WHERE id=1076; -- San Luis / Antioquia
      UPDATE msip_municipio SET osm_id=-1314678 WHERE id=1208; -- Sitionuevo / Magdalena
      UPDATE msip_municipio SET osm_id=-1344478 WHERE id=1215; -- Soatá / Boyacá
      UPDATE msip_municipio SET osm_id=-1343129 WHERE id=1073; -- Salazar / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1314631 WHERE id=49; -- Riohacha / La Guajira
      UPDATE msip_municipio SET osm_id=-1459999 WHERE id=1217; -- San Francisco / Putumayo
      UPDATE msip_municipio SET osm_id=-1354793 WHERE id=1202; -- Silvania / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11894039 WHERE id=1111; -- Samaniego / Nariño
      UPDATE msip_municipio SET osm_id=-1412959 WHERE id=1078; -- San Juan de Rioseco / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1314629 WHERE id=1058; -- San Juan del Cesar / La Guajira
      UPDATE msip_municipio SET osm_id=-11375289 WHERE id=1139; -- San Martín / Meta
      UPDATE msip_municipio SET osm_id=-1324929 WHERE id=1184; -- San Pedro / Sucre
      UPDATE msip_municipio SET osm_id=-1423519 WHERE id=1186; -- Santa Bárbara de Pinto / Magdalena
      UPDATE msip_municipio SET osm_id=-4047301 WHERE id=1039; -- Roncesvalles / Tolima
      UPDATE msip_municipio SET osm_id=-1423449 WHERE id=1191; -- Sativasur / Boyacá
      UPDATE msip_municipio SET osm_id=-1316171 WHERE id=1043; -- Sabanalarga / Antioquia
      UPDATE msip_municipio SET osm_id=-1631618 WHERE id=1263; -- Sucre / Cauca
      UPDATE msip_municipio SET osm_id=-1423489 WHERE id=1205; -- Simacota / Santander
      UPDATE msip_municipio SET osm_id=-1326226 WHERE id=466; -- El Peñón / Bolívar
      UPDATE msip_municipio SET osm_id=-1423486 WHERE id=440; -- El Paso / Cesar
      UPDATE msip_municipio SET osm_id=-1311756 WHERE id=452; -- El Tablón de Gómez / Nariño
      UPDATE msip_municipio SET osm_id=-1473586 WHERE id=417; -- El Águila / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1341569 WHERE id=1224; -- Soledad / Atlántico
      UPDATE msip_municipio SET osm_id=-1448411 WHERE id=1062; -- San Jacinto / Bolívar
      UPDATE msip_municipio SET osm_id=-1343451 WHERE id=1107; -- San Bernardo del Viento / Córdoba
      UPDATE msip_municipio SET osm_id=-1326261 WHERE id=1057; -- San Fernando / Bolívar
      UPDATE msip_municipio SET osm_id=-1315081 WHERE id=1115; -- San Gil / Santander
      UPDATE msip_municipio SET osm_id=-1343269 WHERE id=1232; -- Sopetrán / Antioquia
      UPDATE msip_municipio SET osm_id=-11317288 WHERE id=713; -- Tauramena / Casanare
      UPDATE msip_municipio SET osm_id=-11891928 WHERE id=1306; -- Teorama / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1315401 WHERE id=1142; -- Santa María / Boyacá
      UPDATE msip_municipio SET osm_id=-1356412 WHERE id=936; -- Pradera / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1455831 WHERE id=1209; -- Sibundoy / Putumayo
      UPDATE msip_municipio SET osm_id=-1414846 WHERE id=497; -- Fusagasugá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1344616 WHERE id=499; -- Gachantivá / Boyacá
      UPDATE msip_municipio SET osm_id=-1343472 WHERE id=963; -- Puerto Libertador / Córdoba
      UPDATE msip_municipio SET osm_id=-1740111 WHERE id=1203; -- Silvia / Cauca
      UPDATE msip_municipio SET osm_id=-1324931 WHERE id=53; -- Sincelejo / Sucre
      UPDATE msip_municipio SET osm_id=-1335178 WHERE id=1342; -- Tubará / Atlántico
      UPDATE msip_municipio SET osm_id=-1353392 WHERE id=964; -- Pulí / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1473598 WHERE id=1344; -- Tuluá / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1315429 WHERE id=1248; -- Susacón / Boyacá
      UPDATE msip_municipio SET osm_id=-1308618 WHERE id=1375; -- Venadillo / Tolima
      UPDATE msip_municipio SET osm_id=-1356149 WHERE id=1286; -- Tenjo / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1309448 WHERE id=1387; -- Villahermosa / Tolima
      UPDATE msip_municipio SET osm_id=-1345218 WHERE id=1401; -- Viotá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1473602 WHERE id=1040; -- Rosas / Cauca
      UPDATE msip_municipio SET osm_id=-1315456 WHERE id=507; -- Gámbita / Santander
      UPDATE msip_municipio SET osm_id=-1346349 WHERE id=1326; -- Tuchín / Córdoba
      UPDATE msip_municipio SET osm_id=-11909704 WHERE id=1245; -- Suesca / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1343444 WHERE id=1274; -- Tarazá / Antioquia
      UPDATE msip_municipio SET osm_id=-1423509 WHERE id=55; -- Valledupar / Cesar
      UPDATE msip_municipio SET osm_id=-11895072 WHERE id=1067; -- San Francisco / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1316178 WHERE id=1423; -- Yondó / Antioquia
      UPDATE msip_municipio SET osm_id=-1315402 WHERE id=1085; -- San Luis de Gaceno / Boyacá
      UPDATE msip_municipio SET osm_id=-1343442 WHERE id=1183; -- San Onofre / Sucre
      UPDATE msip_municipio SET osm_id=-1423502 WHERE id=1179; -- Santa Ana / Magdalena
      UPDATE msip_municipio SET osm_id=-1342096 WHERE id=613; -- Isnos / Huila
      UPDATE msip_municipio SET osm_id=-1764419 WHERE id=1360; -- Villa Rica / Cauca
      UPDATE msip_municipio SET osm_id=-1308654 WHERE id=1349; -- Tuta / Boyacá
      UPDATE msip_municipio SET osm_id=-1352374 WHERE id=1352; -- Tutazá / Boyacá
      UPDATE msip_municipio SET osm_id=-1315432 WHERE id=1219; -- Socotá / Boyacá
      UPDATE msip_municipio SET osm_id=-10688131 WHERE id=1251; -- Supatá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1448409 WHERE id=1424; -- Zambrano / Bolívar
      UPDATE msip_municipio SET osm_id=-1308616 WHERE id=710; -- Lérida / Tolima
      UPDATE msip_municipio SET osm_id=-1448416 WHERE id=759; -- María La Baja / Bolívar
      UPDATE msip_municipio SET osm_id=-1423496 WHERE id=780; -- Mogotes / Santander
      UPDATE msip_municipio SET osm_id=-1342113 WHERE id=1285; -- Tello / Huila
      UPDATE msip_municipio SET osm_id=-1324946 WHERE id=784; -- Montelíbano / Córdoba
      UPDATE msip_municipio SET osm_id=-11382140 WHERE id=39; -- Mitú / Vaupés
      UPDATE msip_municipio SET osm_id=-2460383 WHERE id=1350; -- Turbaná / Bolívar
      UPDATE msip_municipio SET osm_id=-1342126 WHERE id=42; -- Neiva / Huila
      UPDATE msip_municipio SET osm_id=-11318880 WHERE id=240; -- Monterrey / Casanare
      UPDATE msip_municipio SET osm_id=-1342483 WHERE id=1363; -- Usiacurí / Atlántico
      UPDATE msip_municipio SET osm_id=-1412856 WHERE id=810; -- Nimaima / Cundinamarca
      UPDATE msip_municipio SET osm_id=-2463270 WHERE id=812; -- Norosí / Bolívar
      UPDATE msip_municipio SET osm_id=-11893741 WHERE id=1388; -- Villa Caro / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1309441 WHERE id=1391; -- Villavieja / Huila
      UPDATE msip_municipio SET osm_id=-1464203 WHERE id=1427; -- Zarzal / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1343455 WHERE id=1132; -- San Pelayo / Córdoba
      UPDATE msip_municipio SET osm_id=-1315425 WHERE id=1190; -- Sativanorte / Boyacá
      UPDATE msip_municipio SET osm_id=-1314972 WHERE id=1258; -- Suratá / Santander
      UPDATE msip_municipio SET osm_id=-1343140 WHERE id=1028; -- Riosucio / Chocó
      UPDATE msip_municipio SET osm_id=-1423475 WHERE id=1267; -- Tamalameque / Cesar
      UPDATE msip_municipio SET osm_id=-1448412 WHERE id=1347; -- Turbaco / Bolívar
      UPDATE msip_municipio SET osm_id=-1342110 WHERE id=1072; -- Saladoblanco / Huila
      UPDATE msip_municipio SET osm_id=-1324930 WHERE id=1113; -- San Carlos / Córdoba
      UPDATE msip_municipio SET osm_id=-1342100 WHERE id=1133; -- Santa Isabel / Tolima
      UPDATE msip_municipio SET osm_id=-11894050 WHERE id=1153; -- Santacruz / Nariño
      UPDATE msip_municipio SET osm_id=-1316170 WHERE id=1194; -- Segovia / Antioquia
      UPDATE msip_municipio SET osm_id=-1343265 WHERE id=1307; -- Unguía / Chocó
      UPDATE msip_municipio SET osm_id=-4046960 WHERE id=1196; -- Sevilla / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-4060575 WHERE id=1409; -- Villagarzón / Putumayo
      UPDATE msip_municipio SET osm_id=-1315465 WHERE id=1390; -- Villanueva / Santander
      UPDATE msip_municipio SET osm_id=-1309465 WHERE id=1396; -- Villarrica / Tolima
      UPDATE msip_municipio SET osm_id=-1343450 WHERE id=1244; -- Sucre / Sucre
      UPDATE msip_municipio SET osm_id=-1344500 WHERE id=1324; -- Toca / Boyacá
      UPDATE msip_municipio SET osm_id=-1473590 WHERE id=1338; -- Toro / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1343460 WHERE id=1348; -- Turbo / Antioquia
      UPDATE msip_municipio SET osm_id=-1307280 WHERE id=1392; -- Vigía del Fuerte / Antioquia
      UPDATE msip_municipio SET osm_id=-1448410 WHERE id=1394; -- Villanueva / Bolívar
      UPDATE msip_municipio SET osm_id=-1307282 WHERE id=1362; -- Urrao / Antioquia
      UPDATE msip_municipio SET osm_id=-1324932 WHERE id=1368; -- Valencia / Córdoba
      UPDATE msip_municipio SET osm_id=-1473592 WHERE id=1380; -- Versalles / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1345222 WHERE id=1410; -- Yacopí / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1343462 WHERE id=944; -- Pueblo Nuevo / Córdoba
      UPDATE msip_municipio SET osm_id=-1316176 WHERE id=912; -- Peque / Antioquia
      UPDATE msip_municipio SET osm_id=-1343446 WHERE id=920; -- Pinillos / Bolívar
      UPDATE msip_municipio SET osm_id=-1335186 WHERE id=934; -- Ponedera / Atlántico
      UPDATE msip_municipio SET osm_id=-1422326 WHERE id=949; -- Puerto Boyacá / Boyacá
      UPDATE msip_municipio SET osm_id=-1343456 WHERE id=957; -- Puerto Escondido / Córdoba
      UPDATE msip_municipio SET osm_id=-1342106 WHERE id=977; -- Puerto Rico / Caquetá
      UPDATE msip_municipio SET osm_id=-1344976 WHERE id=965; -- Quípama / Boyacá
      UPDATE msip_municipio SET osm_id=-1717036 WHERE id=1259; -- Suárez / Cauca
      UPDATE msip_municipio SET osm_id=-1308656 WHERE id=1234; -- Sotaquirá / Boyacá
      UPDATE msip_municipio SET osm_id=-1343280 WHERE id=1416; -- Yarumal / Antioquia
      UPDATE msip_municipio SET osm_id=-1740146 WHERE id=1334; -- Toribío / Cauca
      UPDATE msip_municipio SET osm_id=-1422330 WHERE id=1364; -- Útica / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11329266 WHERE id=1351; -- Túquerres / Nariño
      UPDATE msip_municipio SET osm_id=-1354486 WHERE id=1379; -- Vergara / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11896946 WHERE id=1397; -- Villa del Rosario / Norte de Santander
      UPDATE msip_municipio SET osm_id=-408546 WHERE id=1400; -- Viterbo / Caldas
      UPDATE msip_municipio SET osm_id=-1554296 WHERE id=1421; -- Yotoco / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1343027 WHERE id=439; -- El Litoral del San Juan / Chocó
      UPDATE msip_municipio SET osm_id=-1356337 WHERE id=468; -- Espinal / Tolima
      UPDATE msip_municipio SET osm_id=-1315447 WHERE id=473; -- Florián / Santander
      UPDATE msip_municipio SET osm_id=-1309447 WHERE id=487; -- Fresno / Tolima
      UPDATE msip_municipio SET osm_id=-1314677 WHERE id=492; -- Fundación / Magdalena
      UPDATE msip_municipio SET osm_id=-1423447 WHERE id=649; -- La Gloria / Cesar
      UPDATE msip_municipio SET osm_id=-1354447 WHERE id=658; -- La Palma / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1324927 WHERE id=718; -- Lorica / Córdoba
      UPDATE msip_municipio SET osm_id=-1341567 WHERE id=746; -- Malambo / Atlántico
      UPDATE msip_municipio SET osm_id=-1341297 WHERE id=753; -- Margarita / Bolívar
      UPDATE msip_municipio SET osm_id=-1343447 WHERE id=849; -- Moñitos / Córdoba
      UPDATE msip_municipio SET osm_id=-1344997 WHERE id=801; -- Muzo / Boyacá
      UPDATE msip_municipio SET osm_id=-1413207 WHERE id=902; -- Pasca / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1423517 WHERE id=924; -- Pelaya / Cesar
      UPDATE msip_municipio SET osm_id=-11331777 WHERE id=935; -- Potosí / Nariño
      UPDATE msip_municipio SET osm_id=-1423507 WHERE id=943; -- Pueblo Bello / Cesar
      UPDATE msip_municipio SET osm_id=-11319147 WHERE id=542; -- Sabanalarga / Casanare
      UPDATE msip_municipio SET osm_id=-11890197 WHERE id=1070; -- Sabanas de San Ángel / Magdalena
      UPDATE msip_municipio SET osm_id=-1341537 WHERE id=1047; -- Sabanalarga / Atlántico
      UPDATE msip_municipio SET osm_id=-1448407 WHERE id=1066; -- San Juan Nepomuceno / Bolívar
      UPDATE msip_municipio SET osm_id=-11313177 WHERE id=585; -- San Luis de Palenque / Casanare
      UPDATE msip_municipio SET osm_id=-1473597 WHERE id=1096; -- San Pedro / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1423487 WHERE id=1177; -- San Zenón / Magdalena
      UPDATE msip_municipio SET osm_id=-4062807 WHERE id=1103; -- Santa Catalina / Bolívar
      UPDATE msip_municipio SET osm_id=-1473607 WHERE id=1315; -- Timbío / Cauca
      UPDATE msip_municipio SET osm_id=-1345217 WHERE id=1325; -- Tocaima / Cundinamarca
      UPDATE msip_municipio SET osm_id=-7229667 WHERE id=630; -- Uribe / Meta
      UPDATE msip_municipio SET osm_id=-1324947 WHERE id=1365; -- Valdivia / Antioquia
      UPDATE msip_municipio SET osm_id=-1315417 WHERE id=709; -- Villa de Leyva / Boyacá
      UPDATE msip_municipio SET osm_id=-11319705 WHERE id=47; -- Puerto Carreño / Vichada
      UPDATE msip_municipio SET osm_id=-1438576 WHERE id=443; -- Paz de Ariporo / Casanare
      UPDATE msip_municipio SET osm_id=-1314676 WHERE id=1361; -- Uribia / La Guajira
      UPDATE msip_municipio SET osm_id=-11381007 WHERE id=1084; -- Taraira / Vaupés
      UPDATE msip_municipio SET osm_id=-11319669 WHERE id=887; -- La Primavera / Vichada
      UPDATE msip_municipio SET osm_id=-1438572 WHERE id=141; -- Hato Corozal / Casanare
      UPDATE msip_municipio SET osm_id=-1311768 WHERE id=86; -- Buenaventura / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-11311667 WHERE id=1193; -- Saravena / Arauca
      UPDATE msip_municipio SET osm_id=-11311666 WHERE id=1279; -- Tame / Arauca
      UPDATE msip_municipio SET osm_id=-1335173 WHERE id=535; -- Altos del Rosario / Bolívar
      UPDATE msip_municipio SET osm_id=-1344984 WHERE id=699; -- La Victoria / Boyacá
      UPDATE msip_municipio SET osm_id=-1311674 WHERE id=611; -- Ipiales / Nariño
      UPDATE msip_municipio SET osm_id=-11333699 WHERE id=969; -- Pupiales / Nariño
      UPDATE msip_municipio SET osm_id=-11333702 WHERE id=371; -- Aldana / Nariño
      UPDATE msip_municipio SET osm_id=-11323982 WHERE id=614; -- Ancuya / Nariño
      UPDATE msip_municipio SET osm_id=-1311769 WHERE id=1125; -- Sandoná / Nariño
      UPDATE msip_municipio SET osm_id=-1454218 WHERE id=647; -- La Florida / Nariño
      UPDATE msip_municipio SET osm_id=-1342114 WHERE id=942; -- Puerto Caicedo / Putumayo
      UPDATE msip_municipio SET osm_id=-1453542 WHERE id=457; -- El Tambo / Nariño
      UPDATE msip_municipio SET osm_id=-1320710 WHERE id=445; -- El Peñol / Nariño
      UPDATE msip_municipio SET osm_id=-4046719 WHERE id=1317; -- Timbiquí / Cauca
      UPDATE msip_municipio SET osm_id=-1309990 WHERE id=664; -- La Unión / Nariño
      UPDATE msip_municipio SET osm_id=-1309992 WHERE id=642; -- La Cruz / Nariño
      UPDATE msip_municipio SET osm_id=-1554702 WHERE id=77; -- Bolívar / Cauca
      UPDATE msip_municipio SET osm_id=-1554703 WHERE id=770; -- Mercaderes / Cauca
      UPDATE msip_municipio SET osm_id=-1309439 WHERE id=988; -- Acevedo / Huila
      UPDATE msip_municipio SET osm_id=-1554704 WHERE id=661; -- La Vega / Cauca
      UPDATE msip_municipio SET osm_id=-1694626 WHERE id=1211; -- Balboa / Cauca
      UPDATE msip_municipio SET osm_id=-1320723 WHERE id=704; -- Leiva / Nariño
      UPDATE msip_municipio SET osm_id=-1356413 WHERE id=372; -- Almaguer / Cauca
      UPDATE msip_municipio SET osm_id=-1342104 WHERE id=420; -- Elías / Huila
      UPDATE msip_municipio SET osm_id=-1310024 WHERE id=1175; -- Santa Rosa / Cauca
      UPDATE msip_municipio SET osm_id=-1706524 WHERE id=46; -- Popayán / Cauca
      UPDATE msip_municipio SET osm_id=-1342105 WHERE id=641; -- La Argentina / Huila
      UPDATE msip_municipio SET osm_id=-1457952 WHERE id=659; -- La Plata / Huila
      UPDATE msip_municipio SET osm_id=-1459033 WHERE id=798; -- Morelia / Caquetá
      UPDATE msip_municipio SET osm_id=-1309450 WHERE id=506; -- Garzón / Huila
      UPDATE msip_municipio SET osm_id=-1342094 WHERE id=544; -- Gigante / Huila
      UPDATE msip_municipio SET osm_id=-1459974 WHERE id=874; -- Paicol / Huila
      UPDATE msip_municipio SET osm_id=-1459941 WHERE id=806; -- Nátaga / Huila
      UPDATE msip_municipio SET osm_id=-1342112 WHERE id=612; -- Íquira / Huila
      UPDATE msip_municipio SET osm_id=-1342122 WHERE id=1412; -- Yaguará / Huila
      UPDATE msip_municipio SET osm_id=-1342115 WHERE id=1029; -- Rivera / Huila
      UPDATE msip_municipio SET osm_id=-11372667 WHERE id=605; -- La Macarena / Meta
      UPDATE msip_municipio SET osm_id=-1320707 WHERE id=28; -- Cali / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1473641 WHERE id=166; -- Candelaria / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1320727 WHERE id=1422; -- Yumbo / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1473616 WHERE id=563; -- Guacarí / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1473605 WHERE id=1019; -- Restrepo / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1554306 WHERE id=1385; -- Vijes / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1343024 WHERE id=717; -- Lloró / Chocó
      UPDATE msip_municipio SET osm_id=-1309484 WHERE id=234; -- Cértegui / Chocó
      UPDATE msip_municipio SET osm_id=-1343033 WHERE id=774; -- Mistrató / Risaralda
      UPDATE msip_municipio SET osm_id=-1307008 WHERE id=1192; -- Bagadó / Chocó
      UPDATE msip_municipio SET osm_id=-1342093 WHERE id=929; -- Planadas / Tolima
      UPDATE msip_municipio SET osm_id=-1308612 WHERE id=1098; -- Saldaña / Tolima
      UPDATE msip_municipio SET osm_id=-1343039 WHERE id=19; -- Armenia / Quindío
      UPDATE msip_municipio SET osm_id=-1460512 WHERE id=197; -- Cartago / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1460505 WHERE id=325; -- Alcalá / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1311763 WHERE id=1358; -- Ulloa / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-3594131 WHERE id=475; -- Filandia / Quindío
      UPDATE msip_municipio SET osm_id=-1343043 WHERE id=45; -- Pereira / Risaralda
      UPDATE msip_municipio SET osm_id=-1347548 WHERE id=255; -- Dosquebradas / Risaralda
      UPDATE msip_municipio SET osm_id=-1309464 WHERE id=333; -- Coello / Tolima
      UPDATE msip_municipio SET osm_id=-1353143 WHERE id=547; -- Girardot / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1353060 WHERE id=1022; -- Ricaurte / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1354625 WHERE id=809; -- Nilo / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1309479 WHERE id=755; -- Marsella / Risaralda
      UPDATE msip_municipio SET osm_id=-408116 WHERE id=259; -- Chinchiná / Caldas
      UPDATE msip_municipio SET osm_id=-408547 WHERE id=725; -- Anserma / Caldas
      UPDATE msip_municipio SET osm_id=-385843 WHERE id=1393; -- Villamaría / Caldas
      UPDATE msip_municipio SET osm_id=-407428 WHERE id=764; -- Marulanda / Caldas
      UPDATE msip_municipio SET osm_id=-410729 WHERE id=474; -- Filadelfia / Caldas
      UPDATE msip_municipio SET osm_id=-1307015 WHERE id=623; -- Jardín / Antioquia
      UPDATE msip_municipio SET osm_id=-408354 WHERE id=1024; -- Riosucio / Caldas
      UPDATE msip_municipio SET osm_id=-410728 WHERE id=846; -- Aranzazu / Caldas
      UPDATE msip_municipio SET osm_id=-410730 WHERE id=148; -- Aguadas / Caldas
      UPDATE msip_municipio SET osm_id=-410731 WHERE id=1060; -- Salamina / Caldas
      UPDATE msip_municipio SET osm_id=-410727 WHERE id=869; -- Pácora / Caldas
      UPDATE msip_municipio SET osm_id=-407429 WHERE id=909; -- Pensilvania / Caldas
      UPDATE msip_municipio SET osm_id=-1412596 WHERE id=573; -- Guaduas / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1342098 WHERE id=601; -- Honda / Tolima
      UPDATE msip_municipio SET osm_id=-1422327 WHERE id=950; -- Puerto Salgar / Cundinamarca
      UPDATE msip_municipio SET osm_id=-407425 WHERE id=645; -- La Dorada / Caldas
      UPDATE msip_municipio SET osm_id=-1414794 WHERE id=893; -- Arbeláez / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1413003 WHERE id=425; -- El Colegio / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1345091 WHERE id=265; -- Chipaque / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1355929 WHERE id=653; -- La Mesa / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1355693 WHERE id=288; -- Albán / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1413026 WHERE id=739; -- Madrid / Cundinamarca
      UPDATE msip_municipio SET osm_id=-8347238 WHERE id=636; -- La Calera / Cundinamarca
      UPDATE msip_municipio SET osm_id=-10688528 WHERE id=578; -- Guasca / Cundinamarca
      UPDATE msip_municipio SET osm_id=-10688529 WHERE id=1225; -- Sopó / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1356460 WHERE id=751; -- Medina / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1356876 WHERE id=895; -- Paratebueno / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11371698 WHERE id=387; -- Cumaral / Meta
      UPDATE msip_municipio SET osm_id=-1421709 WHERE id=482; -- Fómeque / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1438581 WHERE id=1018; -- Restrepo / Meta
      UPDATE msip_municipio SET osm_id=-11909530 WHERE id=632; -- Junín / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1422331 WHERE id=757; -- Maripí / Boyacá
      UPDATE msip_municipio SET osm_id=-1346489 WHERE id=1195; -- Sesquilé / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11897354 WHERE id=750; -- Manta / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1346486 WHERE id=1395; -- Villapinzón / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1346475 WHERE id=385; -- Cucunubá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-10688530 WHERE id=708; -- Lenguazaque / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11895070 WHERE id=560; -- Guachetá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1315406 WHERE id=1013; -- Ráquira / Boyacá
      UPDATE msip_municipio SET osm_id=-1315407 WHERE id=1051; -- Samacá / Boyacá
      UPDATE msip_municipio SET osm_id=-1315409 WHERE id=1048; -- Sáchica / Boyacá
      UPDATE msip_municipio SET osm_id=-1353543 WHERE id=1309; -- Tibaná / Boyacá
      UPDATE msip_municipio SET osm_id=-1351876 WHERE id=985; -- Ramiriquí / Boyacá
      UPDATE msip_municipio SET osm_id=-1344159 WHERE id=282; -- Chivatá / Boyacá
      UPDATE msip_municipio SET osm_id=-11317213 WHERE id=59; -- Aguazul / Casanare
      UPDATE msip_municipio SET osm_id=-1315389 WHERE id=639; -- Labranzagrande / Boyacá
      UPDATE msip_municipio SET osm_id=-11313242 WHERE id=58; -- Yopal / Casanare
      UPDATE msip_municipio SET osm_id=-1307289 WHERE id=1049; -- Salgar / Antioquia
      UPDATE msip_municipio SET osm_id=-1307291 WHERE id=139; -- Caicedo / Antioquia
      UPDATE msip_municipio SET osm_id=-1307005 WHERE id=1269; -- Támesis / Antioquia
      UPDATE msip_municipio SET osm_id=-1307016 WHERE id=300; -- Abejorral / Antioquia
      UPDATE msip_municipio SET osm_id=-1307287 WHERE id=486; -- Fredonia / Antioquia
      UPDATE msip_municipio SET osm_id=-1307234 WHERE id=1116; -- Santa Bárbara / Antioquia
      UPDATE msip_municipio SET osm_id=-1307235 WHERE id=692; -- La Unión / Antioquia
      UPDATE msip_municipio SET osm_id=-1350586 WHERE id=785; -- Montebello / Antioquia
      UPDATE msip_municipio SET osm_id=-1307285 WHERE id=1318; -- Titiribí / Antioquia
      UPDATE msip_municipio SET osm_id=-1307261 WHERE id=536; -- Amagá / Antioquia
      UPDATE msip_municipio SET osm_id=-1343267 WHERE id=752; -- Anzá / Antioquia
      UPDATE msip_municipio SET osm_id=-1343271 WHERE id=416; -- Ebéjico / Antioquia
      UPDATE msip_municipio SET osm_id=-1307238 WHERE id=634; -- La Ceja / Antioquia
      UPDATE msip_municipio SET osm_id=-3947503 WHERE id=1026; -- Rionegro / Antioquia
      UPDATE msip_municipio SET osm_id=-1307242 WHERE id=1151; -- El Santuario / Antioquia
      UPDATE msip_municipio SET osm_id=-1348660 WHERE id=1059; -- San Francisco / Antioquia
      UPDATE msip_municipio SET osm_id=-407427 WHERE id=819; -- Norcasia / Caldas
      UPDATE msip_municipio SET osm_id=-1307009 WHERE id=1221; -- Sonsón / Antioquia
      UPDATE msip_municipio SET osm_id=-1307272 WHERE id=567; -- Guarne / Antioquia
      UPDATE msip_municipio SET osm_id=-1343285 WHERE id=754; -- Marinilla / Antioquia
      UPDATE msip_municipio SET osm_id=-1307240 WHERE id=1055; -- San Carlos / Antioquia
      UPDATE msip_municipio SET osm_id=-1343262 WHERE id=727; -- Santa Fé de Antioquia / Antioquia
      UPDATE msip_municipio SET osm_id=-1307239 WHERE id=1087; -- San Rafael / Antioquia
      UPDATE msip_municipio SET osm_id=-1343276 WHERE id=1065; -- San Jerónimo / Antioquia
      UPDATE msip_municipio SET osm_id=-1307241 WHERE id=968; -- Puerto Nare / Antioquia
      UPDATE msip_municipio SET osm_id=-1307262 WHERE id=1404; -- Bello / Antioquia
      UPDATE msip_municipio SET osm_id=-1307257 WHERE id=1372; -- Belmira / Antioquia
      UPDATE msip_municipio SET osm_id=-1316169 WHERE id=1052; -- San Andrés de Cuerquía / Antioquia
      UPDATE msip_municipio SET osm_id=-1307268 WHERE id=714; -- Liborina / Antioquia
      UPDATE msip_municipio SET osm_id=-1307265 WHERE id=462; -- Entrerríos / Antioquia
      UPDATE msip_municipio SET osm_id=-1307290 WHERE id=1271; -- Barbosa / Antioquia
      UPDATE msip_municipio SET osm_id=-1316184 WHERE id=1015; -- Remedios / Antioquia
      UPDATE msip_municipio SET osm_id=-1307288 WHERE id=961; -- Puerto Berrío / Antioquia
      UPDATE msip_municipio SET osm_id=-1316177 WHERE id=550; -- Amalfi / Antioquia
      UPDATE msip_municipio SET osm_id=-1316174 WHERE id=1371; -- Vegachí / Antioquia
      UPDATE msip_municipio SET osm_id=-1422324 WHERE id=858; -- Otanche / Boyacá
      UPDATE msip_municipio SET osm_id=-1307292 WHERE id=1097; -- San Roque / Antioquia
      UPDATE msip_municipio SET osm_id=-1307266 WHERE id=732; -- Maceo / Antioquia
      UPDATE msip_municipio SET osm_id=-1307259 WHERE id=552; -- Gómez Plata / Antioquia
      UPDATE msip_municipio SET osm_id=-1306567 WHERE id=897; -- Pauna / Boyacá
      UPDATE msip_municipio SET osm_id=-1350635 WHERE id=193; -- Caracolí / Antioquia
      UPDATE msip_municipio SET osm_id=-1343284 WHERE id=343; -- Concepción / Antioquia
      UPDATE msip_municipio SET osm_id=-1345187 WHERE id=1120; -- San Pablo de Borbur / Boyacá
      UPDATE msip_municipio SET osm_id=-1307286 WHERE id=349; -- Alejandría / Antioquia
      UPDATE msip_municipio SET osm_id=-1324922 WHERE id=557; -- Guadalupe / Antioquia
      UPDATE msip_municipio SET osm_id=-1307258 WHERE id=298; -- Cisneros / Antioquia
      UPDATE msip_municipio SET osm_id=-1315460 WHERE id=1378; -- Vélez / Santander
      UPDATE msip_municipio SET osm_id=-1315452 WHERE id=660; -- La Paz / Santander
      UPDATE msip_municipio SET osm_id=-1344032 WHERE id=267; -- Chipatá / Santander
      UPDATE msip_municipio SET osm_id=-1315449 WHERE id=1100; -- San Benito / Santander
      UPDATE msip_municipio SET osm_id=-1423505 WHERE id=404; -- El Carmen de Chucurí / Santander
      UPDATE msip_municipio SET osm_id=-1344470 WHERE id=955; -- Puerto Parra / Santander
      UPDATE msip_municipio SET osm_id=-1315461 WHERE id=1187; -- Santa Helena del Opón / Santander
      UPDATE msip_municipio SET osm_id=-1315466 WHERE id=263; -- Chima / Santander
      UPDATE msip_municipio SET osm_id=-1423454 WHERE id=1239; -- San Martín / Cesar
      UPDATE msip_municipio SET osm_id=-1423511 WHERE id=88; -- Aguachica / Cesar
      UPDATE msip_municipio SET osm_id=-1343466 WHERE id=1014; -- Río Viejo / Bolívar
      UPDATE msip_municipio SET osm_id=-11897330 WHERE id=958; -- Puerto Wilches / Santander
      UPDATE msip_municipio SET osm_id=-11893921 WHERE id=144; -- Cáchira / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1335168 WHERE id=1138; -- Santa Rosa del Sur / Bolívar
      UPDATE msip_municipio SET osm_id=-1326251 WHERE id=775; -- Montecristo / Bolívar
      UPDATE msip_municipio SET osm_id=-11897251 WHERE id=1063; -- Sabana de Torres / Santander
      UPDATE msip_municipio SET osm_id=-1423499 WHERE id=533; -- Ábrego / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1329742 WHERE id=1204; -- Simití / Bolívar
      UPDATE msip_municipio SET osm_id=-1343441 WHERE id=791; -- Morales / Bolívar
      UPDATE msip_municipio SET osm_id=-1314969 WHERE id=1027; -- Rionegro / Santander
      UPDATE msip_municipio SET osm_id=-1423467 WHERE id=1025; -- Río de Oro / Cesar
      UPDATE msip_municipio SET osm_id=-1423491 WHERE id=1181; -- San Alberto / Cesar
      UPDATE msip_municipio SET osm_id=-1326220 WHERE id=726; -- Arenal / Bolívar
      UPDATE msip_municipio SET osm_id=-1314968 WHERE id=446; -- El Playón / Santander
      UPDATE msip_municipio SET osm_id=-1343459 WHERE id=1099; -- San Antero / Córdoba
      UPDATE msip_municipio SET osm_id=-1448420 WHERE id=1053; -- San Estanislao / Bolívar
      UPDATE msip_municipio SET osm_id=-1342464 WHERE id=1017; -- Repelón / Atlántico
      UPDATE msip_municipio SET osm_id=-1342458 WHERE id=749; -- Manatí / Atlántico
      UPDATE msip_municipio SET osm_id=-1423457 WHERE id=451; -- El Piñón / Magdalena
      UPDATE msip_municipio SET osm_id=-1350670 WHERE id=236; -- Cerro de San Antonio / Magdalena
      UPDATE msip_municipio SET osm_id=-1341514 WHERE id=191; -- Candelaria / Atlántico
      UPDATE msip_municipio SET osm_id=-1335189 WHERE id=631; -- Juan de Acosta / Atlántico
      UPDATE msip_municipio SET osm_id=-1335185 WHERE id=921; -- Piojó / Atlántico
      UPDATE msip_municipio SET osm_id=-1423461 WHERE id=1016; -- Remolino / Magdalena
      UPDATE msip_municipio SET osm_id=-1308640 WHERE id=1311; -- Tibasosa / Boyacá
      UPDATE msip_municipio SET osm_id=-1308647 WHERE id=409; -- Duitama / Boyacá
      UPDATE msip_municipio SET osm_id=-1315387 WHERE id=1147; -- Santa Rosa de Viterbo / Boyacá
      UPDATE msip_municipio SET osm_id=-11897329 WHERE id=479; -- Floresta / Boyacá
      UPDATE msip_municipio SET osm_id=-1438577 WHERE id=697; -- Támara / Casanare
      UPDATE msip_municipio SET osm_id=-11313241 WHERE id=460; -- Pore / Casanare
      UPDATE msip_municipio SET osm_id=-1315435 WHERE id=899; -- Paya / Boyacá
      UPDATE msip_municipio SET osm_id=-1438580 WHERE id=558; -- Sácama / Casanare
      UPDATE msip_municipio SET osm_id=-1314971 WHERE id=822; -- Ocamonte / Santander
      UPDATE msip_municipio SET osm_id=-1315437 WHERE id=1460; -- Boavita / Boyacá
      UPDATE msip_municipio SET osm_id=-1343697 WHERE id=219; -- Carcasí / Santander
      UPDATE msip_municipio SET osm_id=-1343118 WHERE id=721; -- Los Santos / Santander
      UPDATE msip_municipio SET osm_id=-1315077 WHERE id=860; -- Aratoca / Santander
      UPDATE msip_municipio SET osm_id=-1423510 WHERE id=239; -- Cerrito / Santander
      UPDATE msip_municipio SET osm_id=-1423465 WHERE id=345; -- Concepción / Santander
      UPDATE msip_municipio SET osm_id=-11312178 WHERE id=18; -- Arauca / Arauca
      UPDATE msip_municipio SET osm_id=-1343390 WHERE id=1201; -- Silos / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1423477 WHERE id=763; -- Matanza / Santander
      UPDATE msip_municipio SET osm_id=-1423498 WHERE id=27; -- Bucaramanga / Santander
      UPDATE msip_municipio SET osm_id=-1423481 WHERE id=245; -- Charta / Santander
      UPDATE msip_municipio SET osm_id=-1423455 WHERE id=1119; -- Santiago / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1343650 WHERE id=411; -- Durania / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1423450 WHERE id=1101; -- San Cayetano / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1423482 WHERE id=705; -- Los Patios / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1351320 WHERE id=442; -- El Tarra / Norte de Santander
      UPDATE msip_municipio SET osm_id=-2884601 WHERE id=1320; -- Tibú / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1344404 WHERE id=54; -- Tunja / Boyacá
      UPDATE msip_municipio SET osm_id=-1315418 WHERE id=1074; -- San Eduardo / Boyacá
      UPDATE msip_municipio SET osm_id=-1315421 WHERE id=1450; -- Berbeo / Boyacá
      UPDATE msip_municipio SET osm_id=-1315412 WHERE id=870; -- Páez / Boyacá
      UPDATE msip_municipio SET osm_id=-1438574 WHERE id=202; -- Chámeza / Casanare
      UPDATE msip_municipio SET osm_id=-1315393 WHERE id=789; -- Aquitania / Boyacá
      UPDATE msip_municipio SET osm_id=-1315392 WHERE id=876; -- Pajarito / Boyacá
      UPDATE msip_municipio SET osm_id=-1314675 WHERE id=741; -- Maicao / La Guajira
      UPDATE msip_municipio SET osm_id=-1314680 WHERE id=933; -- Manaure / La Guajira
      UPDATE msip_municipio SET osm_id=-408545 WHERE id=1403; -- Belalcázar / Caldas
      UPDATE msip_municipio SET osm_id=-1315085 WHERE id=80; -- Bolívar / Santander
      UPDATE msip_municipio SET osm_id=-1324945 WHERE id=1273; -- Buenavista / Córdoba
      UPDATE msip_municipio SET osm_id=-1632077 WHERE id=847; -- Argelia / Cauca
      UPDATE msip_municipio SET osm_id=-1324925 WHERE id=137; -- Caimito / Sucre
      UPDATE msip_municipio SET osm_id=-1717032 WHERE id=164; -- Cajibío / Cauca
      UPDATE msip_municipio SET osm_id=-11910119 WHERE id=85; -- Bucarasica / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1423503 WHERE id=260; -- Chitagá / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1415409 WHERE id=131; -- Cabrera / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1324933 WHERE id=1083; -- San Pedro de Urabá / Antioquia
      UPDATE msip_municipio SET osm_id=-11371494 WHERE id=136; -- Cabuyaro / Meta
      UPDATE msip_municipio SET osm_id=-1343044 WHERE id=165; -- Calarcá / Quindío
      UPDATE msip_municipio SET osm_id=-1328501 WHERE id=1064; -- San Jacinto del Cauca / Bolívar
      UPDATE msip_municipio SET osm_id=-1315385 WHERE id=115; -- Busbanzá / Boyacá
      UPDATE msip_municipio SET osm_id=-2721167 WHERE id=768; -- Becerril / Cesar
      UPDATE msip_municipio SET osm_id=-1326225 WHERE id=283; -- Cicuco / Bolívar
      UPDATE msip_municipio SET osm_id=-1423488 WHERE id=258; -- Chinácota / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1320721 WHERE id=448; -- El Rosario / Nariño
      UPDATE msip_municipio SET osm_id=-1309483 WHERE id=340; -- Condoto / Chocó
      UPDATE msip_municipio SET osm_id=-385842 WHERE id=37; -- Manizales / Caldas
      UPDATE msip_municipio SET osm_id=-1320725 WHERE id=906; -- Policarpa / Nariño
      UPDATE msip_municipio SET osm_id=-1311684 WHERE id=656; -- La Tola / Nariño
      UPDATE msip_municipio SET osm_id=-11888630 WHERE id=867; -- Pacho / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1351279 WHERE id=431; -- El Guamo / Bolívar
      UPDATE msip_municipio SET osm_id=-1324943 WHERE id=196; -- Carepa / Antioquia
      UPDATE msip_municipio SET osm_id=-1315430 WHERE id=279; -- Chita / Boyacá
      UPDATE msip_municipio SET osm_id=-1343437 WHERE id=284; -- Ciénaga de Oro / Córdoba
      UPDATE msip_municipio SET osm_id=-1455825 WHERE id=1228; -- Santiago / Putumayo
      UPDATE msip_municipio SET osm_id=-1453491 WHERE id=356; -- Contadero / Nariño
      UPDATE msip_municipio SET osm_id=-1307237 WHERE id=299; -- Cocorná / Antioquia
      UPDATE msip_municipio SET osm_id=-410733 WHERE id=807; -- Neira / Caldas
      UPDATE msip_municipio SET osm_id=-1355485 WHERE id=400; -- Cumbitara / Nariño
      UPDATE msip_municipio SET osm_id=-1308615 WHERE id=368; -- Coyaima / Tolima
      UPDATE msip_municipio SET osm_id=-1423464 WHERE id=382; -- Cucutilla / Norte de Santander
      UPDATE msip_municipio SET osm_id=-11378346 WHERE id=51; -- San José del Guaviare / Guaviare
      UPDATE msip_municipio SET osm_id=-1324917 WHERE id=364; -- Corozal / Sucre
      UPDATE msip_municipio SET osm_id=-7296967 WHERE id=363; -- Cota / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1423478 WHERE id=458; -- El Zulia / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1473600 WHERE id=1031; -- Riofrío / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1461170 WHERE id=711; -- Ansermanuevo / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1324952 WHERE id=1313; -- Tierralta / Córdoba
      UPDATE msip_municipio SET osm_id=-1307279 WHERE id=665; -- Abriaquí / Antioquia
      UPDATE msip_municipio SET osm_id=-1309482 WHERE id=543; -- Génova / Quindío
      UPDATE msip_municipio SET osm_id=-1315462 WHERE id=587; -- Güepsa / Santander
      UPDATE msip_municipio SET osm_id=-1315079 WHERE id=441; -- El Peñón / Santander
      UPDATE msip_municipio SET osm_id=-1342099 WHERE id=447; -- El Paujíl / Caquetá
      UPDATE msip_municipio SET osm_id=-1316183 WHERE id=1355; -- Uramita / Antioquia
      UPDATE msip_municipio SET osm_id=-1341497 WHERE id=190; -- Calamar / Bolívar
      UPDATE msip_municipio SET osm_id=-1473595 WHERE id=702; -- La Victoria / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1632120 WHERE id=449; -- El Tambo / Cauca
      UPDATE msip_municipio SET osm_id=-1314687 WHERE id=52; -- Santa Marta / Magdalena
      UPDATE msip_municipio SET osm_id=-11326597 WHERE id=651; -- La Llanada / Nariño
      UPDATE msip_municipio SET osm_id=-1308614 WHERE id=1030; -- Rioblanco / Tolima
      UPDATE msip_municipio SET osm_id=-1342479 WHERE id=729; -- Luruaco / Atlántico
      UPDATE msip_municipio SET osm_id=-10688531 WHERE id=734; -- Machetá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1343038 WHERE id=1075; -- San José del Palmar / Chocó
      UPDATE msip_municipio SET osm_id=-1473615 WHERE id=545; -- Ginebra / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1342108 WHERE id=34; -- Ibagué / Tolima
      UPDATE msip_municipio SET osm_id=-1307271 WHERE id=488; -- Frontino / Antioquia
      UPDATE msip_municipio SET osm_id=-1342133 WHERE id=40; -- Mocoa / Putumayo
      UPDATE msip_municipio SET osm_id=-1421811 WHERE id=592; -- Gutiérrez / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1351871 WHERE id=595; -- Hacarí / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1740108 WHERE id=621; -- Jambaló / Cauca
      UPDATE msip_municipio SET osm_id=-1459025 WHERE id=776; -- Milán / Caquetá
      UPDATE msip_municipio SET osm_id=-1438578 WHERE id=539; -- Fortul / Arauca
      UPDATE msip_municipio SET osm_id=-1473610 WHERE id=881; -- Palmira / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1472569 WHERE id=143; -- Calima / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1311686 WHERE id=795; -- Mosquera / Nariño
      UPDATE msip_municipio SET osm_id=-1343037 WHERE id=947; -- Pueblo Rico / Risaralda
      UPDATE msip_municipio SET osm_id=-1356172 WHERE id=1430; -- Zipaquirá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11891528 WHERE id=691; -- La Jagua de Ibirico / Cesar
      UPDATE msip_municipio SET osm_id=-1315411 WHERE id=509; -- Garagoa / Boyacá
      UPDATE msip_municipio SET osm_id=-1315087 WHERE id=296; -- Cimitarra / Santander
      UPDATE msip_municipio SET osm_id=-4079108 WHERE id=1405; -- Belén de Umbría / Risaralda
      UPDATE msip_municipio SET osm_id=-1350666 WHERE id=781; -- Momil / Córdoba
      UPDATE msip_municipio SET osm_id=-1473604 WHERE id=657; -- La Sierra / Cauca
      UPDATE msip_municipio SET osm_id=-1309480 WHERE id=767; -- Apía / Risaralda
      UPDATE msip_municipio SET osm_id=-1341518 WHERE id=1255; -- Baranoa / Atlántico
      UPDATE msip_municipio SET osm_id=-11909464 WHERE id=982; -- Quipile / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1412973 WHERE id=603; -- Anapoima / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1316180 WHERE id=1329; -- Toledo / Antioquia
      UPDATE msip_municipio SET osm_id=-1632053 WHERE id=898; -- Patía / Cauca
      UPDATE msip_municipio SET osm_id=-1311677 WHERE id=346; -- Consacá / Nariño
      UPDATE msip_municipio SET osm_id=-1350672 WHERE id=339; -- Concordia / Magdalena
      UPDATE msip_municipio SET osm_id=-385841 WHERE id=889; -- Palestina / Caldas
      UPDATE msip_municipio SET osm_id=-1461001 WHERE id=615; -- Andalucía / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1315086 WHERE id=1319; -- Barrancabermeja / Santander
      UPDATE msip_municipio SET osm_id=-11323754 WHERE id=1411; -- Yacuanquer / Nariño
      UPDATE msip_municipio SET osm_id=-1342923 WHERE id=412; -- Alpujarra / Tolima
      UPDATE msip_municipio SET osm_id=-11909823 WHERE id=808; -- Nemocón / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1306565 WHERE id=788; -- Moniquirá / Boyacá
      UPDATE msip_municipio SET osm_id=-1343723 WHERE id=853; -- Onzaga / Santander
      UPDATE msip_municipio SET osm_id=-1335166 WHERE id=880; -- Palmar de Varela / Atlántico
      UPDATE msip_municipio SET osm_id=-408549 WHERE id=1032; -- Risaralda / Caldas
      UPDATE msip_municipio SET osm_id=-1315459 WHERE id=850; -- Oiba / Santander
      UPDATE msip_municipio SET osm_id=-1350656 WHERE id=538; -- Cotorra / Córdoba
      UPDATE msip_municipio SET osm_id=-1342924 WHERE id=855; -- Ortega / Tolima
      UPDATE msip_municipio SET osm_id=-1412714 WHERE id=978; -- Quebradanegra / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1308639 WHERE id=904; -- Paz de Río / Boyacá
      UPDATE msip_municipio SET osm_id=-1473585 WHERE id=868; -- Padilla / Cauca
      UPDATE msip_municipio SET osm_id=-1631652 WHERE id=1146; -- San Sebastián / Cauca
      UPDATE msip_municipio SET osm_id=-11375598 WHERE id=960; -- Puerto Lleras / Meta
      UPDATE msip_municipio SET osm_id=-1353404 WHERE id=1383; -- Vianí / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1335169 WHERE id=907; -- Puerto Nariño / Amazonas
      UPDATE msip_municipio SET osm_id=-1342919 WHERE id=971; -- Purificación / Tolima
      UPDATE msip_municipio SET osm_id=-1309436 WHERE id=882; -- Palocabildo / Tolima
      UPDATE msip_municipio SET osm_id=-1307011 WHERE id=593; -- Andes / Antioquia
      UPDATE msip_municipio SET osm_id=-1306566 WHERE id=264; -- Chiquinquirá / Boyacá
      UPDATE msip_municipio SET osm_id=-1315408 WHERE id=865; -- Pachavita / Boyacá
      UPDATE msip_municipio SET osm_id=-1315423 WHERE id=948; -- Puente Nacional / Santander
      UPDATE msip_municipio SET osm_id=-1324936 WHERE id=337; -- Colosó / Sucre
      UPDATE msip_municipio SET osm_id=-1353401 WHERE id=1458; -- Bituima / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1473594 WHERE id=1038; -- Roldanillo / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1342925 WHERE id=937; -- Prado / Tolima
      UPDATE msip_municipio SET osm_id=-1423453 WHERE id=1332; -- Tona / Santander
      UPDATE msip_municipio SET osm_id=-1348653 WHERE id=976; -- Puerto Triunfo / Antioquia
      UPDATE msip_municipio SET osm_id=-11895166 WHERE id=381; -- Cubará / Boyacá
      UPDATE msip_municipio SET osm_id=-1351285 WHERE id=1108; -- Santa Lucía / Atlántico
      UPDATE msip_municipio SET osm_id=-1315404 WHERE id=644; -- La Capilla / Boyacá
      UPDATE msip_municipio SET osm_id=-11897374 WHERE id=1312; -- Tibirita / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11374851 WHERE id=954; -- Puerto López / Meta
      UPDATE msip_municipio SET osm_id=-1759531 WHERE id=956; -- Puerto Tejada / Cauca
      UPDATE msip_municipio SET osm_id=-1316172 WHERE id=114; -- Buriticá / Antioquia
      UPDATE msip_municipio SET osm_id=-1356228 WHERE id=1061; -- San Cayetano / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1423448 WHERE id=1144; -- San Sebastián de Buenavista / Magdalena
      UPDATE msip_municipio SET osm_id=-11896741 WHERE id=984; -- Ragonvalia / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1324934 WHERE id=1092; -- Sampués / Sucre
      UPDATE msip_municipio SET osm_id=-1423504 WHERE id=1140; -- San Vicente de Chucurí / Santander
      UPDATE msip_municipio SET osm_id=-11373709 WHERE id=1124; -- San Juan de Arama / Meta
      UPDATE msip_municipio SET osm_id=-1307269 WHERE id=1081; -- San Pedro de Los Milagros / Antioquia
      UPDATE msip_municipio SET osm_id=-1351868 WHERE id=501; -- Gamarra / Cesar
      UPDATE msip_municipio SET osm_id=-1306569 WHERE id=1150; -- Santa Sofía / Boyacá
      UPDATE msip_municipio SET osm_id=-2460349 WHERE id=378; -- Coveñas / Sucre
      UPDATE msip_municipio SET osm_id=-1315446 WHERE id=461; -- Encino / Santander
      UPDATE msip_municipio SET osm_id=-1423462 WHERE id=913; -- Pijiño del Carmen / Magdalena
      UPDATE msip_municipio SET osm_id=-1316181 WHERE id=1068; -- San José de La Montaña / Antioquia
      UPDATE msip_municipio SET osm_id=-1342132 WHERE id=926; -- Pitalito / Huila
      UPDATE msip_municipio SET osm_id=-1423458 WHERE id=1283; -- Tenerife / Magdalena
      UPDATE msip_municipio SET osm_id=-11319551 WHERE id=1042; -- Santa Rosalía / Vichada
      UPDATE msip_municipio SET osm_id=-1351878 WHERE id=1335; -- Tota / Boyacá
      UPDATE msip_municipio SET osm_id=-11332236 WHERE id=491; -- Funes / Nariño
      UPDATE msip_municipio SET osm_id=-11372442 WHERE id=771; -- Puerto Concordia / Meta
      UPDATE msip_municipio SET osm_id=-10298142 WHERE id=483; -- Recetor / Casanare
      UPDATE msip_municipio SET osm_id=-1460149 WHERE id=1308; -- Teruel / Huila
      UPDATE msip_municipio SET osm_id=-1342089 WHERE id=1281; -- Tesalia / Huila
      UPDATE msip_municipio SET osm_id=-1314628 WHERE id=1398; -- Villanueva / La Guajira
      UPDATE msip_municipio SET osm_id=-1412828 WHERE id=1399; -- Villeta / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1354469 WHERE id=1337; -- Topaipí / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11894047 WHERE id=939; -- Providencia / Nariño
      UPDATE msip_municipio SET osm_id=-1324942 WHERE id=1071; -- Sahagún / Córdoba
      UPDATE msip_municipio SET osm_id=-1350540 WHERE id=1456; -- Betulia / Antioquia
      UPDATE msip_municipio SET osm_id=-1309444 WHERE id=1314; -- Timaná / Huila
      UPDATE msip_municipio SET osm_id=-407422 WHERE id=1077; -- Samaná / Caldas
      UPDATE msip_municipio SET osm_id=-1350646 WHERE id=422; -- El Calvario / Meta
      UPDATE msip_municipio SET osm_id=-1342092 WHERE id=1110; -- Santa María / Huila
      UPDATE msip_municipio SET osm_id=-11895624 WHERE id=1330; -- Toledo / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1423515 WHERE id=884; -- Palmar / Santander
      UPDATE msip_municipio SET osm_id=-1345036 WHERE id=624; -- Jenesano / Boyacá
      UPDATE msip_municipio SET osm_id=-1315436 WHERE id=625; -- Jericó / Boyacá
      UPDATE msip_municipio SET osm_id=-1335171 WHERE id=1322; -- Tiquisio / Bolívar
      UPDATE msip_municipio SET osm_id=-1308641 WHERE id=1275; -- Tasco / Boyacá
      UPDATE msip_municipio SET osm_id=-1307267 WHERE id=347; -- Concordia / Antioquia
      UPDATE msip_municipio SET osm_id=-1309988 WHERE id=1136; -- San Lorenzo / Nariño
      UPDATE msip_municipio SET osm_id=-5794061 WHERE id=637; -- La Cumbre / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1315431 WHERE id=419; -- El Cocuy / Boyacá
      UPDATE msip_municipio SET osm_id=-1315084 WHERE id=1425; -- Zapatoca / Santander
      UPDATE msip_municipio SET osm_id=-1342036 WHERE id=696; -- Lejanías / Meta
      UPDATE msip_municipio SET osm_id=-1438582 WHERE id=1130; -- San Juanito / Meta
      UPDATE msip_municipio SET osm_id=-1320726 WHERE id=1265; -- Taminango / Nariño
      UPDATE msip_municipio SET osm_id=-1343279 WHERE id=617; -- Itagüí / Antioquia
      UPDATE msip_municipio SET osm_id=-407426 WHERE id=747; -- Manzanares / Caldas
      UPDATE msip_municipio SET osm_id=-1315453 WHERE id=1247; -- Sucre / Santander
      UPDATE msip_municipio SET osm_id=-1342103 WHERE id=1276; -- Tarqui / Huila
      UPDATE msip_municipio SET osm_id=-11909803 WHERE id=1278; -- Tausa / Cundinamarca
      UPDATE msip_municipio SET osm_id=-4840466 WHERE id=1226; -- Sogamoso / Boyacá
      UPDATE msip_municipio SET osm_id=-1473583 WHERE id=427; -- El Cairo / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1308620 WHERE id=779; -- Murillo / Tolima
      UPDATE msip_municipio SET osm_id=-11896145 WHERE id=928; -- Puerto Santander / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1324940 WHERE id=818; -- Nechí / Antioquia
      UPDATE msip_municipio SET osm_id=-1307003 WHERE id=626; -- Jericó / Antioquia
      UPDATE msip_municipio SET osm_id=-1353247 WHERE id=730; -- Macanal / Boyacá
      UPDATE msip_municipio SET osm_id=-1438584 WHERE id=173; -- La Salina / Casanare
      UPDATE msip_municipio SET osm_id=-1342116 WHERE id=600; -- Hobo / Huila
      UPDATE msip_municipio SET osm_id=-1423485 WHERE id=1090; -- San Andrés / Santander
      UPDATE msip_municipio SET osm_id=-1423480 WHERE id=914; -- Piedecuesta / Santander
      UPDATE msip_municipio SET osm_id=-1324920 WHERE id=930; -- Planeta Rica / Córdoba
      UPDATE msip_municipio SET osm_id=-1343035 WHERE id=1114; -- San Luis / Tolima
      UPDATE msip_municipio SET osm_id=-1717035 WHERE id=1152; -- Santander de Quilichao / Cauca
      UPDATE msip_municipio SET osm_id=-1309442 WHERE id=1242; -- Suaza / Huila
      UPDATE msip_municipio SET osm_id=-1314967 WHERE id=786; -- Molagavita / Santander
      UPDATE msip_municipio SET osm_id=-1350432 WHERE id=1266; -- Tadó / Chocó
      UPDATE msip_municipio SET osm_id=-1351874 WHERE id=1105; -- Salamina / Magdalena
      UPDATE msip_municipio SET osm_id=-1343653 WHERE id=1463; -- Bochalema / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1342090 WHERE id=1041; -- Rovira / Tolima
      UPDATE msip_municipio SET osm_id=-1309440 WHERE id=894; -- Palestina / Huila
      UPDATE msip_municipio SET osm_id=-1342130 WHERE id=1106; -- San Antonio / Tolima
      UPDATE msip_municipio SET osm_id=-1423490 WHERE id=1213; -- San Diego / Cesar
      UPDATE msip_municipio SET osm_id=-1307260 WHERE id=1134; -- Santa Rosa de Osos / Antioquia
      UPDATE msip_municipio SET osm_id=-1345190 WHERE id=1198; -- Siachoque / Boyacá
      UPDATE msip_municipio SET osm_id=-1345076 WHERE id=1037; -- Rondón / Boyacá
      UPDATE msip_municipio SET osm_id=-1315403 WHERE id=1377; -- Ventaquemada / Boyacá
      UPDATE msip_municipio SET osm_id=-10688524 WHERE id=469; -- Facatativá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1306570 WHERE id=1249; -- Sutamarchán / Boyacá
      UPDATE msip_municipio SET osm_id=-1329820 WHERE id=1260; -- Talaigua Nuevo / Bolívar
      UPDATE msip_municipio SET osm_id=-1315428 WHERE id=701; -- La Uvita / Boyacá
      UPDATE msip_municipio SET osm_id=-11329238 WHERE id=1188; -- Sapuyes / Nariño
      UPDATE msip_municipio SET osm_id=-11332349 WHERE id=607; -- Iles / Nariño
      UPDATE msip_municipio SET osm_id=-11323753 WHERE id=1268; -- Tangua / Nariño
      UPDATE msip_municipio SET osm_id=-1473611 WHERE id=970; -- Puracé / Cauca
      UPDATE msip_municipio SET osm_id=-1310021 WHERE id=566; -- Guapi / Cauca
      UPDATE msip_municipio SET osm_id=-8209431 WHERE id=437; -- El Charco / Nariño
      UPDATE msip_municipio SET osm_id=-1722357 WHERE id=174; -- Caldono / Cauca
      UPDATE msip_municipio SET osm_id=-1456104 WHERE id=169; -- Campoalegre / Huila
      UPDATE msip_municipio SET osm_id=-1460959 WHERE id=821; -- Obando / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1438579 WHERE id=986; -- Acacías / Meta
      UPDATE msip_municipio SET osm_id=-1470279 WHERE id=134; -- Caicedonia / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1347532 WHERE id=111; -- Buenavista / Quindío
      UPDATE msip_municipio SET osm_id=-1356246 WHERE id=1141; -- Salento / Quindío
      UPDATE msip_municipio SET osm_id=-1308613 WHERE id=454; -- Alvarado / Tolima
      UPDATE msip_municipio SET osm_id=-1309461 WHERE id=537; -- Ambalema / Tolima
      UPDATE msip_municipio SET osm_id=-1309460 WHERE id=915; -- Piedras / Tolima
      UPDATE msip_municipio SET osm_id=-1309438 WHERE id=220; -- Casabianca / Tolima
      UPDATE msip_municipio SET osm_id=-1350632 WHERE id=1453; -- Betania / Antioquia
      UPDATE msip_municipio SET osm_id=-9175595 WHERE id=456; -- El Rosal / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1328504 WHERE id=1095; -- San Pablo / Bolívar
      UPDATE msip_municipio SET osm_id=-1335188 WHERE id=232; -- Cantagallo / Bolívar
      UPDATE msip_municipio SET osm_id=-1423476 WHERE id=548; -- Girón / Santander
      UPDATE msip_municipio SET osm_id=-1343260 WHERE id=987; -- Acandí / Chocó
      UPDATE msip_municipio SET osm_id=-1354431 WHERE id=1353; -- Ubalá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-10688525 WHERE id=505; -- Gachetá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11909871 WHERE id=221; -- Carmen de Carupa / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1422328 WHERE id=450; -- El Peñón / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1346513 WHERE id=1328; -- Tocancipá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1315420 WHERE id=171; -- Campohermoso / Boyacá
      UPDATE msip_municipio SET osm_id=-11375750 WHERE id=335; -- Miraflores / Guaviare
      UPDATE msip_municipio SET osm_id=-11381355 WHERE id=235; -- Carurú / Vaupés
      UPDATE msip_municipio SET osm_id=-11371088 WHERE id=106; -- Barranca de Upía / Meta
      UPDATE msip_municipio SET osm_id=-11319781 WHERE id=1246; -- Cumaribo / Vichada
      UPDATE msip_municipio SET osm_id=-2346720 WHERE id=35; -- Inírida / Guainía
      UPDATE msip_municipio SET osm_id=-11334013 WHERE id=594; -- Barrancominas / Guainía
      UPDATE msip_municipio SET osm_id=-1324928 WHERE id=256; -- Chigorodó / Antioquia
      UPDATE msip_municipio SET osm_id=-1316173 WHERE id=176; -- Cañasgordas / Antioquia
      UPDATE msip_municipio SET osm_id=-1343268 WHERE id=643; -- Angostura / Antioquia
      UPDATE msip_municipio SET osm_id=-1308642 WHERE id=338; -- Cómbita / Boyacá
      UPDATE msip_municipio SET osm_id=-1324938 WHERE id=1426; -- Zaragoza / Antioquia
      UPDATE msip_municipio SET osm_id=-1324949 WHERE id=436; -- El Bagre / Antioquia
      UPDATE msip_municipio SET osm_id=-1343443 WHERE id=863; -- Arboletes / Antioquia
      UPDATE msip_municipio SET osm_id=-1324926 WHERE id=396; -- Chalán / Sucre
      UPDATE msip_municipio SET osm_id=-11890329 WHERE id=892; -- Aracataca / Magdalena
      UPDATE msip_municipio SET osm_id=-1315427 WHERE id=925; -- Pisba / Boyacá
      UPDATE msip_municipio SET osm_id=-1314681 WHERE id=1257; -- Barrancas / La Guajira
      UPDATE msip_municipio SET osm_id=-1314688 WHERE id=602; -- Albania / La Guajira
      UPDATE msip_municipio SET osm_id=-1423446 WHERE id=392; -- Curití / Santander
      UPDATE msip_municipio SET osm_id=-1343244 WHERE id=862; -- Arboledas / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1423473 WHERE id=562; -- Guaca / Santander
      UPDATE msip_municipio SET osm_id=-1343685 WHERE id=1178; -- Santa Bárbara / Santander
      UPDATE msip_municipio SET osm_id=-11893635 WHERE id=554; -- Gramalote / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1314632 WHERE id=377; -- Cravo Norte / Arauca
      UPDATE msip_municipio SET osm_id=-1309473 WHERE id=981; -- Quinchía / Risaralda
      UPDATE msip_municipio SET osm_id=-1315434 WHERE id=1223; -- Socha / Boyacá
      UPDATE msip_municipio SET osm_id=-1309449 WHERE id=854; -- Oporapa / Huila
      UPDATE msip_municipio SET osm_id=-1343724 WHERE id=1121; -- San Joaquín / Santander
      UPDATE msip_municipio SET osm_id=-11311826 WHERE id=975; -- Puerto Rondón / Arauca
      UPDATE msip_municipio SET osm_id=-1307281 WHERE id=1376; -- Venecia / Antioquia
      UPDATE msip_municipio SET osm_id=-1423506 WHERE id=1189; -- Sardinata / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1413196 WHERE id=1216; -- Soacha / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1473596 WHERE id=1340; -- Trujillo / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1306573 WHERE id=864; -- Arcabuco / Boyacá
      UPDATE msip_municipio SET osm_id=-1421796 WHERE id=1359; -- Une / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1423516 WHERE id=1272; -- Barichara / Santander
      UPDATE msip_municipio SET osm_id=-1307283 WHERE id=145; -- Caldas / Antioquia
      UPDATE msip_municipio SET osm_id=-1421816 WHERE id=979; -- Quetame / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1353484 WHERE id=257; -- Chinavita / Boyacá
      UPDATE msip_municipio SET osm_id=-1453522 WHERE id=715; -- Linares / Nariño
      UPDATE msip_municipio SET osm_id=-1356138 WHERE id=1199; -- Sibaté / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1473587 WHERE id=438; -- El Dovio / Valle del Cauca
      UPDATE msip_municipio SET osm_id=-1307277 WHERE id=465; -- Envigado / Antioquia
      UPDATE msip_municipio SET osm_id=-1356457 WHERE id=498; -- Gachalá / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1348583 WHERE id=1370; -- Valparaíso / Antioquia
      UPDATE msip_municipio SET osm_id=-11333637 WHERE id=561; -- Guachucal / Nariño
      UPDATE msip_municipio SET osm_id=-1315391 WHERE id=504; -- Gámeza / Boyacá
      UPDATE msip_municipio SET osm_id=-1309477 WHERE id=698; -- La Tebaida / Quindío
      UPDATE msip_municipio SET osm_id=-1423497 WHERE id=638; -- Labateca / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1355853 WHERE id=689; -- Anolaima / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1315424 WHERE id=1102; -- San Mateo / Boyacá
      UPDATE msip_municipio SET osm_id=-408355 WHERE id=1252; -- Supía / Caldas
      UPDATE msip_municipio SET osm_id=-1350634 WHERE id=1277; -- Tarso / Antioquia
      UPDATE msip_municipio SET osm_id=-1342107 WHERE id=918; -- Pital / Huila
      UPDATE msip_municipio SET osm_id=-1303947 WHERE id=873; -- Páez / Cauca

      --- Manualmente que si están en osm-boundaries y OSM pero tal vez diferente nombre
      UPDATE msip_municipio SET osm_id=-4047171 WHERE id=199; -- Carmen de Apicalá / Tolima
      UPDATE msip_municipio SET osm_id=-10688130 WHERE id=700; -- La Vega / Cundinamarca
      UPDATE msip_municipio SET osm_id=-11893926 WHERE id=650; -- La Esperanza / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1387968 WHERE id=24; -- Bogotá, D.C. / Bogotá, D.C.
      UPDATE msip_municipio SET osm_id=-4047422 WHERE id=1259; -- Suárez / Cauca
      UPDATE msip_municipio SET osm_id=-11889093 WHERE id=1236; -- Subachoque / Cundinamarca
      UPDATE msip_municipio SET osm_id=-4079004 WHERE id=33; -- Florencia / Caqueta

      --- Manualmente que están en OSM en admin_level=7 con entidad_te=Área no municipializada o Isla

      UPDATE msip_municipio SET osm_id=-1335191 WHERE id=459; -- El Encanto / Amazonas
      UPDATE msip_municipio SET osm_id=-1335172 WHERE id=703; -- La Chorrera / Amazonas
      UPDATE msip_municipio SET osm_id=-3718640 WHERE id=707; -- La Pedrera / Amazonas
      UPDATE msip_municipio SET osm_id=-1335182 WHERE id=738; -- La Victoria / Amazonas
      UPDATE msip_municipio SET osm_id=-1335187 WHERE id=777; -- Mirití - Paraná / Amazonas
      UPDATE msip_municipio SET osm_id=-1335170 WHERE id=896; -- Puerto Alegría / Amazonas
      UPDATE msip_municipio SET osm_id=-1335190 WHERE id=903; -- Puerto Arica / Amazonas
      UPDATE msip_municipio SET osm_id=-1335167 WHERE id=1089; -- Puerto Santander / Amazonas
      UPDATE msip_municipio SET osm_id=-1335184 WHERE id=1282; -- Tarapacá / Amazonas

      UPDATE msip_municipio SET osm_id=-2287617 WHERE id=50; -- San Andrés / Archipiélago de San Andrés, Providencia y Santa Catalina

      UPDATE msip_municipio SET osm_id=-11378726 WHERE id=1414; -- Cacahual / Guainía
      UPDATE msip_municipio SET osm_id=-11378924 WHERE id=1408; -- La Guadalupe / Guainía
      UPDATE msip_municipio SET osm_id=-11380591 WHERE id=1417; -- Morichal / Guainía
      UPDATE msip_municipio SET osm_id=-11380616 WHERE id=1415; -- Pana Pana / Guainía
      UPDATE msip_municipio SET osm_id=-11380495 WHERE id=1407; -- Puerto Colombia / Guainía
      UPDATE msip_municipio SET osm_id=-11379165 WHERE id=1406; -- San Felipe / Guainía
      UPDATE msip_municipio SET osm_id=-11382169 WHERE id=866; -- Pacoa / Vaupés
      UPDATE msip_municipio SET osm_id=-11381990 WHERE id=1250; -- Papunahua / Vaupés
      UPDATE msip_municipio SET osm_id=-11381490 WHERE id=1418; -- Yavaraté / Vaupés

      --- Manualmente municipios que no salen en osm-boundaries pero si están en OSM aunque con nombres en ocasiones diferentes al DIVIPOLA

      UPDATE msip_municipio SET osm_id=-1853402 WHERE id=938; -- Providencia / Archipiélago de San Andrés, Providencia y Santa Catalina
      UPDATE msip_municipio SET osm_id=-1316182 WHERE id=690; -- Anorí / Antioquia
      UPDATE msip_municipio SET osm_id=-1307236 WHERE id=922; -- Argelia / Antioquia
      UPDATE msip_municipio SET osm_id=-1316175 WHERE id=170; -- Campamento / Antioquia
      UPDATE msip_municipio SET osm_id=-1307278 WHERE id=214; -- Carolina / Antioquia
      UPDATE msip_municipio SET osm_id=-1307264 WHERE id=408; -- Donmatías / Antioquia
      UPDATE msip_municipio SET osm_id=-1307284  WHERE id=646; -- La Estrella / Antioquia
      UPDATE msip_municipio SET osm_id=-3947504 WHERE id=1104; -- San Vicente Ferrer / Antioquia
      UPDATE msip_municipio SET osm_id=-1343259 WHERE id=910; -- Peñol / Antioquia
      UPDATE msip_municipio SET osm_id=-1350575  WHERE id=1020; -- Retiro / Antioquia
      UPDATE msip_municipio SET osm_id=-1307273 WHERE id=1143; -- Santo Domingo / Antioquia
      UPDATE msip_municipio SET osm_id=-1307256 WHERE id=1413; -- Yalí / Antioquia
      UPDATE msip_municipio SET osm_id=-1307275 WHERE id=1420; -- Yolombó / Antioquia
      UPDATE msip_municipio SET osm_id=-1341506 WHERE id=1033; -- Arroyohondo / Bolívar
      UPDATE msip_municipio SET osm_id=-1335168 WHERE id=1126; -- Santa Rosa / Bolívar
      UPDATE msip_municipio SET osm_id=-1345101 WHERE id=384; -- Cucaita / Boyacá
      UPDATE msip_municipio SET osm_id=-1315433 WHERE id=590; -- Güicán de La Sierra / Boyacá
      UPDATE msip_municipio SET osm_id=-1315413 WHERE id=1356; -- Úmbita / Boyacá
      UPDATE msip_municipio SET osm_id=-11313200 WHERE id=743; -- Trinidad / Casanare
      UPDATE msip_municipio SET osm_id=-1342121 WHERE id=494; -- Albania / Caquetá
      UPDATE msip_municipio SET osm_id=-1342131 WHERE id=1457; -- Belén de Los Andaquíes / Caquetá
      UPDATE msip_municipio SET osm_id=-1342117 WHERE id=215; -- Cartagena del Chairá / Caquetá
      UPDATE msip_municipio SET osm_id=-1342095 WHERE id=1214; -- San Vicente del Caguán / Caquetá
      UPDATE msip_municipio SET osm_id=-1342101 WHERE id=1220; -- Solano / Caquetá
      UPDATE msip_municipio SET osm_id=-1342124 WHERE id=1262; -- Solita / Caquetá
      UPDATE msip_municipio SET osm_id=-1342102 WHERE id=1374; -- Valparaíso / Caquetá
      UPDATE msip_municipio SET osm_id=-11313104 WHERE id=397; -- Orocué / Casanare
      UPDATE msip_municipio SET osm_id=-11319054 WHERE id=756; -- Villanueva / Casanare
      UPDATE msip_municipio SET osm_id=-1311685 WHERE id=723; -- López de Micay / Cauca
      UPDATE msip_municipio SET osm_id=-1725505 WHERE id=916; -- Piendamó - Tunía / Cauca
      UPDATE msip_municipio SET osm_id=-1473606 WHERE id=1230; -- Sotará Paispamba / Cauca
      UPDATE msip_municipio SET osm_id=-1343040 WHERE id=432; -- Alto Baudó / Chocó
      UPDATE msip_municipio SET osm_id=-1348348 WHERE id=848; -- Atrato / Chocó
      UPDATE msip_municipio SET osm_id=-1343136 WHERE id=1210; -- Bahía Solano / Chocó
      UPDATE msip_municipio SET osm_id=-1343028 WHERE id=1237; -- Bajo Baudó / Chocó
      UPDATE msip_municipio SET osm_id=-1343266 WHERE id=1465; -- Bojayá / Chocó
      UPDATE msip_municipio SET osm_id=-1343139 WHERE id=213; -- Carmen del Darién / Chocó
      UPDATE msip_municipio SET osm_id=-1307024 WHERE id=172; -- El Cantón del San Pablo / Chocó
      UPDATE msip_municipio SET osm_id=-1348640 WHERE id=733; -- Medio Atrato / Chocó
      UPDATE msip_municipio SET osm_id=-1307006 WHERE id=1012; -- Río Quito / Chocó
      UPDATE msip_municipio SET osm_id=-1309475 WHERE id=1207; -- Msipí / Chocó
      UPDATE msip_municipio SET osm_id=-1350459 WHERE id=1323; -- Unión Panamericana / Chocó
      UPDATE msip_municipio SET osm_id=-1324951 WHERE id=972; -- Purísima de La Concepción / Córdoba
      UPDATE msip_municipio SET osm_id=-10687625 WHERE id=262; -- Chía / Cundinamarca
      UPDATE msip_municipio SET osm_id=-10687625 WHERE id=1357; -- Villa de San Diego de Ubaté / Cundinamarca
      UPDATE msip_municipio SET osm_id=-1314686 WHERE id=640; -- Hatonuevo / La Guajira
      UPDATE msip_municipio SET osm_id=-1423456 WHERE id=962; -- Ariguaní / Magdalena
      UPDATE msip_municipio SET osm_id=-1423492 WHERE id=254; -- Chivolo / Magdalena
      UPDATE msip_municipio SET osm_id=-1423472 WHERE id=778; -- Nueva Granada / Magdalena
      UPDATE msip_municipio SET osm_id=-1423494 WHERE id=1459; -- Zapayán / Magdalena
      UPDATE msip_municipio SET osm_id=-1351875 WHERE id=1462; -- Zona Bananera / Magdalena
      UPDATE msip_municipio SET osm_id=-11371824 WHERE id=941; -- Puerto Gaitán / Meta
      UPDATE msip_municipio SET osm_id=-11372871 WHERE id=1182; -- Vistahermosa / Meta
      UPDATE msip_municipio SET osm_id=-1309983 WHERE id=287; -- Albán / Nariño
      UPDATE msip_municipio SET osm_id=-1309987 WHERE id=861; -- Arboleda / Nariño
      UPDATE msip_municipio SET osm_id=-1309989 WHERE id=336; -- Colón / Nariño
      UPDATE msip_municipio SET osm_id=-1311687 WHERE id=383; -- Cuaspud Carlosama / Nariño
      UPDATE msip_municipio SET osm_id=-8209432 WHERE id=879; -- Francisco Pizarro / Nariño
      UPDATE msip_municipio SET osm_id=-11326664 WHERE id=719; -- Los Andes / Nariño
      UPDATE msip_municipio SET osm_id=-1355567 WHERE id=735; -- Magüí / Nariño
      UPDATE msip_municipio SET osm_id=-11322770 WHERE id=748; -- Mallama / Nariño
      UPDATE msip_municipio SET osm_id=-1311681 WHERE id=813; -- Olaya Herrera / Nariño
      UPDATE msip_municipio SET osm_id=-1311746 WHERE id=1036; -- Roberto Payán / Nariño
      UPDATE msip_municipio SET osm_id=-1311750 WHERE id=1345; -- San Andrés de Tumaco / Nariño
      UPDATE msip_municipio SET osm_id=-1311765 WHERE id=1149; -- Santa Bárbara / Nariño
      UPDATE msip_municipio SET osm_id=-11891732 WHERE id=663; -- La Playa / Norte de Santander
      UPDATE msip_municipio SET osm_id=-11897110 WHERE id=32; -- San José de Cúcuta / Norte de Santander
      UPDATE msip_municipio SET osm_id=-1342125 WHERE id=953; -- Puerto Leguízamo / Putumayo
      UPDATE msip_municipio SET osm_id=-1311675 WHERE id=1222; -- San Miguel / Putumayo
      UPDATE msip_municipio SET osm_id=-1311673 WHERE id=1381; -- Valle del Guamuez / Putumayo
      UPDATE msip_municipio SET osm_id=-1343032 WHERE id=297; -- Circasia / Quindío
      UPDATE msip_municipio SET osm_id=-1343030 WHERE id=1137; -- Santuario / Risaralda
      UPDATE msip_municipio SET osm_id=-1423500 WHERE id=1218; -- Socorro / Santander
      UPDATE msip_municipio SET osm_id=-1343440 WHERE id=720; -- Los Palmitos / Sucre
      UPDATE msip_municipio SET osm_id=-1343454 WHERE id=886; -- Palmito / Sucre
      UPDATE msip_municipio SET osm_id=-1324939 WHERE id=1336; -- San José de Toluviejo / Sucre
      UPDATE msip_municipio SET osm_id=-1324950 WHERE id=1200; -- San Luis de Sincé / Sucre
      UPDATE msip_municipio SET osm_id=-1343445 WHERE id=1331; -- Santiago de Tolú / Sucre
      UPDATE msip_municipio SET osm_id=-1342109 WHERE id=923; -- Armero / Tolima
      UPDATE msip_municipio SET osm_id=-1309452 WHERE id=761; -- San Sebastián de Mariquita / Tolima
      UPDATE msip_municipio SET osm_id=-4047422 WHERE id=1243; -- Suárez / Tolima
      UPDATE msip_municipio SET osm_id=-1461502 WHERE id=112; -- Guadalajara de Buga / Valle del Cauca

    SQL
  end

  #- Problemas en OSM
  # 1. Está como en admin_level=6 el resguardo Tumbarao, en
  # osm-boundaries parecería una relación pero con osm_id positivo.
  # En OSM está como vía pero admin_level=6:
  # https://www.openstreetmap.org/way/62089775#map=15/2.7368/-76.4793

  # 2. Nombres de municipios diferentes (por corregir o agregar oficial)
  # Pijaos, Boyacá 1345101 (aunque al mapero no le guste el nombre) debería ser Cucaita como indican la Alcaldía http://www.cucaita-boyaca.gov.co/, DIVOLA y Wikipedia https://es.wikipedia.org/wiki/es:Cucaita?uselang=es
  # San Rafael de Úmbita, Boyaca 1315413 debería ser Úmbita como indica Alcaldia http://www.umbita-boyaca.gov.co/, DIVIPOLA y Wikipedia
  # Paimadó, Choco 1307006 debería ser Río Quito como indican alcaldía http://www.rioquito-choco.gov.co/ (con capítal Paimadó) y DIVIPOLA
  # Las Ánimas, Choco 1350459 debería ser Unión Panamericana como indica alcaldia http://www.unionpanamericana-choco.gov.co/ (con capital Las Ánimas) y DIVIPOLA
  # San Vicente, Antioquía 3947504 debería tener nombre oficial San Vicente Ferrer como indica alcaldia http://www.sanvicente-antioquia.gov.co/  (al ver la página)
  # Buga, Valle del Cauca 1461502 debería tener nombre oficial Guadalajara de Buga  como indica alcaldia http://www.guadalajaradebuga-valle.gov.co/
  # Mariquita, Tolima debería tener nombre oficial San Sebastián de Mariquita como indican alcaldia https://www.sansebastiandemariquita-tolima.gov.co/

  # Puede haber más nombres oficiales faltantes (Esperar a consultas alcaldías y DANE)

  # 3. Faltan que sea municipio o Isla (tras consulta al DANE)
  # Providencia , lo más cercano es Isla Providencia 1853402  que
  # es relación, pero no tiene admin_level (6 si fuera municipio 7 si fuera Isla con entidad_te=Isla)

  # Preguntas alcaldias
  # Santa Bárbara - Iscuande 1311765 según DIVIPOLA el nombre oficial es Santa Bárbara pero la página de la alcaldia http://www.santabarbara-narino.gov.co/ dice tanto "Santa Bárbara" como "Santa Bárbara Iscuande".  Pregunté en https://www.facebook.com/permalink.php?story_fbid=pfbid02rsNnHEJzVzwx4Ki1d89PSVQCM1yLLBe59QYN2ekQhyTDpxC6iQW3AQ9yCEo5trmLl&id=100064763500226

  # Seguramente errores DANE

  # Carolina / Antioquia debería ser Carolina del Principe como indica la alcaldía http://www.carolinadelprincipe-antioquia.gov.co/
  # Peñol / Antioquia debería ser El Peñol como indica la alcaldía https://elpenol-antioquia.gov.co/
  # Retiro / Antioquia debería ser El Retiro como indica la alcaldia http://www.elretiro-antioquia.gov.co/
  # Santa Rosa / Bolívar debería ser Santa Rosa del Sur como indica la alcaldia http://www.santarosadelsur-bolivar.gov.co/
  # Msipí / Chocó debería ser Sipí como indica la alcaldia http://www.sipi-choco.gov.co/
  # Vistahermosa / Meta debería ser Vista Hermosa como indica la alcaldía http://www.vistahermosa-meta.gov.co/  (ver página)
  # Chivolo / Magdalena debería ser Chibolo como indica la alcaldía http://www.chibolo-magdalena.gov.co/
  # Magüi / Nariño debería ser Magüi Payan como indica la alcaldía  http://www.maguipayan-narino.gov.co/
  # La Playa / Norte de Santander debería ser La Playa de Belén como indica la alcaldía http://www.laplayadebelen-nortedesantander.gov.co/e
  # Palmito / Sucre debería ser San Antonio de Palmito como indica la alcaldía http://www.sanantoniodepalmito-sucre.gov.co/

  # Preguntas DANE

  # ¿Villa de San Diego de Ubaté / Cundinamarca  no debería ser Ubaté como indica la alcaldía http://www.ubate-cundinamarca.gov.co/ ?
  # ¿Santiago de Tolú, Sucre, no debería ser Toluviejo como indica la alcaldía http://www.toluviejo-sucre.gov.co/ ?
  # ¿San Luis de Since / Sucre no debería ser Since como indica la alcaldía http://www.since-sucre.gov.co/
  # ¿San Andrés de Tumaco / Nariño no debería ser Tumaco como indica la alcaldía http://www.tumaco-narino.gov.co/ ?
  # ¿Purisima de la Concepción / Córdoba no debería ser Purisima como indica la alcaldía http://www.purisima-cordoba.gov.co/
  # ¿Socorro / Santander no debería ser El Socorro, como indican Wikipedia y OSM https://es.wikipedia.org/wiki/es:El%20Socorro%20(Santander)?uselang=es ?

  def down
    execute <<-SQL
      UPDATE msip_municipio SET osm_id=NULL;
    SQL
  end
end
