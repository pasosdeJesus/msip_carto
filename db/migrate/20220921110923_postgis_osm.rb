class PostgisOsm < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL
      CREATE EXTENSION IF NOT EXISTS postgis;#{' '}

      ALTER TABLE msip_pais ADD osm_id INTEGER;
      ALTER TABLE msip_pais ADD osm_frontera geography(MULTIPOLYGON, 4326);#{' '}
      ALTER TABLE msip_pais ADD osm_fecha DATE;

      ALTER TABLE msip_departamento ADD osm_id INTEGER;
      ALTER TABLE msip_departamento ADD osm_frontera geography(MULTIPOLYGON, 4326);
      ALTER TABLE msip_departamento ADD osm_fecha DATE;

      ALTER TABLE msip_municipio ADD osm_id INTEGER;
      ALTER TABLE msip_municipio ADD osm_frontera geography(MULTIPOLYGON, 4326);
      ALTER TABLE msip_municipio ADD osm_fecha DATE;

      ALTER TABLE msip_clase ADD osm_id INTEGER;
      ALTER TABLE msip_clase ADD osm_frontera geography(MULTIPOLYGON, 4326);
      ALTER TABLE msip_clase ADD osm_fecha DATE;
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE msip_clase DROP osm_frontera;
      ALTER TABLE msip_clase DROP osm_id;
      ALTER TABLE msip_clase DROP osm_fecha;
      ALTER TABLE msip_municipio DROP osm_frontera;
      ALTER TABLE msip_municipio DROP osm_id;
      ALTER TABLE msip_municipio DROP osm_fecha;
      ALTER TABLE msip_departamento DROP osm_frontera;
      ALTER TABLE msip_departamento DROP osm_id;
      ALTER TABLE msip_departamento DROP osm_fecha;
      ALTER TABLE msip_pais DROP osm_frontera;
      ALTER TABLE msip_pais DROP osm_id;
      ALTER TABLE msip_pais DROP osm_fecha;
      --DROP EXTENSION postgis;#{' '}
    SQL
  end
end
