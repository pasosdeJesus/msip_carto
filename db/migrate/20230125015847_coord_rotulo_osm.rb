class CoordRotuloOsm < ActiveRecord::Migration[7.0]
  def change
    add_column :msip_pais, :osm_rotulolat, :float
    add_column :msip_pais, :osm_rotulolon, :float
    add_column :msip_departamento, :osm_rotulolat, :float
    add_column :msip_departamento, :osm_rotulolon, :float
    add_column :msip_municipio, :osm_rotulolat, :float
    add_column :msip_municipio, :osm_rotulolon, :float
    add_column :msip_clase, :osm_rotulolat, :float
    add_column :msip_clase, :osm_rotulolon, :float
  end
end
