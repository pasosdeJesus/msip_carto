class CamposMgn2021 < ActiveRecord::Migration[7.0]
  def change
    add_column :msip_pais, :mgn_frontera, :geography
    add_column :msip_pais, :mgn_fecha, :date

    add_column :msip_departamento, :mgn_frontera, :geography
    add_column :msip_departamento, :mgn_fecha, :date

    add_column :msip_municipio, :mgn_frontera, :geography
    add_column :msip_municipio, :mgn_fecha, :date

    add_column :msip_clase, :mgn_frontera, :geography
    add_column :msip_clase, :mgn_fecha, :date
  end
end
