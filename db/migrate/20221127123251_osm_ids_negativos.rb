class OsmIdsNegativos < ActiveRecord::Migration[7.0]
  # Hemos notado que para Colombia la inmensa mayoría de osm_ids son negativos
  # Sin embargo hay unos cuantos positivos, hasta nivel 6 por ejemplo
  # Tumburao, 62089775
  # Resguardo Indígena Novirao 62090224
  # La Esperanza 303560302
  # Resguardo Indígena Ambaló 62089725
  # Pero todos los de nivel 4 (i.e departamentos) son negativos
  def up
    execute <<-SQL
      UPDATE msip_pais SET osm_id=-osm_id WHERE id=170 AND osm_id>0;
      UPDATE msip_departamento SET osm_id=-osm_id WHERE osm_id>0;
    SQL
  end

  def down
    execute <<-SQL
      UPDATE msip_pais SET osm_id=-osm_id WHERE id=170 AND osm_id<0;
      UPDATE msip_departamento SET osm_id=-osm_id WHERE osm_id<0;
    SQL
  end
end
