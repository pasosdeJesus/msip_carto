class AgregaCoordRotuloCol < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL
      UPDATE msip_pais SET osm_rotulolat='4.099917',
        osm_rotulolon='-72.9088133' WHERE id=170; -- Colombia

      UPDATE msip_departamento SET osm_rotulolat='-1.305299', osm_rotulolon='-71.4659212' WHERE id=55; -- Amazonas
      UPDATE msip_departamento SET osm_rotulolat='7.0000085', osm_rotulolon='-75.5000086' WHERE id=35; -- Antioquia
      UPDATE msip_departamento SET osm_rotulolat='6.6666755', osm_rotulolon='-71.0000086' WHERE id=50; -- Arauca
      UPDATE msip_departamento SET osm_rotulolat='12.9800387', osm_rotulolon='-81.5320418' WHERE id=53; -- Archipiélago de San Andrés, Providencia y Santa Catalina
      UPDATE msip_departamento SET osm_rotulolat='10.6773422', osm_rotulolon='-74.9718666' WHERE id=48; -- Atlántico
      UPDATE msip_departamento SET osm_rotulolat='4.6534649', osm_rotulolon='-74.0836453' WHERE id=4; -- Bogotá, D.C.
      UPDATE msip_departamento SET osm_rotulolat='9.3660477', osm_rotulolon='-74.8023636' WHERE id=7; -- Bolívar
      UPDATE msip_departamento SET osm_rotulolat='5.6278979', osm_rotulolon='-72.8268617' WHERE id=11; -- Boyacá
      UPDATE msip_departamento SET osm_rotulolat='5.310695669394131', osm_rotulolon='-75.31317856462695' WHERE id=13; -- Caldas
      UPDATE msip_departamento SET osm_rotulolat='1.1153385', osm_rotulolon='-74.1056838' WHERE id=15; -- Caquetá
      UPDATE msip_departamento SET osm_rotulolat='5.5000085', osm_rotulolon='-71.5000086' WHERE id=51; -- Casanare
      UPDATE msip_departamento SET osm_rotulolat='2.187815547077872', osm_rotulolon='-76.70910098658763' WHERE id=17; -- Cauca
      UPDATE msip_departamento SET osm_rotulolat='9.3333415', osm_rotulolon='-73.5000086' WHERE id=20; -- Cesar
      UPDATE msip_departamento SET osm_rotulolat='5.991870367103487', osm_rotulolon='-76.92561528816991' WHERE id=29; -- Chocó
      UPDATE msip_departamento SET osm_rotulolat='8.3344713', osm_rotulolon='-75.6666238' WHERE id=24; -- Córdoba
      UPDATE msip_departamento SET osm_rotulolat='5.1096596', osm_rotulolon='-74.0995854' WHERE id=27; -- Cundinamarca
      UPDATE msip_departamento SET osm_rotulolat='2.5000086', osm_rotulolon='-69.0000086' WHERE id=56; -- Guainía
      UPDATE msip_departamento SET osm_rotulolat='1.823634761012956', osm_rotulolon='-71.83089575429898' WHERE id=57; -- Guaviare
      UPDATE msip_departamento SET osm_rotulolat='2.4738993', osm_rotulolon='-75.5900113' WHERE id=32; -- Huila
      UPDATE msip_departamento SET osm_rotulolat='11.4354118', osm_rotulolon='-72.9002161' WHERE id=33; -- La Guajira
      UPDATE msip_departamento SET osm_rotulolat='10.5808075', osm_rotulolon='-74.0685669' WHERE id=34; -- Magdalena
      UPDATE msip_departamento SET osm_rotulolat='3.5000086', osm_rotulolon='-73.0000086' WHERE id=37; -- Meta
      UPDATE msip_departamento SET osm_rotulolat='1.5842268', osm_rotulolon='-77.8585766' WHERE id=38; -- Nariño
      UPDATE msip_departamento SET osm_rotulolat='8.4417924', osm_rotulolon='-73.0492505' WHERE id=39; -- Norte de Santander
      UPDATE msip_departamento SET osm_rotulolat='0.5000086', osm_rotulolon='-76.0000086' WHERE id=52; -- Putumayo
      UPDATE msip_departamento SET osm_rotulolat='4.4028313', osm_rotulolon='-75.7025795' WHERE id=41; -- Quindío
      UPDATE msip_departamento SET osm_rotulolat='5.2102948', osm_rotulolon='-75.9842236' WHERE id=42; -- Risaralda
      UPDATE msip_departamento SET osm_rotulolat='7.0000085', osm_rotulolon='-73.2500086' WHERE id=43; -- Santander
      UPDATE msip_departamento SET osm_rotulolat='9.151270095021871', osm_rotulolon='-75.09174599673234' WHERE id=45; -- Sucre
      UPDATE msip_departamento SET osm_rotulolat='4.0355786', osm_rotulolon='-75.2086642' WHERE id=46; -- Tolima
      UPDATE msip_departamento SET osm_rotulolat='3.6984053', osm_rotulolon='-76.5501996' WHERE id=47; -- Valle del Cauca
      UPDATE msip_departamento SET osm_rotulolat='0.476154280245977', osm_rotulolon='-70.31038231634471' WHERE id=58; -- Vaupés
      UPDATE msip_departamento SET osm_rotulolat='4.8686613', osm_rotulolon='-69.3736658' WHERE id=59; -- Vichada


      UPDATE msip_municipio SET osm_rotulolat='-1.966195797423301', osm_rotulolon='-72.76766524026765' WHERE id=459; -- El Encanto / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='-1.189111195220321', osm_rotulolon='-72.8484481257724' WHERE id=703; -- La Chorrera / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='-1.305392177094046', osm_rotulolon='-70.06726059342861' WHERE id=707; -- La Pedrera / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='-0.116868950061044', osm_rotulolon='-71.15944270644434' WHERE id=738; -- La Victoria / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='-3.497314012309651', osm_rotulolon='-70.0760257475551' WHERE id=36; -- Leticia / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='-0.573375686107474', osm_rotulolon='-71.18536457497336' WHERE id=777; -- Mirití - Paraná / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='-0.967609035576533', osm_rotulolon='-73.83533948614271' WHERE id=896; -- Puerto Alegría / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='-1.891977370365801', osm_rotulolon='-71.19816064693384' WHERE id=903; -- Puerto Arica / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='-3.659310989013014', osm_rotulolon='-70.47953964877085' WHERE id=907; -- Puerto Nariño / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='-1.103790612709663', osm_rotulolon='-71.88374033744233' WHERE id=1089; -- Puerto Santander / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='-2.537691349310862', osm_rotulolon='-70.13749154757778' WHERE id=1282; -- Tarapacá / Amazonas
      UPDATE msip_municipio SET osm_rotulolat='5.807748484516638', osm_rotulolon='-75.44671838706253' WHERE id=300; -- Abejorral / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.642912791704469', osm_rotulolon='-76.08711933717052' WHERE id=665; -- Abriaquí / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.356810206446736', osm_rotulolon='-75.10060317747092' WHERE id=349; -- Alejandría / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.028316737519975', osm_rotulolon='-75.71994654625539' WHERE id=536; -- Amagá / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.965960876417522', osm_rotulolon='-74.9646403944756' WHERE id=550; -- Amalfi / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.642772913955792', osm_rotulolon='-75.94477751451738' WHERE id=593; -- Andes / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.127073750290954', osm_rotulolon='-75.71962211617559' WHERE id=616; -- Angelópolis / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.856302860193757', osm_rotulolon='-75.37021296547783' WHERE id=643; -- Angostura / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.19313486669991', osm_rotulolon='-75.09174286250091' WHERE id=690; -- Anorí / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.314049414527573', osm_rotulolon='-75.91833806751083' WHERE id=752; -- Anzá / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.889901730673775', osm_rotulolon='-76.56494268585924' WHERE id=766; -- Apartadó / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='8.629140344062936', osm_rotulolon='-76.39874930309662' WHERE id=863; -- Arboletes / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.717370160276283', osm_rotulolon='-75.09982218004278' WHERE id=922; -- Argelia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.172039123282069', osm_rotulolon='-75.81737492118097' WHERE id=973; -- Armenia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.447555779929425', osm_rotulolon='-75.34522302468856' WHERE id=1271; -- Barbosa / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.359630246189797', osm_rotulolon='-75.59310416407236' WHERE id=1404; -- Bello / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.669475856129512', osm_rotulolon='-75.66016860475852' WHERE id=1372; -- Belmira / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.72397054329949', osm_rotulolon='-75.99093140450348' WHERE id=1453; -- Betania / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.199535266437753', osm_rotulolon='-75.96064714873526' WHERE id=1456; -- Betulia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.112462365834333', osm_rotulolon='-75.55182302306254' WHERE id=83; -- Briceño / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.808803429994977', osm_rotulolon='-75.92021306888556' WHERE id=114; -- Buriticá / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.645517724076684', osm_rotulolon='-75.25409495368203' WHERE id=132; -- Cáceres / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.425132737540693', osm_rotulolon='-76.00228950303223' WHERE id=139; -- Caicedo / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.053899579443596', osm_rotulolon='-75.63370843723929' WHERE id=145; -- Caldas / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.050794816486872', osm_rotulolon='-75.28759231723915' WHERE id=170; -- Campamento / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.814446602989445', osm_rotulolon='-76.03552015467251' WHERE id=176; -- Cañasgordas / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.353578477271391', osm_rotulolon='-74.74961446762842' WHERE id=193; -- Caracolí / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.556750534542335', osm_rotulolon='-75.64014704561704' WHERE id=194; -- Caramanta / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.803176542671052', osm_rotulolon='-76.63203596909553' WHERE id=196; -- Carepa / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.756279740228361', osm_rotulolon='-75.31094503583361' WHERE id=214; -- Carolina / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.839351133051547', osm_rotulolon='-75.06928302041' WHERE id=222; -- Caucasia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.628483032656632', osm_rotulolon='-76.65100453787407' WHERE id=256; -- Chigorodó / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.549359440461355', osm_rotulolon='-75.09052919843816' WHERE id=298; -- Cisneros / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.841084145405457', osm_rotulolon='-76.01306742118969' WHERE id=79; -- Ciudad Bolívar / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.001990332907555', osm_rotulolon='-75.17453718611154' WHERE id=299; -- Cocorná / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.384990338104657', osm_rotulolon='-75.22784870238229' WHERE id=343; -- Concepción / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.054424976686952', osm_rotulolon='-75.91625502871408' WHERE id=347; -- Concordia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.354008723542001', osm_rotulolon='-75.50627863254327' WHERE id=358; -- Copacabana / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.950657330581321', osm_rotulolon='-76.30639306369646' WHERE id=403; -- Dabeiba / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.498540491170109', osm_rotulolon='-75.372420043056' WHERE id=408; -- Donmatías / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.323243795893188', osm_rotulolon='-75.79326453322973' WHERE id=416; -- Ebéjico / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.711098742783832', osm_rotulolon='-74.65768650816442' WHERE id=436; -- El Bagre / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.99564211246088', osm_rotulolon='-75.28608201755291' WHERE id=200; -- El Carmen de Viboral / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.127883114976426', osm_rotulolon='-75.25618241061258' WHERE id=1151; -- El Santuario / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.601479252520679', osm_rotulolon='-75.5717636841326' WHERE id=462; -- Entrerríos / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.158507414360884', osm_rotulolon='-75.5488112630814' WHERE id=465; -- Envigado / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.894340368916144', osm_rotulolon='-75.6915876765876' WHERE id=486; -- Fredonia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.679746184469907', osm_rotulolon='-76.31298965709377' WHERE id=488; -- Frontino / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.657044048298461', osm_rotulolon='-75.94883263305768' WHERE id=546; -- Giraldo / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.38196521578112', osm_rotulolon='-75.45231175507632' WHERE id=549; -- Girardota / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.703282019057724', osm_rotulolon='-75.20764010144498' WHERE id=552; -- Gómez Plata / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.128903568573045', osm_rotulolon='-75.14693680498901' WHERE id=555; -- Granada / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.86696572714515', osm_rotulolon='-75.23669250432802' WHERE id=557; -- Guadalupe / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.275130217448325', osm_rotulolon='-75.44382101862558' WHERE id=567; -- Guarne / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.255080190980006', osm_rotulolon='-75.15953246987827' WHERE id=576; -- Guatapé / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.21412953489398', osm_rotulolon='-75.75814418785988' WHERE id=597; -- Heliconia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.797986224510376', osm_rotulolon='-75.91315854846566' WHERE id=608; -- Hispania / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.178640463907955', osm_rotulolon='-75.61774612928401' WHERE id=617; -- Itagüí / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.262818028598431', osm_rotulolon='-75.84812068976467' WHERE id=619; -- Ituango / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.558539534935218', osm_rotulolon='-75.83648896063467' WHERE id=623; -- Jardín / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.772178092487513', osm_rotulolon='-75.7725140717993' WHERE id=626; -- Jericó / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.985826731054295', osm_rotulolon='-75.44441547806215' WHERE id=634; -- La Ceja / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.13829144570513', osm_rotulolon='-75.64957147391883' WHERE id=646; -- La Estrella / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.746928229845123', osm_rotulolon='-75.6116194051818' WHERE id=655; -- La Pintada / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.937141965956812', osm_rotulolon='-75.35770524781857' WHERE id=692; -- La Unión / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.732195424175948', osm_rotulolon='-75.79280343417011' WHERE id=714; -- Liborina / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.54023028627538', osm_rotulolon='-74.73262246785825' WHERE id=732; -- Maceo / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.185270359096966', osm_rotulolon='-75.31664592436151' WHERE id=754; -- Marinilla / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.267368163922541', osm_rotulolon='-75.6185531458131' WHERE id=38; -- Medellín / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.925856245921183', osm_rotulolon='-75.53323682263569' WHERE id=785; -- Montebello / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.819138908690615', osm_rotulolon='-76.7358148315169' WHERE id=796; -- Murindó / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.569989367581862', osm_rotulolon='-75.2006145933524' WHERE id=804; -- Nariño / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.978655609031195', osm_rotulolon='-74.81515970202022' WHERE id=818; -- Nechí / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='8.461343171851395', osm_rotulolon='-76.66633123465314' WHERE id=811; -- Necoclí / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.61092162971778', osm_rotulolon='-75.78176835386667' WHERE id=852; -- Olaya / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.251563622448933', osm_rotulolon='-75.23215901454493' WHERE id=910; -- Peñol / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.056826449784632', osm_rotulolon='-75.87815259915705' WHERE id=912; -- Peque / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.819694887427149', osm_rotulolon='-75.87048227620627' WHERE id=959; -- Pueblorrico / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.463926288410295', osm_rotulolon='-74.53889000168935' WHERE id=961; -- Puerto Berrío / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.142777447923198', osm_rotulolon='-74.66147567926744' WHERE id=968; -- Puerto Nare / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.968987683188041', osm_rotulolon='-74.70127735862611' WHERE id=976; -- Puerto Triunfo / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.943770203513902', osm_rotulolon='-74.55274818067838' WHERE id=1015; -- Remedios / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.059847473389584', osm_rotulolon='-75.52199012105305' WHERE id=1020; -- Retiro / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.162660665890259', osm_rotulolon='-75.41638456525772' WHERE id=1026; -- Rionegro / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.896885121208848', osm_rotulolon='-75.80324584032586' WHERE id=1043; -- Sabanalarga / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.138476699290452', osm_rotulolon='-75.6106562457322' WHERE id=1044; -- Sabaneta / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.96748452401606', osm_rotulolon='-75.98449621741442' WHERE id=1049; -- Salgar / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.886169455880412', osm_rotulolon='-75.68646610195522' WHERE id=1052; -- San Andrés de Cuerquía / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.205774067095134', osm_rotulolon='-74.94241254086727' WHERE id=1055; -- San Carlos / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.892630956767355', osm_rotulolon='-75.00918758940901' WHERE id=1059; -- San Francisco / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.435075202071996', osm_rotulolon='-75.71367188280354' WHERE id=1065; -- San Jerónimo / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.819813337444967', osm_rotulolon='-75.67849606928397' WHERE id=1068; -- San José de La Montaña / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='8.710550763207477', osm_rotulolon='-76.54205656710204' WHERE id=1069; -- San Juan de Urabá / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.0282415023307', osm_rotulolon='-74.94250724291805' WHERE id=1076; -- San Luis / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.438866061698505', osm_rotulolon='-75.57845478489462' WHERE id=1081; -- San Pedro de Los Milagros / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='8.38271540881752', osm_rotulolon='-76.31859749283194' WHERE id=1083; -- San Pedro de Urabá / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.304939464995871', osm_rotulolon='-75.01066807581493' WHERE id=1087; -- San Rafael / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.443946257922764', osm_rotulolon='-74.94235912317023' WHERE id=1097; -- San Roque / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.31435530560769', osm_rotulolon='-75.33577796474607' WHERE id=1104; -- San Vicente Ferrer / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.880490160719032', osm_rotulolon='-75.59512150665049' WHERE id=1116; -- Santa Bárbara / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.52860423583288', osm_rotulolon='-75.92225888829938' WHERE id=727; -- Santa Fé de Antioquia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.707175847360845', osm_rotulolon='-75.47160520957235' WHERE id=1134; -- Santa Rosa de Osos / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.491964831684426', osm_rotulolon='-75.15229064805297' WHERE id=1143; -- Santo Domingo / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.253551079137314', osm_rotulolon='-74.5963039125817' WHERE id=1194; -- Segovia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.754063966019722', osm_rotulolon='-75.10013224387372' WHERE id=1221; -- Sonsón / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.519675506541286', osm_rotulolon='-75.75459861865633' WHERE id=1232; -- Sopetrán / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.67488010284687', osm_rotulolon='-75.72223878641856' WHERE id=1269; -- Támesis / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.47196804210434', osm_rotulolon='-75.41118319817897' WHERE id=1274; -- Tarazá / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.869446153594113', osm_rotulolon='-75.83260749446833' WHERE id=1277; -- Tarso / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.06001250945305', osm_rotulolon='-75.80209669646554' WHERE id=1318; -- Titiribí / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.017193893440323', osm_rotulolon='-75.71225768277546' WHERE id=1329; -- Toledo / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.909795011412807', osm_rotulolon='-76.1161460454253' WHERE id=1355; -- Uramita / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.294555126232154', osm_rotulolon='-76.25894001925123' WHERE id=1362; -- Urrao / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.246708803502633', osm_rotulolon='-75.39718966953994' WHERE id=1365; -- Valdivia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.651253259354199', osm_rotulolon='-75.63717047825307' WHERE id=1370; -- Valparaíso / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.824104837907115', osm_rotulolon='-74.76684259430984' WHERE id=1371; -- Vegachí / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='5.955055652483236', osm_rotulolon='-75.78201913942658' WHERE id=1376; -- Venecia / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.466776378037683', osm_rotulolon='-76.72569371998848' WHERE id=1392; -- Vigía del Fuerte / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.722078904609154', osm_rotulolon='-74.77275104805169' WHERE id=1413; -- Yalí / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.000646490448264', osm_rotulolon='-75.44870332896106' WHERE id=1416; -- Yarumal / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.653805987809068', osm_rotulolon='-74.8528281107571' WHERE id=1420; -- Yolombó / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.912470232294573', osm_rotulolon='-74.21040887489255' WHERE id=1423; -- Yondó / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='7.494629379955587', osm_rotulolon='-74.91751218094194' WHERE id=1426; -- Zaragoza / Antioquia
      UPDATE msip_municipio SET osm_rotulolat='6.776139544151655', osm_rotulolon='-70.5771329323335' WHERE id=18; -- Arauca / Arauca
      UPDATE msip_municipio SET osm_rotulolat='6.804320148132738', osm_rotulolon='-71.26981196336143' WHERE id=1056; -- Arauquita / Arauca
      UPDATE msip_municipio SET osm_rotulolat='6.301004431844053', osm_rotulolon='-70.15768058600946' WHERE id=377; -- Cravo Norte / Arauca
      UPDATE msip_municipio SET osm_rotulolat='6.680430952202901', osm_rotulolon='-71.80232362699064' WHERE id=539; -- Fortul / Arauca
      UPDATE msip_municipio SET osm_rotulolat='6.471693088863303', osm_rotulolon='-71.01433516205267' WHERE id=975; -- Puerto Rondón / Arauca
      UPDATE msip_municipio SET osm_rotulolat='6.903013056029359', osm_rotulolon='-71.87675400571426' WHERE id=1193; -- Saravena / Arauca
      UPDATE msip_municipio SET osm_rotulolat='6.357148672943829', osm_rotulolon='-71.76882415677346' WHERE id=1279; -- Tame / Arauca
      UPDATE msip_municipio SET osm_rotulolat='10.785740463034106', osm_rotulolon='-74.92680756007263' WHERE id=1255; -- Baranoa / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='11.01848851196102', osm_rotulolon='-74.84641441341205' WHERE id=22; -- Barranquilla / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.40665165099167', osm_rotulolon='-74.8861374897795' WHERE id=175; -- Campo de La Cruz / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.490539432011863', osm_rotulolon='-74.8863661073055' WHERE id=191; -- Candelaria / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.894431391606851', osm_rotulolon='-74.89445533161775' WHERE id=502; -- Galapa / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.820931168387947', osm_rotulolon='-75.10075962119745' WHERE id=631; -- Juan de Acosta / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.639986647545896', osm_rotulolon='-75.16582039545014' WHERE id=729; -- Luruaco / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.850003343742072', osm_rotulolon='-74.81120460305995' WHERE id=746; -- Malambo / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.447564980400733', osm_rotulolon='-74.98096905441133' WHERE id=749; -- Manatí / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.690118945589896', osm_rotulolon='-74.76747458849648' WHERE id=880; -- Palmar de Varela / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.736760364010058', osm_rotulolon='-75.15364787140166' WHERE id=921; -- Piojó / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.769290132983077', osm_rotulolon='-74.85984110814555' WHERE id=932; -- Polonuevo / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.60526018147752', osm_rotulolon='-74.80372957759536' WHERE id=934; -- Ponedera / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.999421385200925', osm_rotulolon='-74.91228570200688' WHERE id=952; -- Puerto Colombia / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.50933239224557', osm_rotulolon='-75.12492815163031' WHERE id=1017; -- Repelón / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.800297009378793', osm_rotulolon='-74.77127240257124' WHERE id=1046; -- Sabanagrande / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.627273455582301', osm_rotulolon='-74.94956546550574' WHERE id=1047; -- Sabanalarga / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.35348427535996', osm_rotulolon='-74.95961144585438' WHERE id=1108; -- Santa Lucía / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.739237886377682', osm_rotulolon='-74.79383406337901' WHERE id=1129; -- Santo Tomás / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.901145713134058', osm_rotulolon='-74.78692372866936' WHERE id=1224; -- Soledad / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.31356682099067', osm_rotulolon='-74.9078194334078' WHERE id=1241; -- Suan / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.898601376626905', osm_rotulolon='-74.99149341056093' WHERE id=1342; -- Tubará / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='10.74372718235035', osm_rotulolon='-74.98249549590909' WHERE id=1363; -- Usiacurí / Atlántico
      UPDATE msip_municipio SET osm_rotulolat='4.6534649', osm_rotulolon='-74.0836453' WHERE id=24; -- Bogotá, D.C. / Bogotá, D.C.
      UPDATE msip_municipio SET osm_rotulolat='8.632386086858302', osm_rotulolon='-74.48644179317647' WHERE id=989; -- Achí / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.759840807331145', osm_rotulolon='-74.21358812458084' WHERE id=535; -- Altos del Rosario / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.325169395333234', osm_rotulolon='-74.11919714713527' WHERE id=726; -- Arenal / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.150924447117836', osm_rotulolon='-75.36855775228427' WHERE id=878; -- Arjona / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.231424041787912', osm_rotulolon='-75.0275674513721' WHERE id=1033; -- Arroyohondo / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.817368119833223', osm_rotulolon='-74.20170544471628' WHERE id=1197; -- Barranco de Loba / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.226913748350915', osm_rotulolon='-75.00270490255751' WHERE id=190; -- Calamar / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='7.219080865854902', osm_rotulolon='-74.14064248044096' WHERE id=232; -- Cantagallo / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.038844075141249', osm_rotulolon='-75.73663508989137' WHERE id=31; -- Cartagena de Indias / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.235141335121957', osm_rotulolon='-74.69290064719424' WHERE id=283; -- Cicuco / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.55758190354036', osm_rotulolon='-75.29858040207168' WHERE id=379; -- Clemencia / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.535130674851331', osm_rotulolon='-74.90703816552141' WHERE id=361; -- Córdoba / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.693068226333025', osm_rotulolon='-75.15656166756614' WHERE id=418; -- El Carmen de Bolívar / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.021695065296804', osm_rotulolon='-74.93222655107581' WHERE id=431; -- El Guamo / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.864030311386772', osm_rotulolon='-73.92166033098273' WHERE id=466; -- El Peñón / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.997217980283104', osm_rotulolon='-74.20628407826953' WHERE id=541; -- Hatillo de Loba / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.165216464867331', osm_rotulolon='-74.71343720733391' WHERE id=740; -- Magangué / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.162123869042853', osm_rotulolon='-75.19560626744106' WHERE id=745; -- Mahates / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.056208939284337', osm_rotulolon='-74.21372835044498' WHERE id=753; -- Margarita / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.982124579912174', osm_rotulolon='-75.33559477528618' WHERE id=759; -- María La Baja / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='7.906898418639818', osm_rotulolon='-74.49392757501967' WHERE id=775; -- Montecristo / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.24246119143591', osm_rotulolon='-74.00997501831355' WHERE id=791; -- Morales / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.494701393196458', osm_rotulolon='-74.13558301454096' WHERE id=812; -- Norosí / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.882571642064372', osm_rotulolon='-74.38570714363708' WHERE id=920; -- Pinillos / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.717094659203562', osm_rotulolon='-73.86127582493381' WHERE id=966; -- Regidor / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.429958102751625', osm_rotulolon='-74.08356722661657' WHERE id=1014; -- Río Viejo / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.371553624955833', osm_rotulolon='-75.0789302677506' WHERE id=1034; -- San Cristóbal / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.359137593352912', osm_rotulolon='-75.21391372634545' WHERE id=1053; -- San Estanislao / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.0953320390592', osm_rotulolon='-74.34859157590662' WHERE id=1057; -- San Fernando / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.84825784331899', osm_rotulolon='-75.09806961675322' WHERE id=1062; -- San Jacinto / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.225947510889672', osm_rotulolon='-74.64905760077944' WHERE id=1064; -- San Jacinto del Cauca / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.975404968752164', osm_rotulolon='-75.06244913031827' WHERE id=1066; -- San Juan Nepomuceno / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.76244103031631', osm_rotulolon='-74.0317120326864' WHERE id=1086; -- San Martín de Loba / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='7.408323741572051', osm_rotulolon='-74.16319603736558' WHERE id=1095; -- San Pablo / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.632835453941853', osm_rotulolon='-75.25675876197101' WHERE id=1103; -- Santa Catalina / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.132523982614607', osm_rotulolon='-74.52834845740205' WHERE id=787; -- Santa Cruz de Mompox / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='7.733344936664674', osm_rotulolon='-74.31002174184937' WHERE id=1126; -- Santa Rosa / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='7.733344936664674', osm_rotulolon='-74.31002174184937' WHERE id=1138; -- Santa Rosa del Sur / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='7.824430723823456', osm_rotulolon='-74.03701428855352' WHERE id=1204; -- Simití / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.332099603122868', osm_rotulolon='-75.12253366619137' WHERE id=1229; -- Soplaviento / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.305895516578149', osm_rotulolon='-74.6597322447554' WHERE id=1260; -- Talaigua Nuevo / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='8.501717071137046', osm_rotulolon='-74.32467310487912' WHERE id=1322; -- Tiquisio / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.345847691502787', osm_rotulolon='-75.37606060286848' WHERE id=1347; -- Turbaco / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.233125850694067', osm_rotulolon='-75.44780004981924' WHERE id=1350; -- Turbaná / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='10.451670877343968', osm_rotulolon='-75.26523361690452' WHERE id=1394; -- Villanueva / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='9.761455236324249', osm_rotulolon='-74.89188037264408' WHERE id=1424; -- Zambrano / Bolívar
      UPDATE msip_municipio SET osm_rotulolat='4.955300640023217', osm_rotulolon='-73.39365432228242' WHERE id=373; -- Almeida / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.427254823107888', osm_rotulolon='-72.88883417754408' WHERE id=789; -- Aquitania / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.763088953614729', osm_rotulolon='-73.44925349023258' WHERE id=864; -- Arcabuco / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.008195947733772', osm_rotulolon='-72.8955268217961' WHERE id=1386; -- Belén / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.224707110468908', osm_rotulolon='-73.1046059203866' WHERE id=1450; -- Berbeo / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.924764868306832', osm_rotulolon='-72.85824198516185' WHERE id=1455; -- Betéitiva / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.343345703253831', osm_rotulolon='-72.62256585690062' WHERE id=1460; -- Boavita / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.436101176220129', osm_rotulolon='-73.38630830521495' WHERE id=81; -- Boyacá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.658597331653928', osm_rotulolon='-73.91115710459628' WHERE id=82; -- Briceño / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.483218800026112', osm_rotulolon='-73.95709902133359' WHERE id=87; -- Buenavista / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.847588325899211', osm_rotulolon='-72.87547529644047' WHERE id=115; -- Busbanzá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.574289023994543', osm_rotulolon='-73.88356790494989' WHERE id=167; -- Caldas / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.017278120702931', osm_rotulolon='-73.15261886132188' WHERE id=171; -- Campohermoso / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.962044622653516', osm_rotulolon='-72.96691301201395' WHERE id=238; -- Cerinza / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.202049754725569', osm_rotulolon='-73.34564791517349' WHERE id=257; -- Chinavita / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.626935418474736', osm_rotulolon='-73.81625654574074' WHERE id=264; -- Chiquinquirá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.640922951557904', osm_rotulolon='-73.45267669485291' WHERE id=398; -- Chíquiza / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.663577364928626', osm_rotulolon='-72.43703901432643' WHERE id=276; -- Chiscas / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.060022336774787', osm_rotulolon='-72.4495086688779' WHERE id=279; -- Chita / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.980587432870394', osm_rotulolon='-73.4313521429622' WHERE id=281; -- Chitaraque / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.570399524326067', osm_rotulolon='-73.26442393400531' WHERE id=282; -- Chivatá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='4.872051622765172', osm_rotulolon='-73.37392609867862' WHERE id=406; -- Chivor / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.393477213103205', osm_rotulolon='-73.28345259983301' WHERE id=286; -- Ciénega / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.689364640918162', osm_rotulolon='-73.33711305844558' WHERE id=338; -- Cómbita / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.445362915397324', osm_rotulolon='-74.03105274848141' WHERE id=359; -- Coper / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.82365361854723', osm_rotulolon='-72.8539773751425' WHERE id=365; -- Corrales / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.510614814697193', osm_rotulolon='-72.74865502111346' WHERE id=369; -- Covarachía / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.890650789241349', osm_rotulolon='-72.24793880095069' WHERE id=381; -- Cubará / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.526986307967348', osm_rotulolon='-73.44926184484348' WHERE id=384; -- Cucaita / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.590337474945671', osm_rotulolon='-72.94754360426064' WHERE id=389; -- Cuítiva / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.90783833877852', osm_rotulolon='-73.09080529925508' WHERE id=409; -- Duitama / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.35642718895188', osm_rotulolon='-72.42197826382623' WHERE id=419; -- El Cocuy / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.506518029009971', osm_rotulolon='-72.48381451906077' WHERE id=430; -- El Espino / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.669181778864686', osm_rotulolon='-73.02956511843902' WHERE id=476; -- Firavitoba / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.858552444165456', osm_rotulolon='-72.92934672028079' WHERE id=479; -- Floresta / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.746093776036718', osm_rotulolon='-73.54673877452674' WHERE id=499; -- Gachantivá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.805499365811245', osm_rotulolon='-72.73282357138265' WHERE id=504; -- Gámeza / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.086976374298844', osm_rotulolon='-73.31121120939142' WHERE id=509; -- Garagoa / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.44560912987697', osm_rotulolon='-72.51858821086803' WHERE id=559; -- Guacamayas / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.018835853816696', osm_rotulolon='-73.49008901938797' WHERE id=579; -- Guateque / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='4.929656954303794', osm_rotulolon='-73.49717598972133' WHERE id=583; -- Guayatá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.557141918264482', osm_rotulolon='-72.28450930862448' WHERE id=590; -- Güicán de La Sierra / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.618504971789364', osm_rotulolon='-72.97153220684868' WHERE id=620; -- Iza / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.380117051850028', osm_rotulolon='-73.37973204522937' WHERE id=624; -- Jenesano / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.132823642926925', osm_rotulolon='-72.59051661879386' WHERE id=625; -- Jericó / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.114309034227081', osm_rotulolon='-73.45710400779907' WHERE id=644; -- La Capilla / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.266326132821658', osm_rotulolon='-72.57206374489466' WHERE id=701; -- La Uvita / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.518090213079657', osm_rotulolon='-74.22722152399258' WHERE id=699; -- La Victoria / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.550402969328767', osm_rotulolon='-72.62087472908627' WHERE id=639; -- Labranzagrande / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='4.978471100489625', osm_rotulolon='-73.30404454975852' WHERE id=730; -- Macanal / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.568863615118471', osm_rotulolon='-74.02601257047426' WHERE id=757; -- Maripí / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.15064861594338', osm_rotulolon='-73.1817071099204' WHERE id=772; -- Miraflores / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.708233103802382', osm_rotulolon='-72.69909568650858' WHERE id=782; -- Mongua / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.697941972933119', osm_rotulolon='-72.83673133643427' WHERE id=783; -- Monguí / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.861199753811151', osm_rotulolon='-73.56463734136118' WHERE id=788; -- Moniquirá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.605022664346996', osm_rotulolon='-73.3829891801987' WHERE id=797; -- Motavita / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.520491416776635', osm_rotulolon='-74.12936603134567' WHERE id=801; -- Muzo / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.778375916033629', osm_rotulolon='-72.93454232387401' WHERE id=814; -- Nobsa / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.355581343112525', osm_rotulolon='-73.45389548706207' WHERE id=817; -- Nuevo Colón / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.609138555173415', osm_rotulolon='-73.28244065314375' WHERE id=851; -- Oicatá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.736672023175457', osm_rotulolon='-74.20912689968088' WHERE id=858; -- Otanche / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.150227266444216', osm_rotulolon='-73.39370209657373' WHERE id=865; -- Pachavita / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.099776868968668', osm_rotulolon='-72.99956525250754' WHERE id=870; -- Páez / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.829005711126419', osm_rotulolon='-73.15568261952968' WHERE id=871; -- Paipa / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.384246573350113', osm_rotulolon='-72.70134720808613' WHERE id=876; -- Pajarito / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.440452198059317', osm_rotulolon='-72.46834498708427' WHERE id=885; -- Panqueba / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.685926803432503', osm_rotulolon='-74.00972468914719' WHERE id=897; -- Pauna / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.64545554448127', osm_rotulolon='-72.38744266368414' WHERE id=899; -- Paya / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.025464173665361', osm_rotulolon='-72.77439749735775' WHERE id=904; -- Paz de Río / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.491572922623481', osm_rotulolon='-73.08958219111298' WHERE id=911; -- Pesca / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.745498430597275', osm_rotulolon='-72.46112158677917' WHERE id=925; -- Pisba / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.92922305838013', osm_rotulolon='-74.46567006452123' WHERE id=949; -- Puerto Boyacá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.565062141042823', osm_rotulolon='-74.2289488875742' WHERE id=965; -- Quípama / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.318672811111253', osm_rotulolon='-73.32154201695833' WHERE id=985; -- Ramiriquí / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.363148288354872', osm_rotulolon='-73.21148590640618' WHERE id=1037; -- Rondón / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.697487087207831', osm_rotulolon='-73.77196940945939' WHERE id=1045; -- Saboyá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.583498084516372', osm_rotulolon='-73.54984547851943' WHERE id=1048; -- Sáchica / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.462076243093856', osm_rotulolon='-73.53944495194843' WHERE id=1051; -- Samacá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.2328722295065', osm_rotulolon='-73.05627234603087' WHERE id=1074; -- San Eduardo / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.994379833343614', osm_rotulolon='-73.54514169273855' WHERE id=1080; -- San José de Pare / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='4.810309183059572', osm_rotulolon='-73.14074143373452' WHERE id=1085; -- San Luis de Gaceno / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.406682832881737', osm_rotulolon='-72.56423200116595' WHERE id=1102; -- San Mateo / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.531830922024145', osm_rotulolon='-73.75240113447778' WHERE id=1109; -- San Miguel de Sema / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.67434288900076', osm_rotulolon='-74.11707855492726' WHERE id=1120; -- San Pablo de Borbur / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='4.808020041353132', osm_rotulolon='-73.26035913073095' WHERE id=1142; -- Santa María / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.905151977659885', osm_rotulolon='-72.99928768356494' WHERE id=1147; -- Santa Rosa de Viterbo / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.728781902250964', osm_rotulolon='-73.60831722329768' WHERE id=1150; -- Santa Sofía / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.058655159942081', osm_rotulolon='-73.48848076925435' WHERE id=1135; -- Santana / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.129445142470424', osm_rotulolon='-72.74045490923658' WHERE id=1190; -- Sativanorte / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.074128297450772', osm_rotulolon='-72.72615944117962' WHERE id=1191; -- Sativasur / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.489669881433533', osm_rotulolon='-73.21581217635371' WHERE id=1198; -- Siachoque / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.315065103966268', osm_rotulolon='-72.70338940416308' WHERE id=1215; -- Soatá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.962875591008525', osm_rotulolon='-72.68786329469376' WHERE id=1223; -- Socha / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.931507797478109', osm_rotulolon='-72.56654682393824' WHERE id=1219; -- Socotá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.669232086553662', osm_rotulolon='-72.88871481514639' WHERE id=1226; -- Sogamoso / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='4.969434735443891', osm_rotulolon='-73.4330955959277' WHERE id=1231; -- Somondoco / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.578813741800154', osm_rotulolon='-73.44693276484759' WHERE id=1233; -- Sora / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.481696433031644', osm_rotulolon='-73.32705889996646' WHERE id=1235; -- Soracá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.806577813040134', osm_rotulolon='-73.2623241682514' WHERE id=1234; -- Sotaquirá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.212866717971332', osm_rotulolon='-72.7356821422894' WHERE id=1248; -- Susacón / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.635494247130603', osm_rotulolon='-73.62801300495887' WHERE id=1249; -- Sutamarchán / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.029866120067862', osm_rotulolon='-73.43757088284401' WHERE id=1253; -- Sutatenza / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.880608827000358', osm_rotulolon='-72.72805703335625' WHERE id=1275; -- Tasco / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.073020290603104', osm_rotulolon='-73.43273347375093' WHERE id=1284; -- Tenza / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.300219040225876', osm_rotulolon='-73.39784291631112' WHERE id=1309; -- Tibaná / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.744962119670777', osm_rotulolon='-73.01349113502461' WHERE id=1311; -- Tibasosa / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.571689448585336', osm_rotulolon='-73.68073121965686' WHERE id=1316; -- Tinjacá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.424168785110419', osm_rotulolon='-72.69840184978257' WHERE id=1321; -- Tipacoque / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.578901781059983', osm_rotulolon='-73.1670298761396' WHERE id=1324; -- Toca / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.913434099907096', osm_rotulolon='-73.49528380044644' WHERE id=1327; -- Togüí / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.767289047005593', osm_rotulolon='-72.84359496869364' WHERE id=1333; -- Tópaga / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.480199420159531', osm_rotulolon='-73.01562422640562' WHERE id=1335; -- Tota / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.516597211382388', osm_rotulolon='-73.38637542155737' WHERE id=54; -- Tunja / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.719945087105561', osm_rotulolon='-73.93068046927304' WHERE id=1343; -- Tununguá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.306089674411013', osm_rotulolon='-73.5125714540771' WHERE id=1346; -- Turmequé / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.672277537554053', osm_rotulolon='-73.19047816451732' WHERE id=1349; -- Tuta / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='6.078465929694484', osm_rotulolon='-72.86303264550308' WHERE id=1352; -- Tutazá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.22123', osm_rotulolon='-73.457038' WHERE id=1356; -- Úmbita / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.381937292058449', osm_rotulolon='-73.52024344093715' WHERE id=1377; -- Ventaquemada / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.664771800632349', osm_rotulolon='-73.51668250464915' WHERE id=709; -- Villa de Leyva / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.439563697249023', osm_rotulolon='-73.26876647279546' WHERE id=1402; -- Viracachá / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.274655744680532', osm_rotulolon='-73.17781673719902' WHERE id=1428; -- Zetaquira / Boyacá
      UPDATE msip_municipio SET osm_rotulolat='5.5921764484901', osm_rotulolon='-75.46208717237668' WHERE id=148; -- Aguadas / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.182028241428833', osm_rotulolon='-75.7507240862371' WHERE id=725; -- Anserma / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.261272420020839', osm_rotulolon='-75.48495968706008' WHERE id=846; -- Aranzazu / Caldas
      UPDATE msip_municipio SET osm_rotulolat='4.991754695143171', osm_rotulolon='-75.81822259794366' WHERE id=1403; -- Belalcázar / Caldas
      UPDATE msip_municipio SET osm_rotulolat='4.997469378095071', osm_rotulolon='-75.66987315404876' WHERE id=259; -- Chinchiná / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.28638804972755', osm_rotulolon='-75.5967260587605' WHERE id=474; -- Filadelfia / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.523443461585781', osm_rotulolon='-74.74672728211263' WHERE id=645; -- La Dorada / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.386515156331121', osm_rotulolon='-75.56144426088738' WHERE id=654; -- La Merced / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.09401197893138', osm_rotulolon='-75.49664099915864' WHERE id=37; -- Manizales / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.236555554201151', osm_rotulolon='-75.1629573592599' WHERE id=747; -- Manzanares / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.496366113578673', osm_rotulolon='-75.60985962128278' WHERE id=758; -- Marmato / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.305369621343592', osm_rotulolon='-75.0493639884195' WHERE id=762; -- Marquetalia / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.211574932885692', osm_rotulolon='-75.28081541470439' WHERE id=764; -- Marulanda / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.16999830113856', osm_rotulolon='-75.50057632500204' WHERE id=807; -- Neira / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.632301166306172', osm_rotulolon='-74.87582016248854' WHERE id=819; -- Norcasia / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.487604675876896', osm_rotulolon='-75.48704396810261' WHERE id=869; -- Pácora / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.059597032667469', osm_rotulolon='-75.65821478919577' WHERE id=889; -- Palestina / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.419568701390015', osm_rotulolon='-75.19724867573193' WHERE id=909; -- Pensilvania / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.442901455035868', osm_rotulolon='-75.75962094110756' WHERE id=1024; -- Riosucio / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.113544954230743', osm_rotulolon='-75.76457694826121' WHERE id=1032; -- Risaralda / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.380708439658671', osm_rotulolon='-75.41424550259383' WHERE id=1060; -- Salamina / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.543108197172612', osm_rotulolon='-75.00910525667604' WHERE id=1077; -- Samaná / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.075449103527173', osm_rotulolon='-75.81183516894497' WHERE id=1082; -- San José / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.466647715455736', osm_rotulolon='-75.65025757319788' WHERE id=1252; -- Supía / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.424026073256203', osm_rotulolon='-74.84588972604747' WHERE id=1384; -- Victoria / Caldas
      UPDATE msip_municipio SET osm_rotulolat='4.94903146019807', osm_rotulolon='-75.45313779674221' WHERE id=1393; -- Villamaría / Caldas
      UPDATE msip_municipio SET osm_rotulolat='5.04252298927941', osm_rotulolon='-75.87225614224204' WHERE id=1400; -- Viterbo / Caldas
      UPDATE msip_municipio SET osm_rotulolat='1.228028973344663', osm_rotulolon='-75.86916149220636' WHERE id=494; -- Albania / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.463018505180323', osm_rotulolon='-75.90103113262946' WHERE id=1457; -- Belén de Los Andaquíes / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='0.657871794607269', osm_rotulolon='-74.07004736417835' WHERE id=215; -- Cartagena del Chairá / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.082091958435316', osm_rotulolon='-75.946752675658' WHERE id=341; -- Curillo / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.878566267110758', osm_rotulolon='-75.21062582780857' WHERE id=428; -- El Doncello / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.775976212743543', osm_rotulolon='-75.28566622035504' WHERE id=447; -- El Paujíl / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.746723308925739', osm_rotulolon='-75.59562481588983' WHERE id=33; -- Florencia / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.345132287432677', osm_rotulolon='-75.23350320946851' WHERE id=712; -- La Montañita / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.148654437593465', osm_rotulolon='-75.37187655199482' WHERE id=776; -- Milán / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.396971996272239', osm_rotulolon='-75.69573842843772' WHERE id=798; -- Morelia / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.972089177255488', osm_rotulolon='-75.09688957605351' WHERE id=977; -- Puerto Rico / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.310022775833703', osm_rotulolon='-76.10258880021955' WHERE id=1021; -- San José del Fragua / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.752342613295667', osm_rotulolon='-74.23201128041792' WHERE id=1214; -- San Vicente del Caguán / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='0.279534701091824', osm_rotulolon='-73.18181298484501' WHERE id=1220; -- Solano / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='0.931335053044101', osm_rotulolon='-75.69433342611627' WHERE id=1262; -- Solita / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='1.082190533866754', osm_rotulolon='-75.59456852070625' WHERE id=1374; -- Valparaíso / Caquetá
      UPDATE msip_municipio SET osm_rotulolat='5.104759665122253', osm_rotulolon='-72.50870930819526' WHERE id=59; -- Aguazul / Casanare
      UPDATE msip_municipio SET osm_rotulolat='5.212618718754896', osm_rotulolon='-72.86402386974927' WHERE id=202; -- Chámeza / Casanare
      UPDATE msip_municipio SET osm_rotulolat='6.059000106904163', osm_rotulolon='-70.87904566996897' WHERE id=141; -- Hato Corozal / Casanare
      UPDATE msip_municipio SET osm_rotulolat='6.174605288887249', osm_rotulolon='-72.3602841250177' WHERE id=173; -- La Salina / Casanare
      UPDATE msip_municipio SET osm_rotulolat='4.713582131426157', osm_rotulolon='-72.27464486429318' WHERE id=177; -- Maní / Casanare
      UPDATE msip_municipio SET osm_rotulolat='4.829509725567973', osm_rotulolon='-72.86139229625' WHERE id=240; -- Monterrey / Casanare
      UPDATE msip_municipio SET osm_rotulolat='5.50359839550203', osm_rotulolon='-72.13076481049869' WHERE id=386; -- Nunchía / Casanare
      UPDATE msip_municipio SET osm_rotulolat='4.907273273574984', osm_rotulolon='-71.57409680260895' WHERE id=397; -- Orocué / Casanare
      UPDATE msip_municipio SET osm_rotulolat='5.868605922836062', osm_rotulolon='-70.8771143426453' WHERE id=443; -- Paz de Ariporo / Casanare
      UPDATE msip_municipio SET osm_rotulolat='5.636522610842596', osm_rotulolon='-71.92704140890666' WHERE id=460; -- Pore / Casanare
      UPDATE msip_municipio SET osm_rotulolat='5.272045977783487', osm_rotulolon='-72.78728002222962' WHERE id=483; -- Recetor / Casanare
      UPDATE msip_municipio SET osm_rotulolat='4.810296947911933', osm_rotulolon='-72.99964480259735' WHERE id=542; -- Sabanalarga / Casanare
      UPDATE msip_municipio SET osm_rotulolat='6.048408354530629', osm_rotulolon='-72.21123740798551' WHERE id=558; -- Sácama / Casanare
      UPDATE msip_municipio SET osm_rotulolat='5.187459434328856', osm_rotulolon='-71.58510180913353' WHERE id=585; -- San Luis de Palenque / Casanare
      UPDATE msip_municipio SET osm_rotulolat='5.87815348506904', osm_rotulolon='-72.21859997523512' WHERE id=697; -- Támara / Casanare
      UPDATE msip_municipio SET osm_rotulolat='4.725130505566993', osm_rotulolon='-72.60509707300064' WHERE id=713; -- Tauramena / Casanare
      UPDATE msip_municipio SET osm_rotulolat='4.501858300150916', osm_rotulolon='-72.82270830552343' WHERE id=756; -- Villanueva / Casanare
      UPDATE msip_municipio SET osm_rotulolat='5.245653598207994', osm_rotulolon='-72.22734092085773' WHERE id=58; -- Yopal / Casanare
      UPDATE msip_municipio SET osm_rotulolat='1.912371944795059', osm_rotulolon='-76.83951839279139' WHERE id=372; -- Almaguer / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.327182860043476', osm_rotulolon='-77.27762159358217' WHERE id=847; -- Argelia / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.082714805645248', osm_rotulolon='-77.22272596180447' WHERE id=1211; -- Balboa / Cauca
      UPDATE msip_municipio SET osm_rotulolat='1.866288570489442', osm_rotulolon='-76.99235118555278' WHERE id=77; -- Bolívar / Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.005904696698291', osm_rotulolon='-76.6288266273556' WHERE id=108; -- Buenos Aires / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.648308283663439', osm_rotulolon='-76.7066516756795' WHERE id=164; -- Cajibío / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.834233968662417', osm_rotulolon='-76.48803404112356' WHERE id=174; -- Caldono / Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.072046897800212', osm_rotulolon='-76.37435069555262' WHERE id=192; -- Caloto / Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.142628231939244', osm_rotulolon='-76.19966577988241' WHERE id=360; -- Corinto / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.52003126366517', osm_rotulolon='-77.04593333259083' WHERE id=449; -- El Tambo / Cauca
      UPDATE msip_municipio SET osm_rotulolat='1.693901540738013', osm_rotulolon='-77.09052512384413' WHERE id=496; -- Florencia / Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.144337561500782', osm_rotulolon='-76.39807519576276' WHERE id=540; -- Guachené / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.387331785120438', osm_rotulolon='-77.67703123268151' WHERE id=566; -- Guapi / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.49836226452275', osm_rotulolon='-76.16595413188277' WHERE id=610; -- Inzá / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.849499978032814', osm_rotulolon='-76.31284320784152' WHERE id=621; -- Jambaló / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.20107577594229', osm_rotulolon='-76.7971461034345' WHERE id=657; -- La Sierra / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.052607450720563', osm_rotulolon='-76.76367638272245' WHERE id=661; -- La Vega / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.986830806461374', osm_rotulolon='-77.25673314010209' WHERE id=723; -- López de Micay / Cauca
      UPDATE msip_municipio SET osm_rotulolat='1.783664908504544', osm_rotulolon='-77.19952412251871' WHERE id=770; -- Mercaderes / Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.235734740128954', osm_rotulolon='-76.20318865651878' WHERE id=773; -- Miranda / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.812309050369239', osm_rotulolon='-76.76571480131251' WHERE id=792; -- Morales / Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.19271029607302', osm_rotulolon='-76.33824486950965' WHERE id=868; -- Padilla / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.705227213137462', osm_rotulolon='-76.046982450855' WHERE id=873; -- Páez / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.117382140131346', osm_rotulolon='-77.08838594223064' WHERE id=898; -- Patía / Cauca
      UPDATE msip_municipio SET osm_rotulolat='1.186016528010148', osm_rotulolon='-76.2917358000895' WHERE id=900; -- Piamonte / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.723437411183439', osm_rotulolon='-76.59178804712596' WHERE id=916; -- Piendamó - Tunía / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.457903918073308', osm_rotulolon='-76.60283746166746' WHERE id=46; -- Popayán / Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.265640889346745', osm_rotulolon='-76.43100101408588' WHERE id=956; -- Puerto Tejada / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.272421271643891', osm_rotulolon='-76.47001482758589' WHERE id=970; -- Puracé / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.24498831391636', osm_rotulolon='-76.75849156064314' WHERE id=1040; -- Rosas / Cauca
      UPDATE msip_municipio SET osm_rotulolat='1.861557480436596', osm_rotulolon='-76.7422093808644' WHERE id=1146; -- San Sebastián / Cauca
      UPDATE msip_municipio SET osm_rotulolat='1.4633084884228', osm_rotulolon='-76.55698093849216' WHERE id=1175; -- Santa Rosa / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.986345771163075', osm_rotulolon='-76.52286502233468' WHERE id=1152; -- Santander de Quilichao / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.677798522803771', osm_rotulolon='-76.36622182736457' WHERE id=1203; -- Silvia / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.236871039447338', osm_rotulolon='-76.63741074510867' WHERE id=1230; -- Sotará Paispamba / Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.07655283810651', osm_rotulolon='-74.82264587579172' WHERE id=1259; -- Suárez / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.059443153096938', osm_rotulolon='-76.92187179273543' WHERE id=1263; -- Sucre / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.381678651825603', osm_rotulolon='-76.73412983044825' WHERE id=1315; -- Timbío / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.710708742196916', osm_rotulolon='-77.49233501809776' WHERE id=1317; -- Timbiquí / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.978081017860609', osm_rotulolon='-76.20669303305691' WHERE id=1334; -- Toribío / Cauca
      UPDATE msip_municipio SET osm_rotulolat='2.518411240542189', osm_rotulolon='-76.4087893438998' WHERE id=1339; -- Totoró / Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.187188084256567', osm_rotulolon='-76.46412085244755' WHERE id=1360; -- Villa Rica / Cauca
      UPDATE msip_municipio SET osm_rotulolat='8.260913679198506', osm_rotulolon='-73.64606539276912' WHERE id=88; -- Aguachica / Cesar
      UPDATE msip_municipio SET osm_rotulolat='9.950790506978601', osm_rotulolon='-73.27179189647786' WHERE id=149; -- Agustín Codazzi / Cesar
      UPDATE msip_municipio SET osm_rotulolat='9.513319427661573', osm_rotulolon='-73.95287996732662' WHERE id=571; -- Astrea / Cesar
      UPDATE msip_municipio SET osm_rotulolat='9.729182695298663', osm_rotulolon='-73.30549979963317' WHERE id=768; -- Becerril / Cesar
      UPDATE msip_municipio SET osm_rotulolat='9.946399126102547', osm_rotulolon='-73.88153082858169' WHERE id=1011; -- Bosconia / Cesar
      UPDATE msip_municipio SET osm_rotulolat='9.204225833454455', osm_rotulolon='-73.67964483618503' WHERE id=261; -- Chimichagua / Cesar
      UPDATE msip_municipio SET osm_rotulolat='9.390532605373568', osm_rotulolon='-73.53815624761158' WHERE id=266; -- Chiriguaná / Cesar
      UPDATE msip_municipio SET osm_rotulolat='9.246823805991786', osm_rotulolon='-73.50496729524002' WHERE id=391; -- Curumaní / Cesar
      UPDATE msip_municipio SET osm_rotulolat='10.203500829406636', osm_rotulolon='-73.9011811711179' WHERE id=410; -- El Copey / Cesar
      UPDATE msip_municipio SET osm_rotulolat='9.697809281527336', osm_rotulolon='-73.66122450652058' WHERE id=440; -- El Paso / Cesar
      UPDATE msip_municipio SET osm_rotulolat='8.330280813809965', osm_rotulolon='-73.70915072911104' WHERE id=501; -- Gamarra / Cesar
      UPDATE msip_municipio SET osm_rotulolat='8.40334814322437', osm_rotulolon='-73.38641250698934' WHERE id=551; -- González / Cesar
      UPDATE msip_municipio SET osm_rotulolat='8.648072366980259', osm_rotulolon='-73.64916511212624' WHERE id=649; -- La Gloria / Cesar
      UPDATE msip_municipio SET osm_rotulolat='9.54209647830335', osm_rotulolon='-73.36143164227205' WHERE id=691; -- La Jagua de Ibirico / Cesar
      UPDATE msip_municipio SET osm_rotulolat='10.094585232438545', osm_rotulolon='-73.3031217020337' WHERE id=1035; -- La Paz / Cesar
      UPDATE msip_municipio SET osm_rotulolat='10.39250952936002', osm_rotulolon='-73.0073241817216' WHERE id=760; -- Manaure Balcón del Cesar / Cesar
      UPDATE msip_municipio SET osm_rotulolat='8.952863650870874', osm_rotulolon='-73.57036587773146' WHERE id=872; -- Pailitas / Cesar
      UPDATE msip_municipio SET osm_rotulolat='8.758490115283005', osm_rotulolon='-73.6470840538482' WHERE id=924; -- Pelaya / Cesar
      UPDATE msip_municipio SET osm_rotulolat='10.474987423768361', osm_rotulolon='-73.60228311405021' WHERE id=943; -- Pueblo Bello / Cesar
      UPDATE msip_municipio SET osm_rotulolat='8.196003473983504', osm_rotulolon='-73.51146126267575' WHERE id=1025; -- Río de Oro / Cesar
      UPDATE msip_municipio SET osm_rotulolat='7.835566767374538', osm_rotulolon='-73.48271288635016' WHERE id=1181; -- San Alberto / Cesar
      UPDATE msip_municipio SET osm_rotulolat='10.102671335442116', osm_rotulolon='-73.37055127349558' WHERE id=1213; -- San Diego / Cesar
      UPDATE msip_municipio SET osm_rotulolat='7.914728684665989', osm_rotulolon='-73.58286054771463' WHERE id=1239; -- San Martín / Cesar
      UPDATE msip_municipio SET osm_rotulolat='8.905630444213825', osm_rotulolon='-73.75863177651725' WHERE id=1267; -- Tamalameque / Cesar
      UPDATE msip_municipio SET osm_rotulolat='10.312378586434091', osm_rotulolon='-73.5018708165554' WHERE id=55; -- Valledupar / Cesar
      UPDATE msip_municipio SET osm_rotulolat='8.44032180488074', osm_rotulolon='-77.28587918906905' WHERE id=987; -- Acandí / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.626468557909596', osm_rotulolon='-77.06579451166004' WHERE id=432; -- Alto Baudó / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.564820345215598', osm_rotulolon='-76.61705773220203' WHERE id=848; -- Atrato / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.484145295887145', osm_rotulolon='-76.19777929533639' WHERE id=1192; -- Bagadó / Chocó
      UPDATE msip_municipio SET osm_rotulolat='6.365299089719343', osm_rotulolon='-77.366343744899' WHERE id=1210; -- Bahía Solano / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.024188104380937', osm_rotulolon='-77.25936256544044' WHERE id=1237; -- Bajo Baudó / Chocó
      UPDATE msip_municipio SET osm_rotulolat='6.442944067216475', osm_rotulolon='-77.08800794801752' WHERE id=1465; -- Bojayá / Chocó
      UPDATE msip_municipio SET osm_rotulolat='6.998136197723379', osm_rotulolon='-76.96940420902656' WHERE id=213; -- Carmen del Darién / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.401388013807656', osm_rotulolon='-76.5581080388821' WHERE id=234; -- Cértegui / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.083260995835077', osm_rotulolon='-76.49817278426774' WHERE id=340; -- Condoto / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.365773543888889', osm_rotulolon='-76.77023221512376' WHERE id=172; -- El Cantón del San Pablo / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.808334426243957', osm_rotulolon='-76.19194557015099' WHERE id=423; -- El Carmen de Atrato / Chocó
      UPDATE msip_municipio SET osm_rotulolat='4.239878879465585', osm_rotulolon='-76.97848610989314' WHERE id=439; -- El Litoral del San Juan / Chocó
      UPDATE msip_municipio SET osm_rotulolat='4.828639594385654', osm_rotulolon='-76.80998335434711' WHERE id=618; -- Istmina / Chocó
      UPDATE msip_municipio SET osm_rotulolat='7.056857121067586', osm_rotulolon='-77.67096699680053' WHERE id=633; -- Juradó / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.58281368549448', osm_rotulolon='-76.4134021891796' WHERE id=717; -- Lloró / Chocó
      UPDATE msip_municipio SET osm_rotulolat='6.013190142455436', osm_rotulolon='-76.67359018522122' WHERE id=733; -- Medio Atrato / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.103134821833583', osm_rotulolon='-76.9894606513016' WHERE id=742; -- Medio Baudó / Chocó
      UPDATE msip_municipio SET osm_rotulolat='4.8252034183606', osm_rotulolon='-76.78107146327568' WHERE id=769; -- Medio San Juan / Chocó
      UPDATE msip_municipio SET osm_rotulolat='4.856321419055023', osm_rotulolon='-76.63778382772118' WHERE id=816; -- Nóvita / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.691878005357311', osm_rotulolon='-77.28742255423482' WHERE id=820; -- Nuquí / Chocó
      UPDATE msip_municipio SET osm_rotulolat='6.011741471885883', osm_rotulolon='-76.63365374786167' WHERE id=48; -- Quibdó / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.155940046240451', osm_rotulolon='-76.493763461337' WHERE id=967; -- Río Iró / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.547339220773901', osm_rotulolon='-76.82021168059968' WHERE id=1012; -- Río Quito / Chocó
      UPDATE msip_municipio SET osm_rotulolat='7.249441198370633', osm_rotulolon='-77.21301694743207' WHERE id=1028; -- Riosucio / Chocó
      UPDATE msip_municipio SET osm_rotulolat='4.968373085295691', osm_rotulolon='-76.26385020863461' WHERE id=1075; -- San José del Palmar / Chocó
      UPDATE msip_municipio SET osm_rotulolat='4.625109100878826', osm_rotulolon='-76.58327896161461' WHERE id=1207; -- Sipí / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.248917157677306', osm_rotulolon='-76.38762031618161' WHERE id=1266; -- Tadó / Chocó
      UPDATE msip_municipio SET osm_rotulolat='8.119011410651549', osm_rotulolon='-77.11674105642962' WHERE id=1307; -- Unguía / Chocó
      UPDATE msip_municipio SET osm_rotulolat='5.297905363219376', osm_rotulolon='-76.66831778316376' WHERE id=1323; -- Unión Panamericana / Chocó
      UPDATE msip_municipio SET osm_rotulolat='8.24952095552059', osm_rotulolon='-75.07983373862672' WHERE id=1117; -- Ayapel / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.209176771448197', osm_rotulolon='-75.43762053597322' WHERE id=1273; -- Buenavista / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.716354756651482', osm_rotulolon='-76.24070486587287' WHERE id=1451; -- Canalete / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.908879959971701', osm_rotulolon='-75.87461320624791' WHERE id=237; -- Cereté / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.098528590404605', osm_rotulolon='-75.6418882769334' WHERE id=244; -- Chimá / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.034725238074126', osm_rotulolon='-75.36472859979901' WHERE id=278; -- Chinú / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.820849593346106', osm_rotulolon='-75.63201710779381' WHERE id=284; -- Ciénaga de Oro / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.054487667878986', osm_rotulolon='-75.78123233667333' WHERE id=538; -- Cotorra / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.054043958354457', osm_rotulolon='-75.29953208675069' WHERE id=604; -- La Apartada / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.152297280899765', osm_rotulolon='-75.9087021272964' WHERE id=718; -- Lorica / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.855436578341973', osm_rotulolon='-76.28872999969204' WHERE id=724; -- Los Córdobas / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.26058216682676', osm_rotulolon='-75.64137768439032' WHERE id=781; -- Momil / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='7.764253355244976', osm_rotulolon='-75.78500461154742' WHERE id=784; -- Montelíbano / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.569870777177838', osm_rotulolon='-75.97365988301507' WHERE id=41; -- Montería / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.19243410150505', osm_rotulolon='-76.10842785326223' WHERE id=849; -- Moñitos / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.29391647090677', osm_rotulolon='-75.63374662747219' WHERE id=930; -- Planeta Rica / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.47216533815', osm_rotulolon='-75.43618417486543' WHERE id=944; -- Pueblo Nuevo / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.984977575068188', osm_rotulolon='-76.18001164807255' WHERE id=957; -- Puerto Escondido / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='7.719617465969305', osm_rotulolon='-75.78880201687109' WHERE id=963; -- Puerto Libertador / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.273608451564206', osm_rotulolon='-75.72078320466322' WHERE id=972; -- Purísima de La Concepción / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.803561323193362', osm_rotulolon='-75.44392961993795' WHERE id=1071; -- Sahagún / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.127908749365218', osm_rotulolon='-75.50499948238271' WHERE id=1093; -- San Andrés de Sotavento / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.360022772975633', osm_rotulolon='-75.78659707004891' WHERE id=1099; -- San Antero / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.321521637153053', osm_rotulolon='-75.96195229142013' WHERE id=1107; -- San Bernardo del Viento / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.705847639187137', osm_rotulolon='-75.70684880905306' WHERE id=1113; -- San Carlos / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='7.776518466077629', osm_rotulolon='-75.5874034046279' WHERE id=1122; -- San José de Uré / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.99794042811749', osm_rotulolon='-75.90659166159018' WHERE id=1132; -- San Pelayo / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='7.831324067206424', osm_rotulolon='-76.10336430539763' WHERE id=1313; -- Tierralta / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='9.224855990628377', osm_rotulolon='-75.55014087434868' WHERE id=1326; -- Tuchín / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='8.195952694651279', osm_rotulolon='-76.21206260412191' WHERE id=1368; -- Valencia / Córdoba
      UPDATE msip_municipio SET osm_rotulolat='4.373163395294082', osm_rotulolon='-74.67502659525846' WHERE id=6; -- Agua de Dios / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.896703387659174', osm_rotulolon='-74.45726272026457' WHERE id=288; -- Albán / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.558349923437882', osm_rotulolon='-74.53455020514585' WHERE id=603; -- Anapoima / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.790255884543242', osm_rotulolon='-74.46849033908363' WHERE id=689; -- Anolaima / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.518227023211687', osm_rotulolon='-74.57173573854683' WHERE id=983; -- Apulo / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.230625591764468', osm_rotulolon='-74.41138886582337' WHERE id=893; -- Arbeláez / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.727177015142765', osm_rotulolon='-74.76790708918934' WHERE id=1373; -- Beltrán / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.851281273645485', osm_rotulolon='-74.5306297776684' WHERE id=1458; -- Bituima / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.690596581643704', osm_rotulolon='-74.33792813151537' WHERE id=1464; -- Bojacá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='3.900191273915866', osm_rotulolon='-74.45868365691025' WHERE id=131; -- Cabrera / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.730734582894148', osm_rotulolon='-74.46288783971498' WHERE id=135; -- Cachipay / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.932041132437064', osm_rotulolon='-74.02940665645109' WHERE id=142; -- Cajicá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.369891482502564', osm_rotulolon='-74.52057989793913' WHERE id=198; -- Caparrapí / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.398062310043178', osm_rotulolon='-73.94470533643532' WHERE id=217; -- Cáqueza / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.341286810892365', osm_rotulolon='-73.93757858046783' WHERE id=221; -- Carmen de Carupa / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.960378727542545', osm_rotulolon='-74.63502932468245' WHERE id=242; -- Chaguaní / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.402356262931935', osm_rotulolon='-74.06562202611161' WHERE id=265; -- Chipaque / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.583144822148267', osm_rotulolon='-73.92268278756713' WHERE id=277; -- Choachí / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.116717973408662', osm_rotulolon='-73.668610339704' WHERE id=280; -- Chocontá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.109214323577599', osm_rotulolon='-73.97052617515033' WHERE id=334; -- Cogua / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.783636127304842', osm_rotulolon='-74.12409974417359' WHERE id=363; -- Cota / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.228085567241185', osm_rotulolon='-73.78137032908003' WHERE id=385; -- Cucunubá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.548418986527085', osm_rotulolon='-74.43514409624954' WHERE id=425; -- El Colegio / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.259102106142463', osm_rotulolon='-74.32104731062803' WHERE id=450; -- El Peñón / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.878618243444842', osm_rotulolon='-74.23997781099185' WHERE id=456; -- El Rosal / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.837289877683667', osm_rotulolon='-74.34424173400225' WHERE id=469; -- Facatativá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.538411601352162', osm_rotulolon='-73.80450735487595' WHERE id=482; -- Fómeque / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.311700125687114', osm_rotulolon='-73.9619001430821' WHERE id=485; -- Fosca / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.742888035376707', osm_rotulolon='-74.20194165406386' WHERE id=489; -- Funza / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.409321009144069', osm_rotulolon='-73.77800374266579' WHERE id=493; -- Fúquene / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.313333786062865', osm_rotulolon='-74.4079153756238' WHERE id=497; -- Fusagasugá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.662966239223683', osm_rotulolon='-73.50626385040063' WHERE id=498; -- Gachalá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.00773300652125', osm_rotulolon='-73.87949024240474' WHERE id=500; -- Gachancipá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.85901111092329', osm_rotulolon='-73.63432823268356' WHERE id=505; -- Gachetá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.722906782467176', osm_rotulolon='-73.60465834812874' WHERE id=508; -- Gama / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.335053888258353', osm_rotulolon='-74.8135396825976' WHERE id=547; -- Girardot / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.518867422023735', osm_rotulolon='-74.33957227662249' WHERE id=553; -- Granada / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.382193388920661', osm_rotulolon='-73.69975968687785' WHERE id=560; -- Guachetá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.19249276965002', osm_rotulolon='-74.62472117082118' WHERE id=573; -- Guaduas / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.794829185103249', osm_rotulolon='-73.86156414305813' WHERE id=578; -- Guasca / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.509117189206745', osm_rotulolon='-74.79148252395784' WHERE id=581; -- Guataquí / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.908549381729751', osm_rotulolon='-73.79769067986192' WHERE id=586; -- Guatavita / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.879957271482533', osm_rotulolon='-74.4848493121348' WHERE id=588; -- Guayabal de Síquima / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.227510123827775', osm_rotulolon='-73.84952855963412' WHERE id=591; -- Guayabetal / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.144264675260762', osm_rotulolon='-74.05312016405861' WHERE id=592; -- Gutiérrez / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.572165997729574', osm_rotulolon='-74.69701028577022' WHERE id=627; -- Jerusalén / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.704759626597309', osm_rotulolon='-73.71528162764044' WHERE id=632; -- Junín / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.691308669919788', osm_rotulolon='-73.94607168233973' WHERE id=636; -- La Calera / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.651182072943098', osm_rotulolon='-74.47966475816033' WHERE id=653; -- La Mesa / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.320418385820148', osm_rotulolon='-74.42439382694013' WHERE id=658; -- La Palma / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.196522453284194', osm_rotulolon='-74.41089001336721' WHERE id=662; -- La Peña / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.984558415615464', osm_rotulolon='-74.34548120897911' WHERE id=700; -- La Vega / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.311457546791123', osm_rotulolon='-73.68884880471737' WHERE id=708; -- Lenguazaque / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.042077570485366', osm_rotulolon='-73.61457091500326' WHERE id=734; -- Machetá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.783334286056809', osm_rotulolon='-74.26399185303012' WHERE id=739; -- Madrid / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.988488352074923', osm_rotulolon='-73.56565594932158' WHERE id=750; -- Manta / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.48717823031732', osm_rotulolon='-73.4140211481391' WHERE id=751; -- Medina / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.681786527535626', osm_rotulolon='-74.24390175285698' WHERE id=794; -- Mosquera / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.404845420732573', osm_rotulolon='-74.80397801983452' WHERE id=803; -- Nariño / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.096139315043152', osm_rotulolon='-73.88083343726467' WHERE id=808; -- Nemocón / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.283077765558123', osm_rotulolon='-74.61641937256324' WHERE id=809; -- Nilo / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.125759969521832', osm_rotulolon='-74.39379306932167' WHERE id=810; -- Nimaima / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.063649074166068', osm_rotulolon='-74.39278925673486' WHERE id=815; -- Nocaima / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.1724840226375', osm_rotulolon='-74.16206458017575' WHERE id=867; -- Pacho / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.396277112594409', osm_rotulolon='-74.16853952362096' WHERE id=875; -- Paime / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.172028454414235', osm_rotulolon='-74.47721025999265' WHERE id=891; -- Pandi / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.453814501518615', osm_rotulolon='-73.23428888227042' WHERE id=895; -- Paratebueno / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.29271008947347', osm_rotulolon='-74.27884988216863' WHERE id=902; -- Pasca / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.619798875032664', osm_rotulolon='-74.57057646866762' WHERE id=950; -- Puerto Salgar / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.691630563133506', osm_rotulolon='-74.68375179342652' WHERE id=964; -- Pulí / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.103272384880599', osm_rotulolon='-74.48854433396923' WHERE id=978; -- Quebradanegra / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.336289518627005', osm_rotulolon='-73.85832884223385' WHERE id=979; -- Quetame / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.707001017178965', osm_rotulolon='-74.55322894215666' WHERE id=982; -- Quipile / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.306308270386304', osm_rotulolon='-74.74450287977231' WHERE id=1022; -- Ricaurte / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.600062362339094', osm_rotulolon='-74.34643661331341' WHERE id=1050; -- San Antonio del Tequendama / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.150480595914578', osm_rotulolon='-74.36597714810557' WHERE id=1054; -- San Bernardo / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.317687236599545', osm_rotulolon='-74.08548966084163' WHERE id=1061; -- San Cayetano / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.950497019329665', osm_rotulolon='-74.2865135235193' WHERE id=1067; -- San Francisco / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.843144040575657', osm_rotulolon='-74.68119652477505' WHERE id=1078; -- San Juan de Rioseco / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.952257741142788', osm_rotulolon='-74.42018815445444' WHERE id=1185; -- Sasaima / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.008294397298161', osm_rotulolon='-73.7849167600628' WHERE id=1195; -- Sesquilé / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.465635582634754', osm_rotulolon='-74.26825283494148' WHERE id=1199; -- Sibaté / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.43428161662863', osm_rotulolon='-74.37327054925822' WHERE id=1202; -- Silvania / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.513566845335863', osm_rotulolon='-73.85103437267642' WHERE id=1206; -- Simijaca / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.535454932736148', osm_rotulolon='-74.24485054814136' WHERE id=1216; -- Soacha / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.889952231161256', osm_rotulolon='-73.97732436208132' WHERE id=1225; -- Sopó / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.966104216292793', osm_rotulolon='-74.16493044938417' WHERE id=1236; -- Subachoque / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.128477505483642', osm_rotulolon='-73.78103398110416' WHERE id=1245; -- Suesca / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.059097930034773', osm_rotulolon='-74.23558485860154' WHERE id=1251; -- Supatá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.445402642337597', osm_rotulolon='-73.82836632231187' WHERE id=1254; -- Susa / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.24125940969533', osm_rotulolon='-73.8595582239886' WHERE id=1261; -- Sutatausa / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.949912174639534', osm_rotulolon='-74.09140298255602' WHERE id=1264; -- Tabio / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.201381328881621', osm_rotulolon='-73.96457230064067' WHERE id=1278; -- Tausa / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.643044274391152', osm_rotulolon='-74.39884626780793' WHERE id=1280; -- Tena / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.829280522232076', osm_rotulolon='-74.15941219228387' WHERE id=1286; -- Tenjo / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.323226513239478', osm_rotulolon='-74.48947775841002' WHERE id=1310; -- Tibacuy / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.086363294854032', osm_rotulolon='-73.51144952648178' WHERE id=1312; -- Tibirita / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.456994580814704', osm_rotulolon='-74.63228342349339' WHERE id=1325; -- Tocaima / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.973796899956134', osm_rotulolon='-73.93842137445083' WHERE id=1328; -- Tocancipá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.364857857551901', osm_rotulolon='-74.30483001354045' WHERE id=1337; -- Topaipí / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.754122173709501', osm_rotulolon='-73.42841399385031' WHERE id=1353; -- Ubalá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.498245784884006', osm_rotulolon='-73.97882166105866' WHERE id=1354; -- Ubaque / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.304661340662214', osm_rotulolon='-74.0765120456226' WHERE id=1359; -- Une / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.176084903213497', osm_rotulolon='-74.48963036469992' WHERE id=1364; -- Útica / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.059194551436462', osm_rotulolon='-74.461158482813' WHERE id=857; -- Venecia / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.123460426079792', osm_rotulolon='-74.29907452742546' WHERE id=1379; -- Vergara / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.88079746602391', osm_rotulolon='-74.55118532261098' WHERE id=1383; -- Vianí / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.316590623815144', osm_rotulolon='-73.82837930873316' WHERE id=1357; -- Villa de San Diego de Ubaté / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.268699639183178', osm_rotulolon='-74.19572340900103' WHERE id=1389; -- Villagómez / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.230977096704188', osm_rotulolon='-73.58220432452178' WHERE id=1395; -- Villapinzón / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.020202630938069', osm_rotulolon='-74.48827023451044' WHERE id=1399; -- Villeta / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.436437907119585', osm_rotulolon='-74.4920480146502' WHERE id=1401; -- Viotá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.574764594503071', osm_rotulolon='-74.39063086924317' WHERE id=1410; -- Yacopí / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='4.749755274451422', osm_rotulolon='-74.40215760324755' WHERE id=1429; -- Zipacón / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='5.058005582592156', osm_rotulolon='-74.0076211661728' WHERE id=1430; -- Zipaquirá / Cundinamarca
      UPDATE msip_municipio SET osm_rotulolat='3.074439924211784', osm_rotulolon='-69.90319274593' WHERE id=594; -- Barrancominas / Guainía
      UPDATE msip_municipio SET osm_rotulolat='3.426839883402417', osm_rotulolon='-67.62938395671014' WHERE id=1414; -- Cacahual / Guainía
      UPDATE msip_municipio SET osm_rotulolat='3.299219412751733', osm_rotulolon='-68.61949993362722' WHERE id=35; -- Inírida / Guainía
      UPDATE msip_municipio SET osm_rotulolat='1.41231247500153', osm_rotulolon='-67.01142780103027' WHERE id=1408; -- La Guadalupe / Guainía
      UPDATE msip_municipio SET osm_rotulolat='2.388137351580976', osm_rotulolon='-70.01217815631439' WHERE id=1417; -- Morichal / Guainía
      UPDATE msip_municipio SET osm_rotulolat='2.012370582555035', osm_rotulolon='-69.26873920521969' WHERE id=1415; -- Pana Pana / Guainía
      UPDATE msip_municipio SET osm_rotulolat='2.505021329224987', osm_rotulolon='-68.36689850254113' WHERE id=1407; -- Puerto Colombia / Guainía
      UPDATE msip_municipio SET osm_rotulolat='2.101485299168386', osm_rotulolon='-67.38331995043045' WHERE id=1406; -- San Felipe / Guainía
      UPDATE msip_municipio SET osm_rotulolat='1.581968958081652', osm_rotulolon='-72.93658151419426' WHERE id=201; -- Calamar / Guaviare
      UPDATE msip_municipio SET osm_rotulolat='2.145975146139202', osm_rotulolon='-71.3685045521758' WHERE id=433; -- El Retorno / Guaviare
      UPDATE msip_municipio SET osm_rotulolat='1.342945721066288', osm_rotulolon='-71.99558306209703' WHERE id=335; -- Miraflores / Guaviare
      UPDATE msip_municipio SET osm_rotulolat='2.365578958839214', osm_rotulolon='-72.09003087127495' WHERE id=51; -- San José del Guaviare / Guaviare
      UPDATE msip_municipio SET osm_rotulolat='1.705868061400974', osm_rotulolon='-76.01463274548888' WHERE id=988; -- Acevedo / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.274103777108802', osm_rotulolon='-75.72753450902104' WHERE id=146; -- Agrado / Huila
      UPDATE msip_municipio SET osm_rotulolat='3.241183265448254', osm_rotulolon='-75.3653454414851' WHERE id=223; -- Aipe / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.50089295396867', osm_rotulolon='-75.29854299945214' WHERE id=326; -- Algeciras / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.082736531768352', osm_rotulolon='-75.77472912324139' WHERE id=453; -- Altamira / Huila
      UPDATE msip_municipio SET osm_rotulolat='3.136305586497329', osm_rotulolon='-74.96495852949072' WHERE id=1256; -- Baraya / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.660297069977094', osm_rotulolon='-75.33536083061774' WHERE id=169; -- Campoalegre / Huila
      UPDATE msip_municipio SET osm_rotulolat='3.439910521143293', osm_rotulolon='-74.66218236928638' WHERE id=342; -- Colombia / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.00512916948267', osm_rotulolon='-75.95547367701735' WHERE id=420; -- Elías / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.173617385231395', osm_rotulolon='-75.5868318472961' WHERE id=506; -- Garzón / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.377064120123052', osm_rotulolon='-75.54050932596516' WHERE id=544; -- Gigante / Huila
      UPDATE msip_municipio SET osm_rotulolat='1.986409867333556', osm_rotulolon='-75.70416426538618' WHERE id=569; -- Guadalupe / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.546966275447371', osm_rotulolon='-75.45906724687767' WHERE id=600; -- Hobo / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.686635106307099', osm_rotulolon='-75.7033281470055' WHERE id=612; -- Íquira / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.036491342996036', osm_rotulolon='-76.27610413531875' WHERE id=613; -- Isnos / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.212380077133362', osm_rotulolon='-76.13582828748449' WHERE id=641; -- La Argentina / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.312571687443714', osm_rotulolon='-76.02681056081857' WHERE id=659; -- La Plata / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.578002852561086', osm_rotulolon='-75.79150297489413' WHERE id=806; -- Nátaga / Huila
      UPDATE msip_municipio SET osm_rotulolat='3.023453995831622', osm_rotulolon='-75.28196012628318' WHERE id=42; -- Neiva / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.077396592544955', osm_rotulolon='-76.0529091148476' WHERE id=854; -- Oporapa / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.421279223713054', osm_rotulolon='-75.72191287968565' WHERE id=874; -- Paicol / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.907373405466842', osm_rotulolon='-75.48010298867614' WHERE id=888; -- Palermo / Huila
      UPDATE msip_municipio SET osm_rotulolat='1.684464493995484', osm_rotulolon='-76.15174675975864' WHERE id=894; -- Palestina / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.244853644589059', osm_rotulolon='-75.83677526269055' WHERE id=918; -- Pital / Huila
      UPDATE msip_municipio SET osm_rotulolat='1.811197016458458', osm_rotulolon='-76.15194287572223' WHERE id=926; -- Pitalito / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.77432412622021', osm_rotulolon='-75.20796026834263' WHERE id=1029; -- Rivera / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.101170376092541', osm_rotulolon='-76.21978313592864' WHERE id=1072; -- Saladoblanco / Huila
      UPDATE msip_municipio SET osm_rotulolat='1.915960964451482', osm_rotulolon='-76.43856801211909' WHERE id=1088; -- San Agustín / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.928996724331914', osm_rotulolon='-75.6467307182794' WHERE id=1110; -- Santa María / Huila
      UPDATE msip_municipio SET osm_rotulolat='1.868826858583915', osm_rotulolon='-75.8243539100812' WHERE id=1242; -- Suaza / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.12862053138638', osm_rotulolon='-75.88465242212959' WHERE id=1276; -- Tarqui / Huila
      UPDATE msip_municipio SET osm_rotulolat='3.039055228898109', osm_rotulolon='-75.08529749252466' WHERE id=1285; -- Tello / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.843738948145226', osm_rotulolon='-75.7683659107323' WHERE id=1308; -- Teruel / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.519872864384662', osm_rotulolon='-75.69549276543314' WHERE id=1281; -- Tesalia / Huila
      UPDATE msip_municipio SET osm_rotulolat='1.954764186954167', osm_rotulolon='-75.9255965868922' WHERE id=1314; -- Timaná / Huila
      UPDATE msip_municipio SET osm_rotulolat='3.275154426793714', osm_rotulolon='-75.14609971161234' WHERE id=1391; -- Villavieja / Huila
      UPDATE msip_municipio SET osm_rotulolat='2.650997511405915', osm_rotulolon='-75.52024922210427' WHERE id=1412; -- Yaguará / Huila
      UPDATE msip_municipio SET osm_rotulolat='11.225873752182252', osm_rotulolon='-72.5585022573993' WHERE id=602; -- Albania / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='11.006577337586556', osm_rotulolon='-72.71448234425905' WHERE id=1257; -- Barrancas / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='11.123570262091597', osm_rotulolon='-73.42360727689571' WHERE id=1452; -- Dibulla / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='10.917124285178415', osm_rotulolon='-72.95279332264138' WHERE id=1461; -- Distracción / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='10.636584789943479', osm_rotulolon='-72.89423386783326' WHERE id=110; -- El Molino / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='10.834754645078116', osm_rotulolon='-72.80255552166712' WHERE id=481; -- Fonseca / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='11.102342402140334', osm_rotulolon='-72.69707002108433' WHERE id=640; -- Hatonuevo / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='10.451888669165923', osm_rotulolon='-73.0670901845124' WHERE id=728; -- La Jagua del Pilar / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='11.348094652349154', osm_rotulolon='-72.34251871358128' WHERE id=741; -- Maicao / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='11.59726532655219', osm_rotulolon='-72.5563257126507' WHERE id=933; -- Manaure / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='11.5541299', osm_rotulolon='-72.9038437' WHERE id=49; -- Riohacha / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='10.816456205559872', osm_rotulolon='-73.0662053564666' WHERE id=1058; -- San Juan del Cesar / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='12.015824804372285', osm_rotulolon='-71.79160337127563' WHERE id=1361; -- Uribia / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='10.491869926765656', osm_rotulolon='-73.02712545185875' WHERE id=1367; -- Urumita / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='10.575639413285352', osm_rotulolon='-72.9841529683063' WHERE id=1398; -- Villanueva / La Guajira
      UPDATE msip_municipio SET osm_rotulolat='10.232519822882443', osm_rotulolon='-74.09449409948296' WHERE id=534; -- Algarrobo / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.621349429716801', osm_rotulolon='-73.88704637984401' WHERE id=892; -- Aracataca / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.82065012303862', osm_rotulolon='-74.10148566786376' WHERE id=962; -- Ariguaní / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.281184820315387', osm_rotulolon='-74.81836783445722' WHERE id=236; -- Cerro de San Antonio / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.098242420076197', osm_rotulolon='-74.53322249479021' WHERE id=254; -- Chivolo / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.869176567098277', osm_rotulolon='-74.04931692033104' WHERE id=285; -- Ciénaga / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.229236922123473', osm_rotulolon='-74.78234638474541' WHERE id=339; -- Concordia / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.119661213937405', osm_rotulolon='-73.95625105249974' WHERE id=421; -- El Banco / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.335662501261043', osm_rotulolon='-74.69052389434907' WHERE id=451; -- El Piñón / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.656191601032216', osm_rotulolon='-74.3103021387086' WHERE id=467; -- El Retén / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.453805618894469', osm_rotulolon='-73.90928272617026' WHERE id=492; -- Fundación / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.253157833963064', osm_rotulolon='-74.15782648404156' WHERE id=564; -- Guamal / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.746790300762889', osm_rotulolon='-74.36099742100386' WHERE id=778; -- Nueva Granada / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.165298399763147', osm_rotulolon='-74.8088471321883' WHERE id=908; -- Pedraza / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.505478661214502', osm_rotulolon='-74.18595908959267' WHERE id=913; -- Pijiño del Carmen / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.420520631814213', osm_rotulolon='-74.41523774830556' WHERE id=927; -- Pivijay / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.775833342950376', osm_rotulolon='-74.63938343658664' WHERE id=931; -- Plato / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.8384707091787', osm_rotulolon='-74.34429841546661' WHERE id=945; -- Puebloviejo / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.645109682402474', osm_rotulolon='-74.57188168888531' WHERE id=1016; -- Remolino / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.123892338125902', osm_rotulolon='-74.24846751138568' WHERE id=1070; -- Sabanas de San Ángel / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.519983766270247', osm_rotulolon='-74.73062630814897' WHERE id=1105; -- Salamina / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.34177903625002', osm_rotulolon='-74.21751234709986' WHERE id=1144; -- San Sebastián de Buenavista / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.327774937161045', osm_rotulolon='-74.37548472194439' WHERE id=1177; -- San Zenón / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.54279884171643', osm_rotulolon='-74.29233106483065' WHERE id=1179; -- Santa Ana / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.517520611734511', osm_rotulolon='-74.67392947214992' WHERE id=1186; -- Santa Bárbara de Pinto / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='11.121196100326207', osm_rotulolon='-73.9232699800035' WHERE id=52; -- Santa Marta / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.892375050068855', osm_rotulolon='-74.60964575118204' WHERE id=1208; -- Sitionuevo / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='9.927206961951203', osm_rotulolon='-74.73161248579508' WHERE id=1283; -- Tenerife / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.123123515377424', osm_rotulolon='-74.6772968518888' WHERE id=1459; -- Zapayán / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='10.8016110607656', osm_rotulolon='-74.16357350485039' WHERE id=1462; -- Zona Bananera / Magdalena
      UPDATE msip_municipio SET osm_rotulolat='4.016352016850942', osm_rotulolon='-73.7597097732173' WHERE id=986; -- Acacías / Meta
      UPDATE msip_municipio SET osm_rotulolat='4.484116212674903', osm_rotulolon='-72.9971502568549' WHERE id=106; -- Barranca de Upía / Meta
      UPDATE msip_municipio SET osm_rotulolat='4.314172158981036', osm_rotulolon='-72.9636415680893' WHERE id=136; -- Cabuyaro / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.814509349286948', osm_rotulolon='-73.54595955189419' WHERE id=216; -- Castilla La Nueva / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.799429917498668', osm_rotulolon='-74.117334192168' WHERE id=380; -- Cubarral / Meta
      UPDATE msip_municipio SET osm_rotulolat='4.256882685930734', osm_rotulolon='-73.31304010317466' WHERE id=387; -- Cumaral / Meta
      UPDATE msip_municipio SET osm_rotulolat='4.360859210645615', osm_rotulolon='-73.71779859817231' WHERE id=422; -- El Calvario / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.630716474433635', osm_rotulolon='-73.9007852660247' WHERE id=444; -- El Castillo / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.706660592501333', osm_rotulolon='-73.83760794645488' WHERE id=471; -- El Dorado / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.366943800517186', osm_rotulolon='-73.6114638682681' WHERE id=490; -- Fuente de Oro / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.508297486840995', osm_rotulolon='-73.7689996718633' WHERE id=556; -- Granada / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.951739762157616', osm_rotulolon='-73.97261664502265' WHERE id=565; -- Guamal / Meta
      UPDATE msip_municipio SET osm_rotulolat='2.252921512551488', osm_rotulolon='-74.08345981896433' WHERE id=605; -- La Macarena / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.599128310715083', osm_rotulolon='-74.10129984653534' WHERE id=696; -- Lejanías / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.060137229859465', osm_rotulolon='-72.0314839786092' WHERE id=584; -- Mapiripán / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.070128635962911', osm_rotulolon='-74.13328731398269' WHERE id=589; -- Mesetas / Meta
      UPDATE msip_municipio SET osm_rotulolat='2.705251337536199', osm_rotulolon='-72.75434261427792' WHERE id=771; -- Puerto Concordia / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.886197616908324', osm_rotulolon='-71.68770323847161' WHERE id=941; -- Puerto Gaitán / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.211358545079306', osm_rotulolon='-73.26570258633718' WHERE id=960; -- Puerto Lleras / Meta
      UPDATE msip_municipio SET osm_rotulolat='4.056302492036475', osm_rotulolon='-72.70193566036352' WHERE id=954; -- Puerto López / Meta
      UPDATE msip_municipio SET osm_rotulolat='2.741971696857272', osm_rotulolon='-73.2009071501973' WHERE id=974; -- Puerto Rico / Meta
      UPDATE msip_municipio SET osm_rotulolat='4.251931351908412', osm_rotulolon='-73.43599045947289' WHERE id=1018; -- Restrepo / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.840686874447912', osm_rotulolon='-73.29233129331134' WHERE id=1118; -- San Carlos de Guaroa / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.248548365254277', osm_rotulolon='-73.81429936813386' WHERE id=1124; -- San Juan de Arama / Meta
      UPDATE msip_municipio SET osm_rotulolat='4.466008327106119', osm_rotulolon='-73.66600920184426' WHERE id=1130; -- San Juanito / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.584178181367302', osm_rotulolon='-73.06321357260184' WHERE id=1139; -- San Martín / Meta
      UPDATE msip_municipio SET osm_rotulolat='3.00043666131441', osm_rotulolon='-74.4339442211707' WHERE id=630; -- Uribe / Meta
      UPDATE msip_municipio SET osm_rotulolat='4.083227896237621', osm_rotulolon='-73.49960504502275' WHERE id=57; -- Villavicencio / Meta
      UPDATE msip_municipio SET osm_rotulolat='2.706938109834961', osm_rotulolon='-73.63260580564064' WHERE id=1182; -- Vistahermosa / Meta
      UPDATE msip_municipio SET osm_rotulolat='1.472539622364139', osm_rotulolon='-77.0749007715204' WHERE id=287; -- Albán / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.907560983753006', osm_rotulolon='-77.69829296473448' WHERE id=371; -- Aldana / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.2449796719729', osm_rotulolon='-77.53132113961233' WHERE id=614; -- Ancuya / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.477168008626694', osm_rotulolon='-77.13917749284586' WHERE id=861; -- Arboleda / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.44788598106444', osm_rotulolon='-78.11272115911517' WHERE id=1270; -- Barbacoas / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.59429407507544', osm_rotulolon='-77.04924493680069' WHERE id=1341; -- Belén / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.336191393638174', osm_rotulolon='-77.14965104565422' WHERE id=109; -- Buesaco / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.404412077992221', osm_rotulolon='-77.2836830997223' WHERE id=415; -- Chachagüí / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.633022948405434', osm_rotulolon='-77.05412429841209' WHERE id=336; -- Colón / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.208412178060911', osm_rotulolon='-77.44004837840865' WHERE id=346; -- Consacá / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.939081031432161', osm_rotulolon='-77.53039626885354' WHERE id=356; -- Contadero / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.779839604628524', osm_rotulolon='-77.38555329055389' WHERE id=366; -- Córdoba / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.871604088469554', osm_rotulolon='-77.73895181690574' WHERE id=383; -- Cuaspud Carlosama / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.938829198112982', osm_rotulolon='-77.97577821853464' WHERE id=390; -- Cumbal / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.730306303408612', osm_rotulolon='-77.58992605267981' WHERE id=400; -- Cumbitara / Nariño
      UPDATE msip_municipio SET osm_rotulolat='2.173400139215037', osm_rotulolon='-77.8273802738856' WHERE id=437; -- El Charco / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.516000713852616', osm_rotulolon='-77.43708024998982' WHERE id=445; -- El Peñol / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.841649103211505', osm_rotulolon='-77.4617676165608' WHERE id=448; -- El Rosario / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.401048990263705', osm_rotulolon='-76.99748611174329' WHERE id=452; -- El Tablón de Gómez / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.433696511207101', osm_rotulolon='-77.4001885764174' WHERE id=457; -- El Tambo / Nariño
      UPDATE msip_municipio SET osm_rotulolat='2.058851686195562', osm_rotulolon='-78.59302468898176' WHERE id=879; -- Francisco Pizarro / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.89298790268793', osm_rotulolon='-77.3263063824317' WHERE id=491; -- Funes / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.977490171052028', osm_rotulolon='-77.7393796134534' WHERE id=561; -- Guachucal / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.154578984175895', osm_rotulolon='-77.52640343171204' WHERE id=574; -- Guaitarilla / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.93010898340217', osm_rotulolon='-77.58606126942394' WHERE id=580; -- Gualmatán / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.983288493829158', osm_rotulolon='-77.52812220991117' WHERE id=607; -- Iles / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.073584047540849', osm_rotulolon='-77.51147722044864' WHERE id=609; -- Imués / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.59501646800458', osm_rotulolon='-77.44508590204326' WHERE id=611; -- Ipiales / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.589187668931046', osm_rotulolon='-76.93047779273185' WHERE id=642; -- La Cruz / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.346720285275969', osm_rotulolon='-77.39147413996973' WHERE id=647; -- La Florida / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.574129326533333', osm_rotulolon='-77.7069343257844' WHERE id=651; -- La Llanada / Nariño
      UPDATE msip_municipio SET osm_rotulolat='2.388998850657301', osm_rotulolon='-78.21564863647626' WHERE id=656; -- La Tola / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.621201405176392', osm_rotulolon='-77.15462018544368' WHERE id=664; -- La Unión / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.955047119567454', osm_rotulolon='-77.33211100355032' WHERE id=704; -- Leiva / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.396327093271128', osm_rotulolon='-77.52435211737344' WHERE id=715; -- Linares / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.668997377509577', osm_rotulolon='-77.69986843746274' WHERE id=719; -- Los Andes / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.914445801023081', osm_rotulolon='-78.04067521572291' WHERE id=735; -- Magüí / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.180243966776316', osm_rotulolon='-77.83810950470219' WHERE id=748; -- Mallama / Nariño
      UPDATE msip_municipio SET osm_rotulolat='2.441416206055981', osm_rotulolon='-78.41591387897857' WHERE id=795; -- Mosquera / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.301672721682058', osm_rotulolon='-77.3516641370922' WHERE id=802; -- Nariño / Nariño
      UPDATE msip_municipio SET osm_rotulolat='2.282590890226226', osm_rotulolon='-78.30092093294068' WHERE id=813; -- Olaya Herrera / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.02776041517657', osm_rotulolon='-77.5547641355612' WHERE id=856; -- Ospina / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.0880716057993', osm_rotulolon='-77.18530225068886' WHERE id=44; -- Pasto / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.738650926789672', osm_rotulolon='-77.49696770837909' WHERE id=906; -- Policarpa / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.716753500120173', osm_rotulolon='-77.46111914039035' WHERE id=935; -- Potosí / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.232659967503893', osm_rotulolon='-77.59393541674456' WHERE id=939; -- Providencia / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.824073303055408', osm_rotulolon='-77.35177428143612' WHERE id=951; -- Puerres / Nariño
      UPDATE msip_municipio SET osm_rotulolat='0.915771539117891', osm_rotulolon='-77.63697474985' WHERE id=969; -- Pupiales / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.215077540587191', osm_rotulolon='-78.02868335248372' WHERE id=1023; -- Ricaurte / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.860895544128338', osm_rotulolon='-78.38367677971958' WHERE id=1036; -- Roberto Payán / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.448433437797072', osm_rotulolon='-77.68730238331327' WHERE id=1111; -- Samaniego / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.698125268797211', osm_rotulolon='-78.6349048973559' WHERE id=1345; -- San Andrés de Tumaco / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.525564858468214', osm_rotulolon='-77.01996268395135' WHERE id=1128; -- San Bernardo / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.551781638745752', osm_rotulolon='-77.23438236271588' WHERE id=1136; -- San Lorenzo / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.676888117088371', osm_rotulolon='-76.98617866131792' WHERE id=1145; -- San Pablo / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.536518507867331', osm_rotulolon='-77.10691066404802' WHERE id=1148; -- San Pedro de Cartago / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.287213664363401', osm_rotulolon='-77.46008549501376' WHERE id=1125; -- Sandoná / Nariño
      UPDATE msip_municipio SET osm_rotulolat='2.303384070023258', osm_rotulolon='-77.84532492308132' WHERE id=1149; -- Santa Bárbara / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.288621025687433', osm_rotulolon='-77.74155109447864' WHERE id=1153; -- Santacruz / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.035395073766794', osm_rotulolon='-77.68176295321469' WHERE id=1188; -- Sapuyes / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.594227513969634', osm_rotulolon='-77.32840777417144' WHERE id=1265; -- Taminango / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.080167321207346', osm_rotulolon='-77.35215205992834' WHERE id=1268; -- Tangua / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.146782874106706', osm_rotulolon='-77.6198554411325' WHERE id=1351; -- Túquerres / Nariño
      UPDATE msip_municipio SET osm_rotulolat='1.130288964584127', osm_rotulolon='-77.42297713092067' WHERE id=1411; -- Yacuanquer / Nariño
      UPDATE msip_municipio SET osm_rotulolat='8.013906421218362', osm_rotulolon='-73.18257596383502' WHERE id=533; -- Ábrego / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.6707185', osm_rotulolon='-72.7237135' WHERE id=862; -- Arboledas / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.648190566366747', osm_rotulolon='-72.66866132402306' WHERE id=1463; -- Bochalema / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.086815589676545', osm_rotulolon='-72.94175548219313' WHERE id=85; -- Bucarasica / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.668784130377584', osm_rotulolon='-73.18174367230333' WHERE id=144; -- Cáchira / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.267557187023931', osm_rotulolon='-72.6635143111371' WHERE id=140; -- Cácota / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.577102218197815', osm_rotulolon='-72.59056431171072' WHERE id=258; -- Chinácota / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.042981768016706', osm_rotulolon='-72.55814789875582' WHERE id=260; -- Chitagá / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.822565561179385', osm_rotulolon='-73.20321670197694' WHERE id=344; -- Convención / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.501040620386822', osm_rotulolon='-72.80977258384165' WHERE id=382; -- Cucutilla / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.739668304878464', osm_rotulolon='-72.67848443065284' WHERE id=411; -- Durania / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.86285511198355', osm_rotulolon='-73.32460013353241' WHERE id=424; -- El Carmen / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.685779263436126', osm_rotulolon='-73.04542884980539' WHERE id=442; -- El Tarra / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.086733372631981', osm_rotulolon='-72.64918431804217' WHERE id=458; -- El Zulia / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.920184685815036', osm_rotulolon='-72.82120869393502' WHERE id=554; -- Gramalote / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.368284710182436', osm_rotulolon='-73.08437600647231' WHERE id=595; -- Hacarí / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.493749582570228', osm_rotulolon='-72.49698125148032' WHERE id=598; -- Herrán / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.725934281030148', osm_rotulolon='-73.40904178608513' WHERE id=650; -- La Esperanza / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.270795590922985', osm_rotulolon='-73.19247989964525' WHERE id=663; -- La Playa / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.253918201754477', osm_rotulolon='-72.54159787210449' WHERE id=638; -- Labateca / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.749268180569512', osm_rotulolon='-72.5330173356615' WHERE id=705; -- Los Patios / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.963455713159202', osm_rotulolon='-72.85777055883564' WHERE id=722; -- Lourdes / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.31750442363791', osm_rotulolon='-72.77215949262684' WHERE id=800; -- Mutiscua / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.27493924425512', osm_rotulolon='-73.39608157388164' WHERE id=823; -- Ocaña / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.363959631488288', osm_rotulolon='-72.68897068106551' WHERE id=877; -- Pamplona / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.477430189272361', osm_rotulolon='-72.64009588219275' WHERE id=883; -- Pamplonita / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.329265089996095', osm_rotulolon='-72.41788026634372' WHERE id=928; -- Puerto Santander / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.600145572640001', osm_rotulolon='-72.51239576757783' WHERE id=984; -- Ragonvalia / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.76933046345049', osm_rotulolon='-72.87338465730096' WHERE id=1073; -- Salazar / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.437984803272041', osm_rotulolon='-73.15706271762333' WHERE id=1094; -- San Calixto / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.847003006199976', osm_rotulolon='-72.61553144827917' WHERE id=1101; -- San Cayetano / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.111220005210017', osm_rotulolon='-72.52099989817259' WHERE id=32; -- San José de Cúcuta / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.892879567896422', osm_rotulolon='-72.72322072057757' WHERE id=1119; -- Santiago / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.265387677549944', osm_rotulolon='-72.81993699743812' WHERE id=1189; -- Sardinata / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.18825397642749', osm_rotulolon='-72.80113143011371' WHERE id=1201; -- Silos / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.766454722840244', osm_rotulolon='-73.12257588203391' WHERE id=1306; -- Teorama / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='8.685311384427276', osm_rotulolon='-72.75905786525198' WHERE id=1320; -- Tibú / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.25508073934716', osm_rotulolon='-72.36010980596738' WHERE id=1330; -- Toledo / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.942520845348293', osm_rotulolon='-73.0019918333625' WHERE id=1388; -- Villa Caro / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='7.762641590769222', osm_rotulolon='-72.48877574137484' WHERE id=1397; -- Villa del Rosario / Norte de Santander
      UPDATE msip_municipio SET osm_rotulolat='1.227623237637176', osm_rotulolon='-77.00491173671126' WHERE id=370; -- Colón / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='1.130838519342676', osm_rotulolon='-76.62312176420042' WHERE id=40; -- Mocoa / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='0.661683414887244', osm_rotulolon='-76.97233276725146' WHERE id=575; -- Orito / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='0.43899018466085', osm_rotulolon='-76.32443472732226' WHERE id=940; -- Puerto Asís / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='0.724715039654403', osm_rotulolon='-76.48535861824651' WHERE id=942; -- Puerto Caicedo / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='0.789077835389112', osm_rotulolon='-75.93006118792064' WHERE id=946; -- Puerto Guzmán / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='0.047938893706906', osm_rotulolon='-75.13246675326177' WHERE id=953; -- Puerto Leguízamo / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='1.138264318180012', osm_rotulolon='-76.82746001242968' WHERE id=1217; -- San Francisco / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='0.296121463844916', osm_rotulolon='-76.867248846429' WHERE id=1222; -- San Miguel / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='1.091322171979106', osm_rotulolon='-76.9906198947137' WHERE id=1228; -- Santiago / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='1.224001120397893', osm_rotulolon='-76.90748615417674' WHERE id=1209; -- Sibundoy / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='0.403382553993451', osm_rotulolon='-76.91257158470283' WHERE id=1381; -- Valle del Guamuez / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='0.928709045462196', osm_rotulolon='-76.78458775793032' WHERE id=1409; -- Villagarzón / Putumayo
      UPDATE msip_municipio SET osm_rotulolat='4.502947053415908', osm_rotulolon='-75.73273504155156' WHERE id=19; -- Armenia / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.366996650302606', osm_rotulolon='-75.74935109833469' WHERE id=111; -- Buenavista / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.450978649825236', osm_rotulolon='-75.68875728985874' WHERE id=165; -- Calarcá / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.593550919672207', osm_rotulolon='-75.68321269241994' WHERE id=297; -- Circasia / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.397264308542982', osm_rotulolon='-75.65355569204587' WHERE id=362; -- Córdoba / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.662159434767361', osm_rotulolon='-75.68186598601663' WHERE id=475; -- Filandia / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.185091720553517', osm_rotulolon='-75.77078639473734' WHERE id=543; -- Génova / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.437657815665919', osm_rotulolon='-75.82483624858124' WHERE id=698; -- La Tebaida / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.521114642185368', osm_rotulolon='-75.81828780295682' WHERE id=790; -- Montenegro / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.306045475236485', osm_rotulolon='-75.69851797034124' WHERE id=917; -- Pijao / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.601850031393678', osm_rotulolon='-75.79621593304171' WHERE id=980; -- Quimbaya / Quindío
      UPDATE msip_municipio SET osm_rotulolat='4.612130463626753', osm_rotulolon='-75.53766518260737' WHERE id=1141; -- Salento / Quindío
      UPDATE msip_municipio SET osm_rotulolat='5.1390560730284', osm_rotulolon='-75.9495365065239' WHERE id=767; -- Apía / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='4.920820064196289', osm_rotulolon='-75.94846083447601' WHERE id=1212; -- Balboa / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='5.193668338237548', osm_rotulolon='-75.86175965449162' WHERE id=1405; -- Belén de Umbría / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='4.838592419689523', osm_rotulolon='-75.67239866224939' WHERE id=255; -- Dosquebradas / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='5.323204730584722', osm_rotulolon='-75.80763341394662' WHERE id=568; -- Guática / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='4.991932083117328', osm_rotulolon='-76.01597740146357' WHERE id=648; -- La Celia / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='4.90579499484868', osm_rotulolon='-75.8558333363783' WHERE id=695; -- La Virginia / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='4.943597686325044', osm_rotulolon='-75.76168292937164' WHERE id=755; -- Marsella / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='5.405633953442567', osm_rotulolon='-75.96706110680883' WHERE id=774; -- Mistrató / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='4.769894592649417', osm_rotulolon='-75.68990325444621' WHERE id=45; -- Pereira / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='5.302034022676623', osm_rotulolon='-76.06735683419836' WHERE id=947; -- Pueblo Rico / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='5.322395625504433', osm_rotulolon='-75.71522446282185' WHERE id=981; -- Quinchía / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='4.849285238009124', osm_rotulolon='-75.57667880632462' WHERE id=1123; -- Santa Rosa de Cabal / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='5.036561900610021', osm_rotulolon='-75.95971310572355' WHERE id=1137; -- Santuario / Risaralda
      UPDATE msip_municipio SET osm_rotulolat='6.184891497310679', osm_rotulolon='-73.53711754126263' WHERE id=147; -- Aguada / Santander
      UPDATE msip_municipio SET osm_rotulolat='5.774188286635874', osm_rotulolon='-73.8540947770694' WHERE id=324; -- Albania / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.719917454701257', osm_rotulolon='-73.02711336247724' WHERE id=860; -- Aratoca / Santander
      UPDATE msip_municipio SET osm_rotulolat='5.954448484902966', osm_rotulolon='-73.62490701478406' WHERE id=1238; -- Barbosa / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.644354078901383', osm_rotulolon='-73.21944698280863' WHERE id=1272; -- Barichara / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.986279118386817', osm_rotulolon='-73.8729623788263' WHERE id=1319; -- Barrancabermeja / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.019576095449376', osm_rotulolon='-73.40305232272559' WHERE id=1454; -- Betulia / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.138787631229794', osm_rotulolon='-74.16877434525526' WHERE id=80; -- Bolívar / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.149351365683688', osm_rotulolon='-73.12092004767126' WHERE id=27; -- Bucaramanga / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.563990050129281', osm_rotulolon='-73.251990627499' WHERE id=133; -- Cabrera / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.356892773588829', osm_rotulolon='-72.93121902270957' WHERE id=168; -- California / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.519724141200999', osm_rotulolon='-72.68553179087776' WHERE id=195; -- Capitanejo / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.656671696611839', osm_rotulolon='-72.582943516008' WHERE id=219; -- Carcasí / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.758431413074105', osm_rotulolon='-72.93559102580501' WHERE id=233; -- Cepitá / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.89381417233405', osm_rotulolon='-72.65973707347007' WHERE id=239; -- Cerrito / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.210104641626581', osm_rotulolon='-73.1556270031183' WHERE id=241; -- Charalá / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.246067358159162', osm_rotulolon='-72.99582009967763' WHERE id=245; -- Charta / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.377248307828584', osm_rotulolon='-73.41835374329611' WHERE id=263; -- Chima / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.06147394470743', osm_rotulolon='-73.63708578614421' WHERE id=267; -- Chipatá / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.397737779908184', osm_rotulolon='-74.20915549594535' WHERE id=296; -- Cimitarra / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.809423770546541', osm_rotulolon='-72.62212353583968' WHERE id=345; -- Concepción / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.360376442686948', osm_rotulolon='-73.24688188719949' WHERE id=348; -- Confines / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.304330743209358', osm_rotulolon='-73.51430189512105' WHERE id=357; -- Contratación / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.231399506264244', osm_rotulolon='-72.99972771934995' WHERE id=367; -- Coromoro / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.631952368739101', osm_rotulolon='-73.01670021704015' WHERE id=392; -- Curití / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.675376679092661', osm_rotulolon='-73.58534307702836' WHERE id=404; -- El Carmen de Chucurí / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.252474558086278', osm_rotulolon='-73.55007654824031' WHERE id=426; -- El Guacamayo / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.095742087146856', osm_rotulolon='-73.93188615042409' WHERE id=441; -- El Peñón / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.516709960728131', osm_rotulolon='-73.1979417717318' WHERE id=446; -- El Playón / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.077428075010773', osm_rotulolon='-73.07040467665189' WHERE id=461; -- Encino / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.651848160720344', osm_rotulolon='-72.68873346384608' WHERE id=464; -- Enciso / Santander
      UPDATE msip_municipio SET osm_rotulolat='5.805737044745088', osm_rotulolon='-73.9594399890567' WHERE id=473; -- Florián / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.080493416779692', osm_rotulolon='-73.07596680336353' WHERE id=480; -- Floridablanca / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.66408888982069', osm_rotulolon='-73.3328980067883' WHERE id=503; -- Galán / Santander
      UPDATE msip_municipio SET osm_rotulolat='5.912170379969727', osm_rotulolon='-73.34468872659833' WHERE id=507; -- Gámbita / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.090940991587777', osm_rotulolon='-73.3067281421365' WHERE id=548; -- Girón / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.925698601585176', osm_rotulolon='-72.84549681636952' WHERE id=562; -- Guaca / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.222105983458501', osm_rotulolon='-73.41320855564743' WHERE id=572; -- Guadalupe / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.316146028591261', osm_rotulolon='-73.33427785776031' WHERE id=577; -- Guapotá / Santander
      UPDATE msip_municipio SET osm_rotulolat='5.946100532737991', osm_rotulolon='-73.72594385672723' WHERE id=582; -- Guavatá / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.028088297656888', osm_rotulolon='-73.58532851202447' WHERE id=587; -- Güepsa / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.55385038545333', osm_rotulolon='-73.36877942130405' WHERE id=596; -- Hato / Santander
      UPDATE msip_municipio SET osm_rotulolat='5.855907427104094', osm_rotulolon='-73.84984101761123' WHERE id=628; -- Jesús María / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.713704026092986', osm_rotulolon='-73.09960387396113' WHERE id=629; -- Jordán / Santander
      UPDATE msip_municipio SET osm_rotulolat='5.911884556132788', osm_rotulolon='-74.06195498666436' WHERE id=635; -- La Belleza / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.223315555180695', osm_rotulolon='-73.63809034910035' WHERE id=660; -- La Paz / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.320925486529942', osm_rotulolon='-73.89706050830026' WHERE id=652; -- Landázuri / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.212404314580807', osm_rotulolon='-73.32015315683167' WHERE id=706; -- Lebrija / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.81396574205159', osm_rotulolon='-73.11419770370006' WHERE id=721; -- Los Santos / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.502407150468054', osm_rotulolon='-72.59525970511982' WHERE id=731; -- Macaravita / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.723813425986026', osm_rotulolon='-72.74300438145171' WHERE id=744; -- Málaga / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.341367172744404', osm_rotulolon='-73.06604793787842' WHERE id=763; -- Matanza / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.492001018841225', osm_rotulolon='-72.96853058975383' WHERE id=780; -- Mogotes / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.645971388331286', osm_rotulolon='-72.83113985370223' WHERE id=786; -- Molagavita / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.354841003538486', osm_rotulolon='-73.12687389770643' WHERE id=822; -- Ocamonte / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.217933841486762', osm_rotulolon='-73.29833998338654' WHERE id=850; -- Oiba / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.345114088988765', osm_rotulolon='-72.83571098986182' WHERE id=853; -- Onzaga / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.526228197622646', osm_rotulolon='-73.29538682117261' WHERE id=884; -- Palmar / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.389907496749052', osm_rotulolon='-73.28528140597977' WHERE id=890; -- Palmas del Socorro / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.41752877532523', osm_rotulolon='-73.17609685285919' WHERE id=901; -- Páramo / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.962812899218383', osm_rotulolon='-73.01847026741083' WHERE id=914; -- Piedecuesta / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.514946607557634', osm_rotulolon='-73.17892074458875' WHERE id=919; -- Pinchote / Santander
      UPDATE msip_municipio SET osm_rotulolat='5.814403338928464', osm_rotulolon='-73.69941369265902' WHERE id=948; -- Puente Nacional / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.698698906652438', osm_rotulolon='-73.97763416898651' WHERE id=955; -- Puerto Parra / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.555429104456215', osm_rotulolon='-73.80899975233544' WHERE id=958; -- Puerto Wilches / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.512640689976123', osm_rotulolon='-73.43203051920999' WHERE id=1027; -- Rionegro / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.409435329873112', osm_rotulolon='-73.6008735349495' WHERE id=1063; -- Sabana de Torres / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.79958394571806', osm_rotulolon='-72.82445791484403' WHERE id=1090; -- San Andrés / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.104073674642623', osm_rotulolon='-73.53160992646349' WHERE id=1100; -- San Benito / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.554820746292354', osm_rotulolon='-73.13630747148387' WHERE id=1115; -- San Gil / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.457358366370613', osm_rotulolon='-72.86489813069439' WHERE id=1121; -- San Joaquín / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.625467222829853', osm_rotulolon='-72.74491417652615' WHERE id=1127; -- San José de Miranda / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.566868831061366', osm_rotulolon='-72.65156570492789' WHERE id=1131; -- San Miguel / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.860839578052308', osm_rotulolon='-73.56526789805251' WHERE id=1140; -- San Vicente de Chucurí / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.962200096377438', osm_rotulolon='-72.9242676847824' WHERE id=1178; -- Santa Bárbara / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.415444042371774', osm_rotulolon='-73.59733936900977' WHERE id=1187; -- Santa Helena del Opón / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.667407529028291', osm_rotulolon='-73.70337417923082' WHERE id=1205; -- Simacota / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.455784608083758', osm_rotulolon='-73.24761610083517' WHERE id=1218; -- Socorro / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.112024268065331', osm_rotulolon='-73.37191844061122' WHERE id=1240; -- Suaita / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.45717700781235', osm_rotulolon='-73.00071172495254' WHERE id=1258; -- Suratá / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.150834412111927', osm_rotulolon='-72.94173287432395' WHERE id=1332; -- Tona / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.429044329935752', osm_rotulolon='-73.11223820073442' WHERE id=1369; -- Valle de San José / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.269388640793604', osm_rotulolon='-73.71918118252239' WHERE id=1378; -- Vélez / Santander
      UPDATE msip_municipio SET osm_rotulolat='7.324316373140006', osm_rotulolon='-72.88597124102611' WHERE id=1382; -- Vetas / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.685467888382373', osm_rotulolon='-73.17362552017742' WHERE id=1390; -- Villanueva / Santander
      UPDATE msip_municipio SET osm_rotulolat='6.873468161973634', osm_rotulolon='-73.32421477897203' WHERE id=1425; -- Zapatoca / Santander
      UPDATE msip_municipio SET osm_rotulolat='9.339676942983717', osm_rotulolon='-74.94562511217184' WHERE id=107; -- Buenavista / Sucre
      UPDATE msip_municipio SET osm_rotulolat='8.766264784899873', osm_rotulolon='-75.14551154200964' WHERE id=137; -- Caimito / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.5770367702683', osm_rotulolon='-75.33111313919723' WHERE id=396; -- Chalán / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.535416509718647', osm_rotulolon='-75.36685351196593' WHERE id=337; -- Colosó / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.212961299667803', osm_rotulolon='-75.25921062846218' WHERE id=364; -- Corozal / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.401449623216909', osm_rotulolon='-75.65182520428718' WHERE id=378; -- Coveñas / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.077738369664061', osm_rotulolon='-75.1255581084588' WHERE id=402; -- El Roble / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.124540623801378', osm_rotulolon='-74.9775658832005' WHERE id=405; -- Galeras / Sucre
      UPDATE msip_municipio SET osm_rotulolat='8.379930483340365', osm_rotulolon='-74.67368162585124' WHERE id=463; -- Guaranda / Sucre
      UPDATE msip_municipio SET osm_rotulolat='8.816783970949006', osm_rotulolon='-75.29534698026355' WHERE id=693; -- La Unión / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.43622641292932', osm_rotulolon='-75.23237126732558' WHERE id=720; -- Los Palmitos / Sucre
      UPDATE msip_municipio SET osm_rotulolat='8.562349976545395', osm_rotulolon='-74.75412543170467' WHERE id=736; -- Majagual / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.388849015358891', osm_rotulolon='-75.33640198518844' WHERE id=793; -- Morroa / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.54965221593839', osm_rotulolon='-75.2003972902342' WHERE id=859; -- Ovejas / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.347262058396597', osm_rotulolon='-75.58037036592195' WHERE id=886; -- Palmito / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.168140466697611', osm_rotulolon='-75.37120092289685' WHERE id=1092; -- Sampués / Sucre
      UPDATE msip_municipio SET osm_rotulolat='8.857804739059128', osm_rotulolon='-74.9490422659719' WHERE id=1112; -- San Benito Abad / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.502430284751508', osm_rotulolon='-75.45643432816733' WHERE id=1336; -- San José de Toluviejo / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.292301835268185', osm_rotulolon='-75.219777232153' WHERE id=1176; -- San Juan de Betulia / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.254927959876305', osm_rotulolon='-75.07368252446459' WHERE id=1200; -- San Luis de Sincé / Sucre
      UPDATE msip_municipio SET osm_rotulolat='8.590955541303458', osm_rotulolon='-75.19030470958947' WHERE id=1180; -- San Marcos / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.82050635174984', osm_rotulolon='-75.53508661735113' WHERE id=1183; -- San Onofre / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.388300234556839', osm_rotulolon='-75.03499401959462' WHERE id=1184; -- San Pedro / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.531689621905997', osm_rotulolon='-75.53778338596565' WHERE id=1331; -- Santiago de Tolú / Sucre
      UPDATE msip_municipio SET osm_rotulolat='9.330071110490668', osm_rotulolon='-75.4492411496934' WHERE id=53; -- Sincelejo / Sucre
      UPDATE msip_municipio SET osm_rotulolat='8.8034608567237', osm_rotulolon='-74.72064927338462' WHERE id=1244; -- Sucre / Sucre
      UPDATE msip_municipio SET osm_rotulolat='3.386947264480395', osm_rotulolon='-74.96127651209905' WHERE id=412; -- Alpujarra / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.583949877582484', osm_rotulolon='-74.99853415575204' WHERE id=454; -- Alvarado / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.825362259203847', osm_rotulolon='-74.81985132867929' WHERE id=537; -- Ambalema / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.653135104969676', osm_rotulolon='-75.20402637745764' WHERE id=737; -- Anzoátegui / Tolima
      UPDATE msip_municipio SET osm_rotulolat='5.027064942416024', osm_rotulolon='-74.84592181502687' WHERE id=923; -- Armero / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.419822941966093', osm_rotulolon='-75.50047828638908' WHERE id=1091; -- Ataco / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.402163530147676', osm_rotulolon='-75.50530702521151' WHERE id=138; -- Cajamarca / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.122219735059227', osm_rotulolon='-74.75041576853245' WHERE id=199; -- Carmen de Apicalá / Tolima
      UPDATE msip_municipio SET osm_rotulolat='5.029742921847621', osm_rotulolon='-75.17089283348436' WHERE id=220; -- Casabianca / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.733233452569906', osm_rotulolon='-75.62881974099074' WHERE id=243; -- Chaparral / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.356556510645715', osm_rotulolon='-74.9313969839548' WHERE id=333; -- Coello / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.724549826300597', osm_rotulolon='-75.17453496732689' WHERE id=368; -- Coyaima / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.961056281673571', osm_rotulolon='-74.67219698894434' WHERE id=388; -- Cunday / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.62958821627655', osm_rotulolon='-74.74530465854268' WHERE id=407; -- Dolores / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.18263595379236', osm_rotulolon='-74.91032216008863' WHERE id=468; -- Espinal / Tolima
      UPDATE msip_municipio SET osm_rotulolat='5.079760419063241', osm_rotulolon='-74.95806911470193' WHERE id=472; -- Falan / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.24220148567017', osm_rotulolon='-74.84910700177602' WHERE id=477; -- Flandes / Tolima
      UPDATE msip_municipio SET osm_rotulolat='5.184669601547735', osm_rotulolon='-75.05084509409703' WHERE id=487; -- Fresno / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.071605034933818', osm_rotulolon='-74.98569726467635' WHERE id=570; -- Guamo / Tolima
      UPDATE msip_municipio SET osm_rotulolat='5.047339215279774', osm_rotulolon='-75.24348709893621' WHERE id=599; -- Herveo / Tolima
      UPDATE msip_municipio SET osm_rotulolat='5.185969847417836', osm_rotulolon='-74.77891343806706' WHERE id=601; -- Honda / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.398723415385495', osm_rotulolon='-75.3030057907621' WHERE id=34; -- Ibagué / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.141580709848341', osm_rotulolon='-74.5474354710754' WHERE id=606; -- Icononzo / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.865636351792798', osm_rotulolon='-74.9348247457926' WHERE id=710; -- Lérida / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.878387904683355', osm_rotulolon='-75.05327686173352' WHERE id=716; -- Líbano / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.19545869568412', osm_rotulolon='-74.63999489677772' WHERE id=765; -- Melgar / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.815918559164347', osm_rotulolon='-75.22510727385767' WHERE id=779; -- Murillo / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.537482587487141', osm_rotulolon='-75.14534679960909' WHERE id=805; -- Natagaima / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.925803787346754', osm_rotulolon='-75.28958076995212' WHERE id=855; -- Ortega / Tolima
      UPDATE msip_municipio SET osm_rotulolat='5.091709129616447', osm_rotulolon='-75.01626308668618' WHERE id=882; -- Palocabildo / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.488675338060496', osm_rotulolon='-74.9374777950783' WHERE id=915; -- Piedras / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.101942809552095', osm_rotulolon='-75.81603078933317' WHERE id=929; -- Planadas / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.72576533533964', osm_rotulolon='-74.88452286097716' WHERE id=937; -- Prado / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.848834655784157', osm_rotulolon='-74.8973227812521' WHERE id=971; -- Purificación / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.435053304697062', osm_rotulolon='-75.87837693035387' WHERE id=1030; -- Rioblanco / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.09937268659085', osm_rotulolon='-75.59943668073454' WHERE id=1039; -- Roncesvalles / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.211233998254523', osm_rotulolon='-75.37631504536738' WHERE id=1041; -- Rovira / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.914504969715554', osm_rotulolon='-75.02878807398423' WHERE id=1098; -- Saldaña / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.978828654980321', osm_rotulolon='-75.50934354983352' WHERE id=1106; -- San Antonio / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.148632715019668', osm_rotulolon='-75.13029749984973' WHERE id=1114; -- San Luis / Tolima
      UPDATE msip_municipio SET osm_rotulolat='5.232451364831867', osm_rotulolon='-74.91669346591138' WHERE id=761; -- San Sebastián de Mariquita / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.727760449860948', osm_rotulolon='-75.20702714640474' WHERE id=1133; -- Santa Isabel / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.07655283810651', osm_rotulolon='-74.82264587579172' WHERE id=1243; -- Suárez / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.18442886150654', osm_rotulolon='-75.1762843093818' WHERE id=1366; -- Valle de San Juan / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.703119027819555', osm_rotulolon='-74.94054939306416' WHERE id=1375; -- Venadillo / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.956293186430001', osm_rotulolon='-75.17783459830031' WHERE id=1387; -- Villahermosa / Tolima
      UPDATE msip_municipio SET osm_rotulolat='3.851864553696478', osm_rotulolon='-74.61864287793553' WHERE id=1396; -- Villarrica / Tolima
      UPDATE msip_municipio SET osm_rotulolat='4.686296438738868', osm_rotulolon='-75.79394172963691' WHERE id=325; -- Alcalá / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.145127877568798', osm_rotulolon='-76.16318606347573' WHERE id=615; -- Andalucía / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.793146657159908', osm_rotulolon='-76.01876838780612' WHERE id=711; -- Ansermanuevo / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.715015919324028', osm_rotulolon='-76.14674286984544' WHERE id=905; -- Argelia / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.362613725269046', osm_rotulolon='-76.37140738569619' WHERE id=78; -- Bolívar / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.625971899645813', osm_rotulolon='-77.34208089418802' WHERE id=86; -- Buenaventura / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.187237837270756', osm_rotulolon='-76.09775240235618' WHERE id=113; -- Bugalagrande / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.31524569991286', osm_rotulolon='-75.84654940989138' WHERE id=134; -- Caicedonia / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.398656488726947', osm_rotulolon='-76.58480947479146' WHERE id=28; -- Cali / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.944653004222392', osm_rotulolon='-76.63192698273522' WHERE id=143; -- Calima / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.39045019817853', osm_rotulolon='-76.39108005389518' WHERE id=166; -- Candelaria / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.71247212206371', osm_rotulolon='-75.93110392888073' WHERE id=197; -- Cartago / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.665632882414842', osm_rotulolon='-76.73820921881833' WHERE id=401; -- Dagua / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.927570660347032', osm_rotulolon='-76.06848636636379' WHERE id=417; -- El Águila / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.760743106493368', osm_rotulolon='-76.22222040980887' WHERE id=427; -- El Cairo / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.67294225665465', osm_rotulolon='-76.21860989578384' WHERE id=429; -- El Cerrito / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.536039992102963', osm_rotulolon='-76.29030488276064' WHERE id=438; -- El Dovio / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.314004147088968', osm_rotulolon='-76.17936647081166' WHERE id=478; -- Florida / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.743711854412118', osm_rotulolon='-76.2054361169822' WHERE id=545; -- Ginebra / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.785486355009239', osm_rotulolon='-76.30753035319768' WHERE id=563; -- Guacarí / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.873335651041282', osm_rotulolon='-76.12524312763082' WHERE id=112; -- Guadalajara de Buga / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.206320484246445', osm_rotulolon='-76.60466479485162' WHERE id=622; -- Jamundí / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.700228158188379', osm_rotulolon='-76.59171693684168' WHERE id=637; -- La Cumbre / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.529289725901906', osm_rotulolon='-76.1005081029327' WHERE id=694; -- La Unión / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.484621615790278', osm_rotulolon='-75.97223054542562' WHERE id=702; -- La Victoria / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.592483118809603', osm_rotulolon='-75.95908003328616' WHERE id=821; -- Obando / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.552809422787539', osm_rotulolon='-76.2275104365058' WHERE id=881; -- Palmira / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.42133213036343', osm_rotulolon='-76.17606819232624' WHERE id=936; -- Pradera / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.799302661539251', osm_rotulolon='-76.54417874313924' WHERE id=1019; -- Restrepo / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.108357485594183', osm_rotulolon='-76.35376426416205' WHERE id=1031; -- Riofrío / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.448466311285027', osm_rotulolon='-76.18144793945538' WHERE id=1038; -- Roldanillo / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.980873086063809', osm_rotulolon='-76.2112346787177' WHERE id=1096; -- San Pedro / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.166261314715682', osm_rotulolon='-75.90644891254482' WHERE id=1196; -- Sevilla / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.63544234739156', osm_rotulolon='-76.079186874907' WHERE id=1338; -- Toro / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.235409612052188', osm_rotulolon='-76.34579142682496' WHERE id=1340; -- Trujillo / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.014270637182523', osm_rotulolon='-76.06305293822412' WHERE id=1344; -- Tuluá / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.706930447784122', osm_rotulolon='-75.79034179042094' WHERE id=1358; -- Ulloa / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.608087019711908', osm_rotulolon='-76.24478795176121' WHERE id=1380; -- Versalles / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.750554872418969', osm_rotulolon='-76.4771932217046' WHERE id=1385; -- Vijes / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.914999687805749', osm_rotulolon='-76.40022315048651' WHERE id=1421; -- Yotoco / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='3.593712429621746', osm_rotulolon='-76.51721767449928' WHERE id=1422; -- Yumbo / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='4.38589134878697', osm_rotulolon='-76.04476769222981' WHERE id=1427; -- Zarzal / Valle del Cauca
      UPDATE msip_municipio SET osm_rotulolat='1.048164484707994', osm_rotulolon='-71.30022643173567' WHERE id=235; -- Carurú / Vaupés
      UPDATE msip_municipio SET osm_rotulolat='0.981303046395209', osm_rotulolon='-70.35705349612454' WHERE id=39; -- Mitú / Vaupés
      UPDATE msip_municipio SET osm_rotulolat='0.227192899497972', osm_rotulolon='-70.9299032059245' WHERE id=866; -- Pacoa / Vaupés
      UPDATE msip_municipio SET osm_rotulolat='1.619494759918942', osm_rotulolon='-70.56487513546794' WHERE id=1250; -- Papunahua / Vaupés
      UPDATE msip_municipio SET osm_rotulolat='-0.735337013142372', osm_rotulolon='-69.95226210849479' WHERE id=1084; -- Taraira / Vaupés
      UPDATE msip_municipio SET osm_rotulolat='0.822506740942084', osm_rotulolon='-69.66365608534775' WHERE id=1418; -- Yavaraté / Vaupés
      UPDATE msip_municipio SET osm_rotulolat='4.175733321749778', osm_rotulolon='-69.6274296482946' WHERE id=1246; -- Cumaribo / Vichada
      UPDATE msip_municipio SET osm_rotulolat='5.424905307737558', osm_rotulolon='-69.67794517022584' WHERE id=887; -- La Primavera / Vichada
      UPDATE msip_municipio SET osm_rotulolat='5.719777501335835', osm_rotulolon='-68.2129960904554' WHERE id=47; -- Puerto Carreño / Vichada
      UPDATE msip_municipio SET osm_rotulolat='4.976306051243793', osm_rotulolon='-70.67000111370083' WHERE id=1042; -- Santa Rosalía / Vichada
     
    SQL
  end

  def down
    execute <<-SQL
      UPDATE msip_pais SET osm_rotulolat=NULL,
        osm_rotulolon=NULL WHERE id=170; -- Colombia
      UPDATE msip_departamento SET osm_rotulolat=NULL,
        osm_rotulolon=NULL WHERE id_pais=170; 
      UPDATE msip_municipio SET osm_rotulolat=NULL, osm_rotulolon=NULL
        WHERE id_departamento IN 
          (SELECT id FROM msip_departamento WHERE id_pais=170); 
    SQL
  end
end
