class MejoraRaquiraSucre < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL
      UPDATE msip_municipio 
        SET osm_rotulolat='7.2440678', osm_rotulolon='-76.4365420'
        WHERE id=799; -- Mutatá / Antioquia
      UPDATE msip_municipio 
        SET osm_rotulolat='8.0950848', osm_rotulolon='-76.7285450'
        WHERE id=1348; -- Turbo / Antioquia
      UPDATE msip_municipio 
        SET osm_rotulolat='6.0142', osm_rotulolon='-73.9964'
        WHERE id=1247; -- Sucre / Santander
      UPDATE msip_municipio 
        SET osm_rotulolat='4.8525', osm_rotulolon='-74.0219'
        WHERE id=262; -- Chía / Cundinamarca
      UPDATE msip_municipio 
        SET osm_rotulolat='5.1785', osm_rotulolon='-71.11044'
        WHERE id=743; -- Trinidad / Casanare
      UPDATE msip_municipio 
        SET osm_rotulolat='12.5261', osm_rotulolon='-81.7103'
        WHERE id=50; -- San Andrés

      UPDATE msip_municipio 
        SET osm_rotulolat='13.3480', osm_rotulolon='-81.3777'
        WHERE id=938; -- Providencia

      UPDATE msip_municipio 
        SET osm_rotulolat='5.4943', osm_rotulolon='-73.6222'
        WHERE id=1013; -- Ráquira / Boyacá
    SQL
  end

  def down
  end
end
