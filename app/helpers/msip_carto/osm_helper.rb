module MsipCarto
  module OsmHelper

    # Remplaza en una cadena Markdown unos elementos por HTML
    # 1. `este` por <tt>este</tt>
    # 2. <url> por <a href='url'>url</a>
    def mini_md_html(md)
      pmd = md.dup
      pmd.gsub!(/<([^>]*)>/, '<a href=\'\1\' target=\'_blank\'>\1</a>')
      pmd.gsub!(/`([^`]*)`/, '<tt>\1</tt>')
      pmd
    end
    module_function :mini_md_html

    # Descarga una serie de siluetas de osm-boundaries.com usando la llave 
    # dada las almacena en la tabla osm_relacion y las retorna como un Hash
    #
    # @params llave_osmboundaries Llave en osm-boundaries.com
    # @params osm_ids lista de identificaciones de relaciones en OSM
    # @params version Versión de datos de osm-boundaries.com
    # @params prob    colchón de problemas
    # @return Hash con entradas de la forma id=>geometria donde cada id
    #         corresponde a uno de osm_ids o nil si hay error
    def descargar_siluetas_osmboundaries(
      llave_osmboundaries, osm_ids, version = 'osm20221205', prob)
      if osm_ids.count >=100
        prob << "osm-boundaries permite máximo 100, "\
          "osm_ids tiene #{osm_ids.count}"
        return nil
      end
      ret = {}
      ids = osm_ids.join(',')
      url = 'https://osm-boundaries.com/Download/Submit'\
        "?apiKey=#{llave_osmboundaries}&db=#{version}&osmIds=#{ids}"\
        '&format=EWKB&srid=4326&landOnly' 
      
      # sin landOnly cargaría fronteras marítimas
      warn "OJO url=#{url}"
      # e.g https://osm-boundaries.com/Download/Submit?apiKey=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&db=osm20220808&osmIds=-1303962,-1315120,-1388045,-2181889,-1315209,-12893646,-1316581,-1390144,-396705,-1394843,-1392025,-1392085,-1317223,-1322131,-1305533,-1321032,-1385038,-1380540,-1396291,-1321379,-1319097,-1305166,-1380130,-1324192,-1375357,-1331230,-1374221,-1372374,-1320759,-1309495,-1322825,-1359366,-1357098&format=EWKB&srid=4326&landOnly
      njs = Tempfile.new(['msipcartosil', '.gz'])
      cmd = %(wget ) +
        %(-O #{njs.path} ) +
        %("#{url}")
      warn cmd
      `#{cmd}`
      h = File.read(njs)

      
      gz = Zlib::GzipReader.new(StringIO.new(h))
      des = gz.read
      if des == ''
        prob << "Respuésta vacía de url #{url}"
        return nil
      end

      ewkb = JSON.parse(des)
      if ewkb.class != Array || ewkb[0].class != Hash ||
         ewkb[0]['osm_id'].class != Integer ||
         ewkb[0]['geom'].class != String
        prob << "Respuesta de url no es un JSON EWKB"
        return nil
      end

      ewkb.each do |unarel|
        id = unarel['osm_id'].to_i
        g = unarel['geom']
        if g[0] != '\\' || g[1] != 'x'
          prob << "En respuesta de url el atributo `geom` no tiene "\
            "la sintaxis esperada"
          return nil
        end
        ret[id] = g[2..-1]

        ActiveRecord::Base.connection.execute(
          "DELETE FROM osm_relacion WHERE id=#{id}"
        )
        ActiveRecord::Base.connection.execute(
          'INSERT INTO osm_relacion (id, geom) '\
          "VALUES (#{id}, ST_Multi('#{ret[id]}'))"
        )
      end

      ret
    end
    module_function :descargar_siluetas_osmboundaries

    # Convierte una cadena con un geojson a un svg como objeto Nokogiri
    # @param geoj Cadena con geojson
    # @param ancho Ancho de imagen por generar
    #
    # @return objeto nokgiri con el svg
    def convertir_geojson_a_svg(geoj, ancho=512)
      # Ideas de
      # https://github.com/Vonvikken/svg-maps/blob/master/lib/svg_maps_italy/svg_builder.rb
      ngeoj = Tempfile.new(['msipcarto', '.geojson'])
      nsvg = Tempfile.new(['msipcarto', '.svg'])
      File.open(ngeoj, 'w') do |arch|
        arch.write(geoj)
      end
      cmd = %(node_modules/mapshaper/bin/mapshaper -i "#{ngeoj.path}" ) +
        %(-filter-islands ) +
        %(-o format=svg id-field=id width=#{ancho} "#{nsvg.path}")
      puts "#{cmd}"
      `#{cmd}`
      File.open(nsvg) { |f| Nokogiri::XML(f) }
    end
    module_function :convertir_geojson_a_svg

    # Descarga y valida una vía
    # @param ref Referencia a la vía
    # @param prob Colchón para problemas encontrados
    def validar_via(ref, prob)
      url = "https://www.openstreetmap.org/api/0.6/way/#{ref}.json"
      uri = URI.parse(url)
      h = uri.read
      rel = JSON.parse(h)
      elem = rel['elements'][0]
      tags = elem['tags']
      if !tags
        prob << "La vía <https://www.openstreetmap.org/edit?way=#{ref}> "\
          'no tiene etiquetas'
      else
        unless tags['name']
          prob << "La vía <https://www.openstreetmap.org/edit?way=#{ref}> "\
            'no tiene etiqueta `name`'
        end
        if tags.keys.select { |l| l[0..4] == 'name:' }.count > 0 &&
           !tags['name:es']
          prob << "La vía <https://www.openstreetmap.org/edit?way=#{ref}> "\
            'requiere etiqueta `name:es`'
        end
        %w[
          mechanical ref state_capital state_code
        ].each do |t|
          next unless tags[t]

          prob << 'En la vía '\
            "<https://www.openstreetmap.org/edit?way=#{ref}> "\
            " sobra etiqueta `#{t}`"
        end
      end
    end
    module_function :validar_via


    def url_nodo_osm(id)
      "https://www.openstreetmap.org/api/0.6/node/#{id}.json"
    end
    module_function :url_nodo_osm


    def descargar_nodo_de_osm(id, _prob)
      url = MsipCarto::OsmHelper.url_nodo_osm(id)
      uri = URI.parse(url)
      # Reintentos y redirección según
      # https://stackoverflow.com/questions/27407938/ruby-open-uri-redirect-forbidden
      intentos = 3
      begin
          uri.open(redirect: false)
      rescue OpenURI::HTTPRedirect => redirect
          uri = redirect.uri # assigned from the "Location" response header
          warn "Redirigiendo a #{uri.to_s}"
          retry if (intentos -= 1) > 0
          raise
      end
      h = uri.read
      nodo = JSON.parse(h)
      elem = nodo['elements'][0]
      etiquetas = nodo['tags']
      {
        nodo: nodo,
        elemento: elem,
        etiquetas: etiquetas
      }
    end
    module_function :descargar_nodo_de_osm


    def url_relacion_osm(id)
      "https://www.openstreetmap.org/api/0.6/relation/#{id.to_i.abs}.json"
    end
    module_function :url_relacion_osm


    def descargar_relacion_de_osm(id, prob)
      url = MsipCarto::OsmHelper.url_relacion_osm(id)
      uri = URI.parse(url)

      warn "Descargando de #{url}"
      intentos = 3
      begin
        uri.open(redirect: false)
        h = uri.read
      rescue OpenURI::HTTPRedirect => redirect
        uri = redirect.uri # assigned from the "Location" response header
        warn "Redirigiendo a #{uri.to_s}"
        retry if (intentos -= 1) > 0
        raise
      rescue Exception => e
        prob << "No pudo descargarse relación #{id}"
        return nil
      end

      relacion = JSON.parse(h)
      elem = relacion['elements'][0]
      etiquetas = elem['tags']
      miembros = elem['members']
      {
        relacion: relacion,
        elemento: elem,
        etiquetas: etiquetas,
        miembros: miembros
      }
    end
    module_function :descargar_relacion_de_osm

    # Descarga y valida un nodo rotulo (label)
    # @param id Identificación del nodo
    # @param nombre Identificación del nodo
    # @param prob Colchón para problemas encontrados
    def validar_nodo_rotulo(id, nombre, prob)
      n  = MsipCarto::OsmHelper.descargar_nodo_de_osm(id, prob)
      if !n[:etiquetas]
        prob << 'El nodo rótulo (`label`) '\
          "<#{MsipCarto::OsmHelper.url_nodo_osm(id)}> "\
          'no tiene etiquetas'
      else
        if n[:etiquetas]['name'].to_s != nombre
          prob << 'El nodo rótulo (`label`) '\
            "<#{MsipCarto::OsmHelper.url_nodo_osm(id)}> "\
            "debería tener rotulo `name` -> `#{nombre}`"
        end
        if n[:etiquetas].keys.select { |l| l[0..4] == 'name:' }.count > 0 &&
           !n[:etiquetas]['name:es']
          prob << 'El nodo rótulo (`label`) '\
            "<#{MsipCarto::OsmHelper.url_nodo_osm(id)}> "\
            'requiere etiqueta `name:es`'
        end
        if n[:etiquetas]['place'].to_s != 'state'
          prob << 'El nodo rótulo (`label`) '\
            "<#{MsipCarto::OsmHelper.url_nodo_osm(id)}> "\
            'debería tener etiqueta `place` -> `state`'
        end
      end
    end
    module_function :validar_nodo_rotulo

    # Descarga y valida un nodo centro administrativo (admin_centre)
    # @param prob Colchón para problemas encontrados
    def validar_nodo_centro_administrativo(id, prob)
      n  = MsipCarto::OsmHelper.descargar_nodo_de_osm(id, prob)
      if !n[:etiquetas]
        prob << 'El nodo centro administrativo (`admin_centre`) '\
          "<#{MsipCarto::OsmHelper.url_nodo_osm(id)}> "\
          'no tiene etiquetas'
      else
        unless n[:etiquetas]['name']
          prob << 'El nodo centro administrativo (`admin_centre`) '\
            "<#{MsipCarto::OsmHelper.url_nodo_osm(id)}> "\
            'debería tener etiqueta `name`'
        end
        if n[:etiquetas].keys.select { |l| l[0..4] == 'name:' }.count > 0 &&
           !n[:etiquetas]['name:es']
          prob << 'El nodo centro administrativo (`admin_centre`) '\
            "<#{MsipCarto::OsmHelper.url_nodo_osm(id)}> "\
            ' requiere etiqueta `name:es`'
        end
        if n[:etiquetas]['place'].to_s != 'city'
          prob << 'El nodo centro administrativo (`admin_centre`) '\
            "<#{MsipCarto::OsmHelper.url_nodo_osm(id)}> "\
            'debería tener etiqueta `place` -> `city`'
        end
      end
    end
    module_function :validar_nodo_centro_administrativo

    # Descarga una relación y extrae el miembro con rol label
    # @param idrel Id de la relación
    # @param tabla Tabla con frontera en caso de no encontrar rotulo 
    # @param idtabla Id de registro con frontera en tabla 
    # @param prob Colchón para problemas encontrados
    def cargar_o_inventar_coord_rotulo_de_relacion(idrel, tabla, idtabla, prob)
      r = MsipCarto::OsmHelper.descargar_relacion_de_osm(idrel, prob)
      if !r || !r[:relacion]['elements'] || r[:relacion]['elements'].count == 0
        prob << "La relación "\
          "<#{MsipCarto::OsmHelper.url_relacion_osm(idrel)}> "\
          "no tiene elementos"
        return nil
      end
      unless r[:miembros]
        prob << "La relación "\
          "<#{MsipCarto::OsmHelper.url_relacion_osm(idrel)}> "\
          "no tiene miembros"
        return nil
      end
      nrotulo = r[:miembros].select do |e|
        e['type'] && e['type'] == 'node' &&
          e['role'] && e['role'] == 'label'
      end
      if nrotulo.count == 0
        mens = "La relación "\
          "<#{MsipCarto::OsmHelper.url_relacion_osm(idrel)}> "\
          "no tiene un nodo miembro rotulo (`label`). "
        warn mens
        c = ActiveRecord::Base.connection.execute <<~SQL
          SELECT ST_ASTEXT(ST_CENTROID(osm_frontera))
            FROM #{tabla}
            WHERE id=#{idtabla}
        SQL
        m = c[0]["st_astext"].match(/POINT\(([-.0-9]*) ([-.0-9]*)\)/)
        if !m
          prob << mens
          prob << "No se pudo calcular centroido de la frontera de registro "\
            "#{idtabla} en la tabla #{tabla}. "
          return nil
        end
        return {
          lon: m[1].to_f,
          lat: m[2].to_f
        }
      elsif nrotulo.count > 1
        prob << "La relación "\
          "<#{MsipCarto::OsmHelper.url_relacion_osm(id)}> "\
          "tiene más de un miembro nrotulo"
        return nil
      end
      nodo_id = nrotulo[0]['ref']

      nodo = descargar_nodo_de_osm(nodo_id, prob)

      {
        lat: nodo[:elemento]['lat'],
        lon: nodo[:elemento]['lon']
      }
    end
    module_function :cargar_o_inventar_coord_rotulo_de_relacion

    # @param municipio ActiveRecord
    # @param llave     string   Llave en OSM-Boundaries
    # @param fronteras Hash     nil o geometrías descargadas de osm-boundaries
    # @param prob      string   Colchón para poner problemas encontrados 
    # @return bool true sii puede actualizar, dlc problema en colchón prob
    def actualizar_silueta_municipio_de_osm(
      municipio, llave, fronteras, prob
    )
      if municipio.osm_id.nil?
        prob = 'Municipio no tiene id de OSM'
        return false
      end

      if fronteras.nil?
        fronteras = {}
      end
      if !fronteras[municipio.osm_id]
        r = OsmHelper.descargar_siluetas_osmboundaries(
            llave, [municipio.osm_id] ,prob)
        if r.nil?
          return false
        end
        fronteras.merge!(r)
      end
      ActiveRecord::Base.connection.execute <<~SQL
        UPDATE msip_municipio
          SET osm_frontera=ST_Multi('#{fronteras[municipio.osm_id]}'),
          osm_fecha=now()
          WHERE id=#{municipio.id}
        ;
      SQL
      coord = OsmHelper.cargar_o_inventar_coord_rotulo_de_relacion(
        municipio.osm_id, 'msip_municipio', municipio.id, prob
      )
      return false if coord.nil?
      ActiveRecord::Base.connection.execute <<~SQL
        UPDATE msip_municipio
          SET osm_rotulolat='#{coord[:lat]}',
          osm_rotulolon='#{coord[:lon]}',
          osm_fecha=now()
          WHERE id=#{municipio.id}
        ;
      SQL
      true
    end
    module_function :actualizar_silueta_municipio_de_osm

    # @param departamento ActiveRecord
    # @param llave     string   Llave en OSM-Boundaries
    # @param recursivo bool     hacer lo mismo con departamentos del país
    # @param fronteras Hash     nil o geometrías descargadas de osm-boundaries
    # @param prob      string   Colchón para poner problemas encontrados
    # @return bool true sii puede actualizar, dlc problema en colchón prob
    def actualizar_silueta_departamento_de_osm(
      departamento, llave, recursivo, fronteras, prob
    )
      if departamento.osm_id.nil?
        prob = 'Departamento no tiene id de OSM'
        return false
      end
      if fronteras.nil?
        fronteras = {}
      end
      pide_ids = []
      if !fronteras[departamento.osm_id]
        pide_ids << departamento.osm_id
      end
      lmunicipios = nil
      if recursivo
        lmunicipios = Msip::Municipio.habilitados.where(
          departamento_id: departamento.id
        ).where(osm_rotulolat: nil) # OJO
        lmunicipios.each do |municipio|
          if !fronteras[municipio.osm_id] && pide_ids.count < 99
            pide_ids << municipio.osm_id
          end
        end
      end

      if pide_ids.count > 0
        r = OsmHelper.descargar_siluetas_osmboundaries(llave, pide_ids, prob)
        if r.nil?
          return false
        end
        fronteras.merge!(r)
      end
      ActiveRecord::Base.connection.execute <<~SQL
        UPDATE msip_departamento SET 
          osm_frontera=ST_Multi('#{fronteras[departamento.osm_id]}'),
          osm_fecha=now()
          WHERE id=#{departamento.id}
        ;
      SQL
      coord = OsmHelper.cargar_o_inventar_coord_rotulo_de_relacion(
        departamento.osm_id, 'msip_departamento', departamento.id, prob
      )
      return false if coord.nil?

      ActiveRecord::Base.connection.execute <<~SQL
        UPDATE msip_departamento SET 
          osm_rotulolat='#{coord[:lat]}',
          osm_rotulolon='#{coord[:lon]}',
          osm_fecha=now()
          WHERE id=#{departamento.id}
        ;
      SQL

      if recursivo
        cuenta = 0
        lmunicipios.each do |municipio|
          puts "OJO cuenta=#{cuenta}"
          cuenta += 1
          unless actualizar_silueta_municipio_de_osm(
              municipio, llave, fronteras, prob
          )
            warn "Problema con municipio #{municipio.nombre}: #{prob}"
          end
        end
      end

      true
    end
    module_function :actualizar_silueta_departamento_de_osm

    # @param pais      ActiveRecord
    # @param llave     string   Llave en OSM-Boundaries
    # @param recursivo bool     hacer lo mismo con departamentos del país
    # @param prob      string   Colchón para poner problemas encontrados
    # @return bool true sii puede actualizar, dlc problema en colchón prob
    def actualizar_silueta_pais_de_osm(pais, llave, recursivo, prob)
      if pais.osm_id.nil?
        prob = 'País no tiene id de OSM'
        return false
      end

      pide_ids = [pais.osm_id]
      ldepartamentos = nil
      if recursivo
        ldepartamentos = Msip::Departamento.habilitados.where(pais_id: pais.id)
        ldepartamentos.each do |departamento|
          if !fronteras[departamento.osm_id] && pide_ids.count < 99
            pide_ids << departamento.osm_id
          end
        end
      end

      fronteras = OsmHelper.descargar_siluetas_osmboundaries(
        llave, pide_ids, prob)
      if fronteras.nil?
        return false
      end

      coord = OsmHelper.cargar_o_inventar_coord_rotulo_de_relacion(
        pais.osm_id, 'msip_pais', pais.id, prob
      )
      return false if coord.nil?

      ActiveRecord::Base.connection.execute <<~SQL
        UPDATE msip_pais 
          SET osm_frontera=ST_Multi('#{fronteras[pais.osm_id]}'),
          osm_rotulolat='#{coord[:lat]}',
          osm_rotulolon='#{coord[:lon]}',
          osm_fecha=now()
          WHERE id=#{pais.id}
        ;
      SQL

      if recursivo
        ldepartamentos.each do |departamento|
          unless actualizar_silueta_departamento_de_osm(
            departamento, llave, recursivo, fronteras, prob
          )
            return false
          end
        end
      end

      true
    end
    module_function :actualizar_silueta_pais_de_osm


    # Modificar mapa generado por mapshaper para
    # poner capas así:
    # * Una por departamento con silueta y punto con etiqueta con nombre
    # * Una por municipio con silueta y punto con etiqueta con nombre
    def modificar_svg_y_actualizar_base(svg, pais_id = 170)

      raiz = svg.at_css("svg")
      ldep = Msip::Departamento.habilitados.where(pais_id: pais_id)
      ldep.each do |dep|
        raiz << "<g id=\"gdep#{dep.deplocal_cod}\"></g>"
        gdep = svg.at_css("#gdep#{dep.deplocal_cod}")
        sildep = svg.at_css("#mixtidep#{dep.deplocal_cod}")
        sildep.parent = gdep
        etdep = svg.at_css("#nombredep#{dep.deplocal_cod}")
        etdep["font-family"] = "sans-serif"
        etdep["font-size"] = "12"
        etdep["text-anchor"] = "middle"
        etdep.parent = gdep
        dep.svgrotx = etdep.to_s.sub(/.*translate\(/, "").sub(/ .*/, "").to_f
        dep.svgroty = etdep.to_s.sub(/.*translate\([0-9.]* /, "").sub(/\).*/, "").to_f
        # Notamos que Nokogiri con .to_s convierte acentos al 
        # estilo Boyac&#xE1; 
        dep.save!

        lmun = Msip::Municipio.habilitados.where(departamento_id: dep.id)
        lmun.each do |mun|
          codmun = mun.munlocal_cod
          if pais_id == 170
            codmun += dep.deplocal_cod*1000
          end
          puts "codmun=#{codmun}"
          raiz << "<g id=\"gmun#{codmun}\"></g>"
          gmun = svg.at_css("#gmun#{codmun}")
          silmun = svg.at_css("#mixtimun#{codmun}")
          if silmun.nil?
            puts "No se encontró #mixtimun#{codmun}"
            next
          end
          silmun.parent = gmun
          etmun = svg.at_css("#nombremun#{codmun}")
          etmun["font-family"] = "sans-serif"
          etmun["font-size"] = "12"
          etmun["text-anchor"] = "left"
          etmun.parent = gmun
          mun.svgrotx = etmun.to_s.sub(/.*translate\(/, "").sub(/ .*/, "").to_f
          mun.svgroty = etmun.to_s.sub(/.*translate\([0-9.]* /, "").sub(/\).*/, "").to_f
          mun.save!
        end
      end

      return svg
    end
    module_function :modificar_svg_y_actualizar_base
  end
end
