# frozen_string_literal: true

require 'msip_carto/concerns/controllers/validar_controller'

module MsipCarto
  class ValidarController < Msip::Admin::BasicasController
    include MsipCarto::Concerns::Controllers::ValidarController
  end
end
