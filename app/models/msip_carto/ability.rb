module MsipCarto
  class Ability < Msip::Ability
    BASICAS_PROPIAS = []

    def tablasbasicas
      Msip::Ability::BASICAS_PROPIAS +
        MsipCarto::Ability::BASICAS_PROPIAS
    end

    # Establece autorizaciones con CanCanCan
    def initialize_msip_carto(usuario = nil)
      initialize_msip(usuario)
      return if !usuario || !usuario.rol

      case usuario.rol
      when Ability::ROLADMIN
        # can ...
      end
    end
  end
end
