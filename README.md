# Motor base para sistemas de información con cartografía - `msip_carto`

[![Revisado por Hound](https://img.shields.io/badge/Reviewed_by-Hound-8E64B0.svg)](https://houndci.com) Pruebas y seguridad:[![Estado Construcción](https://gitlab.com/pasosdeJesus/msip_carto/badges/main/pipeline.svg)](https://gitlab.com/pasosdeJesus/msip_carto/-/pipelines?page=1&scope=all&ref=main) [![Mantenibilidad](https://api.codeclimate.com/v1/badges/fa979bbb3fc3d52a9d6f/maintainability)](https://codeclimate.com/github/pasosdeJesus/msip_carto/maintainability) [![Cobertura de Pruebas](https://api.codeclimate.com/v1/badges/fa979bbb3fc3d52a9d6f/test_coverage)](https://codeclimate.com/github/pasosdeJesus/msip_carto/test_coverage) 


![Logo de msip_carto](https://gitlab.com/pasosdeJesus/msip_carto/-/raw/main/test/dummy/app/assets/images/logo.jpg)

Este motor añade al motor [msip](https://gitlab.com/pasosdeJesus/msip) 
la cartografía de Colombia extraida de OpenStreetMap y del MGN 2021 del DANE.

La aplicación de prueba de este motor incluye un validador de departamentos 
en OSM con una interfaz sencilla que permite elegir un departamento, 
y presenta su silueta y valida la relación y sus elementos.

Es un poco lento tras dar un departamento por lo que descarga la relación, el
miembro `admin_centre`, el miembro `label` y los miembros de la 
vía que conforman la silueta.


# Desarrollo
Para apoyar el desarrollo de este motor lo referenciamos a la documentación de
[msip](https://gitlab.com/pasosdeJesus/msip/-/master/doc) 


