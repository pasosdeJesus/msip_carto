Puede ver el listado de personas que han implementado en:
<https://gitlab.com/pasosdeJesus/msip_carto/-/graphs/main>

La mayoría de la funcionalidad depende del motor `msip` cuyos
créditos están en
<https://gitlab.com/pasosdeJesus/msip/-/main/CREDITOS.md>

Se han extraído fronteras de Colombia y sus departamentos de
OpenStreetMap que emplea la licencia 
Open Data Commons Open Database License, ver 
<https://www.openstreetmap.org/copyright>

Se han extraído fronteras de departamentos de Colombia del
MGN 2021 del DANE que emplea la licencia CC-BY-SA, ver
<https://geoportal.dane.gov.co/servicios/descarga-y-metadatos/descarga-mgn-marco-geoestadistico-nacional/>



_De Jehová es la tierra y su plenitud; el mundo, y los que en él habitan._

  Salmo 24:1 traducción Reina Valera 1960
