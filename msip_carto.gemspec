require_relative 'lib/msip_carto/version'

Gem::Specification.new do |spec|
  spec.name        = 'msip_carto'
  spec.version     = MsipCarto::VERSION
  spec.authors     = ['Vladimir Támara Patiño']
  spec.email       = ['vtamara@pasosdeJesus.org']
  spec.homepage    = 'https://gitlab.com/pasosdeJesus/msip_carto'
  spec.summary     = 'Motor msip con cartografía de OSM y fuentes oficiales'
  spec.description = 'Se prefirió separar por lo voluminoso de los mapas.'
  spec.license     = 'ISC'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata['allowed_push_host'] = ''

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = spec.homepage

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  end

  spec.add_dependency 'msip'
  spec.add_dependency 'rails', '>= 7.0.4'
end
