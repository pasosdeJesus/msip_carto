source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# Specify your gem's dependencies in msip_carto.gemspec.
gemspec

gem 'bcrypt'                     # Condensando de claves con bcrypt

gem 'bootsnap'                   # Arranque rápido

gem 'cancancan'                  # Control de acceso

gem 'devise'                     # Autenticación

gem 'devise-i18n'                # Localización e Internacionalización

gem 'jbuilder', '>= 2.7'         # Json

gem 'jsbundling-rails'

gem 'kt-paperclip'               # Anexos

gem 'nokogiri'                   # Procesamiento XML

gem 'pg'                         # PostgreSQL

gem 'puma'                       # Lanza en modo desarrollo

gem 'rails', "~> 7.2"

gem 'rails-i18n'                 # Localización e Internacionalización

gem 'sassc-rails'                # Conversión a CSS

gem 'simple_form'                # Formularios

gem 'sprockets-rails'            # Tuberia de recursos

gem 'stimulus-rails'             # Libreria javascript

gem 'turbo-rails'                # Acelera HTML

gem 'twitter_cldr'               # Localiación e internacionalización

gem 'will_paginate'              # Pagina listados

#### Motores que sobrecargan vistas o basados en MSIP en orden de apilamento
gem 'msip', # SI estilo Pasos de Jesús
    git: 'https://gitlab.com/pasosdeJesus/msip.git', branch: 'main'
    #path: '../msip'

group :development, :test do
  gem 'brakeman'

  gem 'bundler-audit'

  gem 'code-scanning-rubocop'

  gem 'colorize'

  gem 'debug'

  gem 'dotenv-rails'

  gem 'rails-erd'

  gem 'rubocop-minitest'

  gem 'rubocop-rails'

  gem 'rubocop-shopify'
end

group :development do
  gem 'web-console', '>= 3.3.0' # Depura en navegador
end

group :test do
  gem 'capybara'

  gem 'cuprite'

  gem 'minitest'                # Pruebas automáticas de regresión con minitest

  gem 'rails-controller-testing'

  gem 'simplecov'
end

group :production do
  gem 'unicorn'
end

# Start debugger with binding.b [https://github.com/ruby/debug]
# gem "debug", ">= 1.0.0"
